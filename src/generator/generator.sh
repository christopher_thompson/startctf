#!/bin/bash

#   -1 if compile.sh is not found
#   -2 if TRANSFER is not found
#   -3 if SOLUTION is not found
#   -4 if user is not root

if [[ $(id - u) -ne 0 ]];
then
  echo "This program must be run as root!"
  exit -4
fi

if [ $# != 1 ];
then
  echo "Usage: generator.sh USERID"
  echo "This will generate files in the bin/ folder as opposed to the src/ folder"
  echo "Do NOT source this file as it will change directories"
  echo "This file MUST be executed with root privileges"
  echo
  echo "NOTE: If compile.sh, TRANSFER, and SOLUTION do not exist, then"
  echo "the challenge will not be transferred to the user"
  exit
fi

export USERID=$1
echo "USERID is $USERID"

# Set challenges folder root
echo -n "CTFROOT is "
. ./setctfroot.sh

# Generator for challenges: compile binaries by calling their scripts
cd ../challenges
for i in $(find . -type d -maxdepth 2);
do
  i=$(readlink -f $i)

  [ -f "$i/compile.sh" ] && exists_compile=1  || exists_compile=0
  [ -f "$i/TRANSFER" ]   && exists_transfer=1 || exists_transfer=0
  [ -f "$i/SOLUTION" ]   && exists_solution=1 || exists_solution=0

  echo "---------------------------------------------------"
  echo "dir:"$i

  if [[ $exists_compile == 1 && $exists_transfer == 1 && $exists_solution == 1 ]];
  then
    # compile
    echo "calling $i/compile.sh"
    # Change environment
    cd $i
    PROGID=${PWD##*/} #Quick and dirty way of getting PROGID
    bash $i/compile.sh
    # Revert environment
    cd -

    # transfer
    echo "transferring files listed in $i/TRANSFER"
    $CTFROOT/generator/transfer.py $i $i/TRANSFER

    # solution
    echo "getting solution and appending to manifest"

    $CTFROOT/generator/append_solution.py $i/SOLUTION $USERID $PROGID
  else
    echo "The process failed for $i for these specific reasons:"
    echo
    [ $exists_compile == 0 ]  && echo "compile.sh not found in $i. Cannot compile"
    [ $exists_transfer == 0 ] && echo "TRANSFER not found in $i. Cannot transfer files"
    [ $exists_solution == 0 ] && echo "SOLUTION not found in $i. Cannot get solution"
  fi
done

echo "---------------------------------------------------"

cd $CTFROOT
cd ..

#useradd user_$USERID -g teams
#PASSWORD=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w10 | head -n1)
#echo -e "$PASSWORD\n$PASSWORD" | passwd user_$USERID &> /dev/null
#mysql --host=localhost --user=admin_user --password=RougequeenWhalehorse startctf << EOF
#insert into shell_credentials (team_id,shell_user,shell_pass) values('$USERID','user_$USERID','$PASSWORD');
#EOF

#chsh -s /bin/bash user_$USERID
#mkdir /var/jail/home/user_$USERID
#mv bin/challenges /var/jail/home/user_$USERID
#rmdir bin
