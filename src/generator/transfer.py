#!/usr/bin/python

from shutil import move
from shutil import copy2
from sys import argv
from os import makedirs
from os.path import exists,isdir

def safe_file_exists(filename):
 
  try:
    f = open(filename)
    f.close()
  except IOError:
    return False
  return True

def replaceli(string, a, b):
  i = string.rfind(a)
  return string[:i] + b + string[i + len(a):]

def usage_exit(extramsg=None):
  print "Usage: transfer.py BASE_DIR TRANSFER_FILE"
  if extramsg is not None: print extramsg
  exit(1)

if len(argv) != 3: usage_exit()

bdir, tfile = (argv[1], argv[2])

f = open(tfile, "r")
data = f.read();

for i in data.split("\n"):
  if i == "": break
  j = i.split(" ")
  # Defensive measures
  if len(j) != 2:
    print "Improper command: " + i
    break

  src = bdir + "/" + j[1]
  dst = replaceli(src, "src", "bin")
  dstdir = dst[:dst.rfind("/")]

  if j[0] not in ["mv", "cp"]:
    print j[0] + " is not considered a command"
    break
#: a bit confusing conditions but checks if src exists allowing for dir move using short circuit eval
  if not((isdir(src) and exists(src)) or safe_file_exists(src)):
    print src + " does not exist"
    break
  # If the destination directory does not exist, create it
  if not exists(dstdir): makedirs(dstdir)
  if j[0] == "mv":
    print "Moving " + src + " to " + dst
    move(src, dst)
  elif j[1] == "cp":
    print "Copying " + src + " to " + dst
    copy2(src, dst)
