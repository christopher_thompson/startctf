#!/bin/bash

if [ $# != 4 ];
then
  echo "Usage: replace.sh INFILE OUTFILE PROG_ID USER_ID"
  echo "If you are not calling this from within the directory you wish to replace files,"
  echo "INFILE and OUTFILE must be absolute paths."
  exit
fi

# Replace all instances of __REPLACEME__ with the corresponding
# generated hash. $CTFROOT is set by env

hflag=`$CTFROOT/challenges/flagfinder $3 $4`
sed s/__REPLACEME__/$hflag/g $1 > $2
