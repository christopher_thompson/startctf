#!/usr/bin/python

import sys
import os
import MySQLdb
import threading
import subprocess
import time

# This is the replacement for api.php; it is no longer necessary to call php methods
# This method creates a wider gap between frontend and backend because it grabs data
# from the MySQL database instead of being called directly.

def poll_db():
    db_connection = MySQLdb.connect(host="localhost", user="admin_user", passwd="RougequeenWhalehorse", db="startctf")
    cursor = db_connection.cursor()
    cursor.execute("SELECT team_id FROM waiting_registration")
    rows = [item[0] for item in cursor.fetchall()]
    print "Showing teams waiting to be registered"
    for row in rows:
        generate_user(str(row))
        sql = "DELETE FROM waiting_registration WHERE team_id = " + str(row)
        cursor.execute(sql)
        db_connection.commit()
    time.sleep(5)
    poll_db()

def generate_user(id):
    cmd = ["bash", "/git/startctf/src/generator/generator.sh", id]
    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE)
    print proc.communicate()

if not 'SUDO_UID' in os.environ.keys():
  print "This program must be run by root!"
  exit(1)

poll_db()
