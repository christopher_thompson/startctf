#!/usr/bin/python

import sys
import os
import MySQLdb

# Separate file from append_solution.py in order to insert program information
# ONCE and not check multiple times, which would overload the db.
# Preferrably this should be called during installation.

def checkArgs(args):
  if (len(args) == 2):
    if os.path.isfile(str(args[1])):
      return
    else:
      print "MANIFEST location refers to a file that does not exist"
  else:
    print "USAGE: append_challenge.py [MANIFEST LOCATION]"
  sys.exit(1)

# Sanitize function
def s(st):
  return st.replace("'", "\\'")

# USAGE: append_challenge.py [MANIFEST LOCATION]

if __name__ == "__main__":
  checkArgs(sys.argv)
  manifest = open(sys.argv[1], 'r')

  dbc = MySQLdb.connect(host="localhost", user="admin_user", passwd="RougequeenWhalehorse", db="startctf")
  db = dbc.cursor()

  # Just 1 line
  for i in manifest:
    # Not the usual format: actually
    # PROGID, TITLE, DESC, HINT, PTS, DIFF, TOPIC
    progid, title, description, hint, points, diff, topic = i.split("\t")

    db.execute("INSERT INTO challenge_list (prog_id, title, description, hint, difficulty, points, topic) " + 
               "VALUES('" + s(progid) + "','" + s(title) + "','" + s(description) + "','" + s(hint) + "'," + diff + "," + points + ",'" + s(topic) + 
                    "') ON DUPLICATE KEY UPDATE")

  db.close()
  manifest.close()
