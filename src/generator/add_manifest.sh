#!/bin/bash

#   -1 if METADATA is not found

if [ $# != 0 ];
then
  echo "Usage: add_manifest.sh"
  echo "This will update the manifest of every program, grabbing METADATA"
  echo "recursively and inserting it into the MySQL database."
  echo "Checks for duplicates in insertion."
  exit
fi

# Set challenges folder root
echo -n "CTFROOT is "
. ./setctfroot.sh

cd ../challenges
for i in $(find . -type d -maxdepth 2);
do
  i=$(readlink -f $i)
  echo "---------------------------------------------------"
  echo "dir:"$i
  if [ -f "$i/METADATA" ];
  then
    echo "METADATA found"
    $CTFROOT/generator/append_challenge.py $i/METADATA
  else
    echo "METADATA not found in $i. Cannot update manifest"
  fi
done
echo "---------------------------------------------------"
