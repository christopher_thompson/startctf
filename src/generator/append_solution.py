#!/usr/bin/python

import string
import sys
import os.path
import subprocess
import os
import MySQLdb

accepted_char = string.digits + string.letters + string.punctuation + ' ' 

#: Checks if the SOLUTION string s is a valid solution, this is met if and only if
#:      1. The SOLUTION has data of some length
#:      2. The SOLUTION only contains accepted characters
#: Returns true if SOlUTION is valid, or false if SOLUTION is invalid
def is_valid_solution(s):
   if (len(s) == 0): return False
   for c in s:
        if not (c in accepted_char): return False
   return True 

#: Current manifest format (sql db)
#: Each entry will take the form of "PROGID \t USERID \t SOLUTION"
def update_manifest(userid, progid, sol):
    dbc = MySQLdb.connect(host="localhost", user="admin_user", passwd="RougequeenWhalehorse", db="startctf")
    db = dbc.cursor()

    # challenge_solutions table
    db.execute("SELECT team_id FROM challenge_solutions")
    result = db.fetchall()

    # The particular team_id does not exist
    if len(result) == 0 or (long(userid),) not in result:
      db.execute("INSERT INTO challenge_solutions (team_id) VALUES('" + userid + "')");

    db.execute("SELECT column_name FROM information_schema.columns WHERE table_name = 'challenge_solutions'");
    result = db.fetchall()

    # The particular prog_id does not exist
    if len(result) == 0 or (progid,) not in result:
      db.execute("ALTER TABLE challenge_solutions ADD " + progid + " VARCHAR(255)");

    # Update the cell in the location
    db.execute("UPDATE challenge_solutions SET " + progid + " = '" + sol.replace("'","\\'") + "' WHERE team_id = " + userid);

    db.close();

def checkArgs(args):
    if (len(args) == 4):
        if os.path.isfile(str(args[1])):
            return
        else:
            print "SOLUTION location refers to a file that does not exist"
    else:
        print "USAGE: append_solution.py [SOLUTION LOCATION] [USERID] [PROGID]"
    sys.exit(1)

#: USAGE: append_solution.py [SOLUTION LOCATION] [USERID] [PROGID]
if __name__ == "__main__":
    checkArgs(sys.argv)
    sol_f = open(sys.argv[1], 'r')
    sol = sol_f.read().strip() 
    if sol == "flag":
       env = os.environ.copy()
       sol = subprocess.check_output([env.get("CTFROOT") + '/challenges/flagfinder', sys.argv[3], sys.argv[2]])
    if is_valid_solution(sol):
        update_manifest(str(sys.argv[2]), str(sys.argv[3]), sol);
    else:
        print "Solution contained within SOLUTION is invalid (empty or illegal) sol=" + sol
    sol_f.close()

