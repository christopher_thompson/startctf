#!/bin/bash

PROGID="crypto1"
DIR="$CTFROOT/challenges/algorithms/$PROGID/"

$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.py" $DIR$PROGID".py" $PROGID $USERID

python $PROGID".py" > /dev/null
rm $PROGID".py"
