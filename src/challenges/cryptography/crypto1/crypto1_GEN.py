#!/usr/bin/python

#: Challenge Name: crypto1 
#: The objective of this challenge is to find the solution to the given cipher.
#: The cipher presented in this challenge is a Caesar cipher, specifically ROT13.

#: A solution to this challenge is first determining the type of cipher through a
#: diagram, and solving through a ROT decryption program (online or coded locally).

flag = "__REPLACEME__"

#: Rotates the given character c by rot, by taking the difference from the starting
#: character (in corresponding lower/upper case) in ASCII, adding the rotation value
#: modulo 26, and adding back the ASCII of the starting character. If the character
#: is not in the alphabet (digit or symbol) it is simply returned.
def rotateChar(c,rot):
	if (ord(c)>=65 and ord(c)<=90):
		return chr((((ord(c)-65)+rot)%26)+65)
	elif (ord(c)>=97 and ord(c)<=122):
		return chr((((ord(c)-97)+rot)%26)+97)
	else:
		return c

#: Rotates a string s by rot by concatenating the result from rotateChar(c,rot) for
#: every character in the string.
def rotateString(s,rot):
	rotString = ""
	for c in s: rotString += rotateChar(c,rot)
	return rotString

if __name__ == "__main__":
	out = open("crypto1.txt",'w')
	out.write(rotateString("Experience is the teacher of all things - Julius Caesar. Flag: " + flag,13)+'\n')
	out.close()
	

