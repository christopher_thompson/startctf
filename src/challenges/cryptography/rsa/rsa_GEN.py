#!/usr/bin/python

#: Challenge Name: rsa_challenge 
#: The objective of this challenge is to decrypt the message endoded by the RSA algorithm 
#: given p,q (large primes), e (coprime to p*q), and the message. 

#: Solution: Implement the RSA decryption algorithm in Java (using BigInteger) for computation
#: or in nearly any other language, and decrypt the message. Example solution program is provided
#: in this folder.

import random
import string
from Crypto.PublicKey import RSA

flag = "__REPLACEME__"

#: Miller-Rabin Primality Test
def miller_rabin_prime(p, accuracy=100):
   if p == 2 or p == 3: return True
   if p < 2: return False
   numTries = 0
   exponent, remainder = decompose(p - 1)
   for _ in range(accuracy):
      possibleWitness = random.randint(2, p - 2)
      if isWitness(possibleWitness, p, exponent, remainder):
         return False
   return True

def decompose(n):
   exponentOfTwo = 0
   while n % 2 == 0:
      n = n/2
      exponentOfTwo += 1
   return exponentOfTwo, n
 
def isWitness(possibleWitness, p, exponent, remainder):
   possibleWitness = pow(possibleWitness, remainder, p)
   if possibleWitness == 1 or possibleWitness == p - 1:
      return False
   for _ in range(exponent):
      possibleWitness = pow(possibleWitness, 2, p)
      if possibleWitness == p - 1:
         return False
   return True

def getPrime(k):
    n = 0
    while(not miller_rabin_prime(n)):
        n = random.randint(10**(k-1),10**k)
    return n

def encryptMessage(m,p,q,e):
    return pow(m,e,p*q)

if ( __name__ == "__main__"):
    random.seed(flag);
    p = getPrime(100)
    q = getPrime(100)
    e = 65537
    m = random.randint(10**14,10**15) 
    c = encryptMessage(m,p,q,e)

    sol_f = open("SOLUTION", 'w')
    sol_f.write(str(m))
    
    challenge_f = open("rsa_challenge.txt",'w')

    challenge_f.write("p = " + str(p) + '\n')
    challenge_f.write("q = " + str(q) + '\n')
    challenge_f.write("e = " + str(e) + '\n')
    challenge_f.write("c = " + str(c))
    challenge_f.close()
    sol_f.close()
