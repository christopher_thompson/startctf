#!/bin/bash

PROGID="md5"
DIR="$CTFROOT/challenges/algorithms/"$PROGID"/"
$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.py" $DIR$PROGID".py" $PROGID $USERID

python $PROGID"_GEN.py" > /dev/null
rm $PROGID".py"
