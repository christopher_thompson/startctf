import random
import subprocess
import string
from sets import Set

MAX_INDEX = 394748 
SALT_LENGTH = 16

flag = "__REPLACEME__"

def get_md5_crypt(password,salt):
	return subprocess.check_output(["md5pass",password,salt])

def index_set(n):
	s = Set()
	while (len(s) < n):
		s.add(random.randint(0,MAX_INDEX))
	return s

def writeHashes(wordlist,sol,out,num):
	indicies = index_set(num)
	for i in range(MAX_INDEX):
        	line = wordlist.readline().strip();
		if (i in indicies):
			out.write(get_md5_crypt(line,getRandomSalt()))
			sol.write(line)
			if len(indicies) != 1:
				sol.write(",")
			indicies.remove(i)
		if len(indicies) == 0:
		    return	
	return

def getRandomSalt():
   s = ""
   for i in range(SALT_LENGTH):
	   s += random.choice(string.letters+string.digits)
   return s


if __name__ == "__main__":
	random.seed(flag) 
	sol = open("SOLUTION",'w')
	out = open("md5.txt",'w')
	wordlist = open("wordlist.txt",'r')
	writeHashes(wordlist,sol,out,5)
	sol.close()
	out.close()
	wordlist.close()
