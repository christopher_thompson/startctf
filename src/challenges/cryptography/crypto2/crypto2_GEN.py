#!/usr/bin/python

#: Challenge Name: crypto2 
#: The objective of this challenge is to find the solution to the given cipher.
#: The cipher presented in this challenge is a substitution cipher.

#: A solution to this challenge is first determining the type of cipher through a
#: diagram, and solving through a decryption program (online (Decrypto) or coded locally).

import random
import string

flag = "__REPLACEME__"

#: Returns a substitution table that pairs every letter in the alphabet to a corresponding
#: letter that is psuedorandomly chosen (seeded).
def createSubstitutionTable():
	keys = list(string.uppercase)
	values = list(string.uppercase)
	d = dict()
	for c in keys:
		val = random.choice(values)
		values.remove(val);
		d[c] = val
	return d

#: Looks up the given character c in the table d, and returns the corresponding character,
#: maintaining the original case of c, and if the character is not in the table, c is returned.
def lookupChar(c,d):
	if (c.upper() in d):
		res = d[c.upper()];
		if (c.islower()):
			return res.lower();
		return res;
	return c;

#: Rotates a string s by rot by concatenating the result from rotateChar(c,rot) for
#: every character in the string.
def createCipher(s):
	cipher = ""
	d = createSubstitutionTable();
	for c in s: cipher += lookupChar(c,d)
	return cipher

if __name__ == "__main__":
	random.seed(flag)
	out = open("crypto2.txt",'w')
	out.write(createCipher("We found the height of the water tower! Its !@@#ft flag:" + flag))
	out.close()

