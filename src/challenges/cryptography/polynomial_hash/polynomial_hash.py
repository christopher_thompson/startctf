# StartCTF Hash Generator
with open("flagsrc", "r") as FILE:
    flag = FILE.read().strip("\n")
print "StartCTF Hashing."
num = int(raw_input("Enter your key\n>>> "))
ascii_vals = [ord(i) for i in flag]
out = ascii_vals[0] + sum(ascii_vals[i] * num**i for i in range(1, len(ascii_vals)))

print "Your hash %d" %(out)
