#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>

int x = 3;

void be_nice()
{
  gid_t gid = getegid();
  setresgid(gid, gid, gid);
}

int main()
{
  be_nice();

  char buf[80];
  bzero(buf, sizeof(buf));
  int k = read(STDIN_FILENO, buf, 80);

  printf(buf);
  printf("%d!\n", x);

  if(x == 4)
  {
    printf("Congratulations!\n");
    system("/bin/cat flag");
    return 0;
  }

  printf("%s", "You shouldn't see this. If you did, you did something wrong.\n");
}
