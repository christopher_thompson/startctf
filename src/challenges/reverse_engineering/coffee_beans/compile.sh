#!/bin/bash

PROGID="coffee_beans"
DIR="$CTFROOT/challenges/algorithms/$PROGID/"

$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.java" $DIR$PROGID".java" $PROGID $USERID
javac $DIR$PROGID.java  > /dev/null
rm $DIR$PROGID.java
