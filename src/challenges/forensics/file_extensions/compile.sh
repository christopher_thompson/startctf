#!/bin/bash

PROGID="file_extensions"
DIR="$CTFROOT/challenges/forensics/$PROGID/"

$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.py" $DIR$PROGID".py" $PROGID $USERID
python $PROGID".py" > /dev/null
rm $PROGID".py"
