#!/usr/bin/python

import os
from reportlab.pdfgen import canvas

#generate pdfs
c = canvas.Canvas("FILE1.pdf")
c.drawString(100,750,"__REPLACEME__")
c.save()

#generate tar
f = open('temp', 'w')
f.write("__REPLACEME__");
f.close();
os.system("tar -c -f FILE2 temp");
os.remove('temp');

#convert pdf to png
os.system("convert FILE1.pdf FILE3.png");
os.system("convert FILE3.png FILE4.jpeg");
#strip extensions
os.system("mv FILE1.pdf FILE1 ; mv FILE3.png FILE3; mv FILE4.jpeg FILE4");
