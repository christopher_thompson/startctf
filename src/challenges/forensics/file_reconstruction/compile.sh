#!/bin/bash

PROGID="file_reconstruction"
DIR="$CTFROOT/challenges/forensics/$PROGID"

$CTFROOT/generator/replace.sh $PROGID"_GEN.py" $PROGID".py" $PROGID $USERID
python $PROGID".py" > /dev/null
rm *.pc *.jpg $PROGID".py"
