#!/usr/bin/python

import PIL 
import qrcode
import tarfile
import hashlib
import os
import sys
import random
import string
from sets import Set

flag = "__REPLACEME__" 

#: Creates an image of a QRCode representing the flag, and saves it to the desired location.
def createQR(flag):
	img = qrcode.make(flag)
	img.save("qr.jpg", "JPEG")

#: Returns a Set of length n containing unique and psuedorandom decimal strings of length o
def makeSet(n,o):
	s = Set()
	while (len(s) != n):
		s.add(generateString(o))
	return s

#: Returns a pseudorandom string of length n
def generateString(n):
	return ''.join(random.choice(string.digits) for x in range(n))

#: Creates a series of equally sized files from the original file with psuedorandomly assigned file names.
#: File pieces take filename format of "######.pc"
def breakFile(file, blockSize):
	size = os.path.getsize(file.name)	
	pieces = (size//blockSize) if (size%blockSize == 0) else ((size//blockSize) + 1) 
	fnames = makeSet(pieces,6)
	tar = tarfile.open("file_reconstruction.tar.gz","w");
	for x in range(pieces):
		name = fnames.pop();
		sys.stdout.write(name + ".pc ")
		f = open(name+".pc", 'a+r+w+')
		if (size-file.tell() < blockSize):
			f.write(file.read(size-file.tell()))
		f.write(file.read(blockSize))
		f.close()
		tar.add(f.name)
	file.close()
	tar.close();

if __name__ == "__main__":
	random.seed(flag);
	createQR(flag)
	file = open("qr.jpg", 'r+b')
	breakFile(file, os.path.getsize(file.name)//10)




