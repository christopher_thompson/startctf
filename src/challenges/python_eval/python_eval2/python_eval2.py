#!/usr/bin/python2
import os
print "Welcome to CTF Games beta"
print "NOTICE: As a result of an unfortunate hacking incident, the release of" 
print " this game been delayed for the forseeable future. However, additional"
print " security has been added.\n"

print "Debugging information: "
print "   The flag exists: %s\n" %(os.path.isfile("flag"))

print "Select an option"
print "1) Blackjack "
print "2) Slots "
print "3) Snake "

while True:
    num = input("> ")
    print "Sorry, option #%s isn't available at the moment." %(num)
