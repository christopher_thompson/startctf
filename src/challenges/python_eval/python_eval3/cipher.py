#!/usr/bin/python
"""
    Utility to cipher files
"""
# TODO: 
#  - Deciphering method
#  - Fix implementation of cipher
#  - Fix eval exploit

import random
import getopt
import sys

def printhelp():
    print("Usage: python cipher.py -e <FILE>")

def encrypt(directory):
    """
    DON"T EVER CODE LIKE THIS PLEASE THIS IS HORRIBLE PRACTICE IN MORE WAYS THAN
    ONE
    """
    with open(directory, "r") as FILE: contents = FILE.read()
    output = ""
    print("For security, please enter 4 keys:")
    key = "["
    for i in range(4): key += '"' + input() + '", '
    key = eval(key + '""]')[int(random.random() * 100 % 4)]
    if len(key) > len(contents): key = key[:len(contents)]
    if len(key) == 0: key = eval("'" + output + "bugatti" + "'")
    for i in [(ord(contents[i]) + ord(key[i % len(key)])) % 26 for i in range(len(contents))]:
        output += eval("'" + chr(65 + i) + "'")
    return output

def main(argv):
    key = None
    writing_flag = False
    encrypted_contents = ""
    outfile = ""
    try:
        opts, args = getopt.getopt(argv, 'hd:e:o:');
    except getopt.GetOptError:
        printhelp()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
           printhelp() 
        elif opt == '-d':
            print("Nice try!")
        elif opt == '-e':
            encrypted_contents = encrypt(arg)
        elif opt == '-o':
            writing_flag = True
            outfile = arg
    if writing_flag:
        with open(outfile, "w") as OUT:
            OUT.write(encrypted_contents)
    else:
        print(encrypted_contents)

if __name__ == "__main__":
    main(sys.argv[1:])
