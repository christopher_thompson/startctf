import random

PROBLEMS=100

def convert_base(n,b):
    return int(n,b)

def conversion_string():
    int initbase = random.randint(2,36)
    return convert_base(random.randint(0,2**32),initbase) + "Base " + initbase + " to Base " + random.randint(2,36)

if __name__ == "__main__":
    out = open("base_conversion.txt",'w')
    sol = open("SOLUTION",'w')
    for i in range(PROBLEMS):
        out.write(convert_base(random.randint(0,2**32),) + "Base ")
    

