#!/bin/bash

PROGID="the_illuminati_is_real"
DIR="$CTFROOT/challenges/steganography/$PROGID/"

$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.py" $DIR$PROGID".py" $PROGID $USERID
python $DIR$PROGID.py  > /dev/null
rm $PROGID.py
rm cover.jpg
