#!/usr/bin/python
import os
import math
import random
from PIL import Image,ImageEnhance 

VARIANCE_MULTIPLIER=0.9

flag = "__REPLACEME__"

def setBit(byte,bit,val):
    if val:
        bitmask = 1 << bit
        return (byte | bitmask)
    else:
        bitmask = ~(1 << bit)
        return (byte & bitmask)

def testBit(byte, offset):
    bitmask = 1 << offset
    value = byte & bitmask
    return value != 0

#: Encode a string into a cover image
def encode_string(offset,cover,steg,hide):
    steg.write(cover.read(offset))
    for c in s:
       hideb = ord(c)
       for j in range(7,-1,-1):
            coverb=ord(cover.read(1))
            coverb = setBit(coverb,0,testBit(hideb,j)) 
            steg.write(chr(coverb))
    steg.write(cover.read())
    steg.flush()
#:offset is really only useful between 0 and 7
def decode(steg,out,offset):
    steg.seek(offset)
    for i in range(os.path.getsize(steg.name)/8):
       outb = 'a'
       for j in range(7,-1,-1):
            stegb = ord(steg.read(1))
            outb = chr(setBit(ord(outb),j,testBit(stegb,0)))
       out.write(outb)
    out.flush()

def encode(offset,cover,steg,hide):
    steg.write(cover.read(offset))
    for c in string:
       hideb = ord(c)
       for j in range(7,-1,-1):
            coverb=ord(cover.read(1))
            coverb = setLSB(coverb,testBit(hideb,j)) 
            steg.write(chr(coverb))
    steg.write(cover.read())
    steg.flush()

#: Encode a file into a cover image
def encode_file(offset,cover,steg,hide):
    steg.write(cover.read(offset))
    for i in range(os.path.getsize(hide.name)):
       hideb = ord(hide.read(1))
       for j in range(7,-1,-1):
            coverb=ord(cover.read(1))
            coverb = setLSB(coverb,testBit(hideb,j)) 
            steg.write(chr(coverb))
    steg.write(cover.read())
    steg.flush()

def binstring(b):
    return '{0:08b}'.format(b)

def debug(c,s,h):
   print "cover: ----------------------------------------------------------------"
   for i in range(os.path.getsize(c.name)):
       print binstring(ord(c.read(1))) + " |",
   print
   print "hide: ----------------------------------------------------------------"
   for i in range(os.path.getsize(h.name)):
       print binstring(ord(h.read(1))) + " |",
   print
   print "steg: ----------------------------------------------------------------"
   for i in range(os.path.getsize(s.name)):
       print binstring(ord(s.read(1))) + " |",

def obscure_image(orig):
   img = Image.open(orig)
   enhancer = ImageEnhance.Color(img)
   img = enhancer.enhance(random.random()*2/VARIANCE_MULTIPLIER)
   enhancer = ImageEnhance.Brightness(img)
   img = enhancer.enhance(random.random()/VARIANCE_MULTIPLIER)
   enhancer = ImageEnhance.Contrast(img)
   img = enhancer.enhance(random.random()/VARIANCE_MULTIPLIER)
   enhancer = ImageEnhance.Sharpness(img)
   img = enhancer.enhance(random.random()*2/VARIANCE_MULTIPLIER)
   img.save("cover.jpg")

if __name__ == "__main__":
    random.seed(flag)
    obscure_image("universal_cover.jpg")
    cover = open("cover.jpg",'r')
    steg = open("steg.jpg","rw+")

    encode(4000,cover,steg,flag)
   # cover.seek(0)
   # steg.seek(0)
   # hide.seek(0)
   # decoded = open("decoded.txt",'w')
   # decode(steg,decoded,5)
   # univ = "universal_cover.jpg"
   # obscure_image(univ)
