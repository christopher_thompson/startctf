#!/bin/bash

PROGID="outguess"
DIR="$CTFROOT/challenges/steganography/$PROGID/"
$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.sh" $DIR$PROGID".sh" $PROGID $USERID

bash $PROGID"_GEN.sh" > /dev/null
rm $PROGID".sh"
rm hide.txt
