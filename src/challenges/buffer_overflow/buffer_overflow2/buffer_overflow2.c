#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <execinfo.h>

#define TRACE_SIZE 16

void be_nice()
{
  uid_t euid = geteuid();
  setresuid(euid, euid, euid);
}

int main()
{
  be_nice();

  printf("If you enter something, will it work? > ");

  // Set up the stack
  char cmd[16];
  char input[16];

  strcpy(cmd, "/bin/ls");

  // Children: Don't try this at home
  scanf("%s", input);

  // Big no-no
  system(cmd);

  printf("%s\n", "Stack trace:");
  int i = 0;
  for(i = 0; i < TRACE_SIZE; i++)
  {
    unsigned int value = * ((unsigned int *) main + i);
    printf("0x%04x 0x%04x\n", (unsigned short) (value >> 16), (unsigned short) (value & 0xFFFF));
  }

  return 0;
}

