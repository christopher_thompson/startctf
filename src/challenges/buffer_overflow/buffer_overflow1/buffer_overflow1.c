#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void be_nice()
{
    uid_t euid = geteuid();
    setresuid(euid, euid, euid);
}

int main()
{
    be_nice();

    int cookie;
    char buf[80];

    printf("buf: %08x cookie: %08x\n", &buf, &cookie);
    gets(buf);

    if (cookie == 0x41424344) {
        printf("you win!\n");
        system("/bin/sh");
    }

    return 0;
}

