#!/bin/bash
gcc -mpreferred-stack-boundary=3 -m32 -O0 -fno-stack-protector -o rop2 rop2.c
execstack -s rop2
$CTFROOT/generator/replace.sh flagsrc flag rop2 $USERID
sed -i '1i Congratulations.' flag
