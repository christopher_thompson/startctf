#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char * string = "/bin/cat flag";

void be_nice()
{
  gid_t gid = getegid();
  setresgid(gid, gid, gid);
}

void hack_this()
{
  char buf[8];
  printf("The function (system) location is at %p!\n", system);
  printf("The string is located at %p!\n", string);
  read(STDIN_FILENO, buf, 32);
}

int main()
{
  be_nice();
  hack_this();
  printf("%s", "You shouldn't see this. If you did, you did something wrong.\n");
}
