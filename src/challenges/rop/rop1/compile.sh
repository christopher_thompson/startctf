#!/bin/bash
gcc -mpreferred-stack-boundary=3 -m32 -O0 -fno-stack-protector -o rop1 rop1.c
execstack -s rop1
$CTFROOT/generator/replace.sh flagsrc flag rop1 $USERID
