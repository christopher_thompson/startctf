#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void be_nice()
{
  gid_t gid = getegid();
  setresgid(gid, gid, gid);
}

void crack_shell()
{
  printf("%s\n", "Congratulations.");
  system("/bin/cat flag");
}

void hack_this()
{
  char buf[8];
  printf("The function location is at %p!\n", crack_shell);
  read(STDIN_FILENO, buf, 64);
}

int main()
{
  be_nice();
  hack_this();
  printf("%s", "You shouldn't see this. If you did, you did something wrong.\n");
}
