#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void be_nice()
{
  uid_t euid = geteuid();
  setresuid(euid, euid, euid);
}

void random_function()
{
  printf("Random.\n");
}

void hack_me()
{
  char buf[80];
  read(STDIN_FILENO, buf, 100);
  printf("%p %p\n", buf, random_function);
}

int main()
{
  // I won't fail this time... there's really nothing you can do anyways
  hack_me();
  printf("Still here!\n");
}

