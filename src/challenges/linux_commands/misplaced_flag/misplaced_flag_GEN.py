#!/usr/bin/python

#:Challenge Name: misplaced_flag
#:The objective of this challenge is to find a flag contained in one of the files in the
#:directory with format "FLAG: XXXXXXXXXXXXX" (where the X's are replaced with the flag).

#The simple solution to this challenge is to use grep -r "FLAG: " [directory] 

import random
import string
import os

flag = "__REPLACEME__"

#: Generates a psuedorandom string (seeded on flag) of length n.
def getRandomString(n):
	return ''.join(random.choice(string.hexdigits) for x in range(n))

#: Generates a file that consists of (if flagFile is set to False) random lines
#: of length 64 or (if flagFile is set to True) random lines with the exception of
#: a single line in the format "FLAG: XXXXXXXXXXXXXXX".
def makeFile(name,lines, flagFile):
	f = open("misplaced_flag/" + name + ".txt",'w')
	index = random.randint(0,lines-1) if (flagFile) else -1 
	for i in range(lines):
		if (i == index):
			f.write("FLAG: " + flag + "\n")
		else:
			f.write(getRandomString(64) + "\n")
	f.close()

#: Generates n files in the directory using makeFile, and randomly chooses which file
#: is the flag-bearing file.
def makeFileSet(n):
	index = random.randint(0,n-1)
	for i in range(n):
		makeFile(str(i).zfill(6),100,i==index)

if __name__ == "__main__":
	random.seed(flag)
	os.mkdir("misplaced_flag")
	makeFileSet(10)
