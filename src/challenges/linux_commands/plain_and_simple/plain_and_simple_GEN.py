#!/usr/bin/python

#:Challenge Name: cat_challenge
#:The objective of this challenge is to read the generated file to find the flag.
#:The simple solution to this challenge would be to use: cat [infile] 

flag = "__REPLACEME__"
challenge_f = open("cat_challenge.txt",'w')
challenge_f.write(flag + '\n')
challenge_f.close()
