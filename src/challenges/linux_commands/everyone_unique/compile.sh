#!/bin/bash

PROGID="everyone_unique"
DIR="$CTFROOT/challenges/linux_commands/$PROGID/"

$CTFROOT/generator/replace.sh $DIR$PROGID"_GEN.py" $DIR$PROGID".py" $PROGID $USERID
python $PROGID".py" > /dev/null
rm $PROGID".py"
