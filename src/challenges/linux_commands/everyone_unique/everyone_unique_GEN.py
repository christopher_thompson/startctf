#!/usr/bin/python

#: Challenge Name: everyone_unique
#: The objective of this challenge is to find the only line from this output
#: that is not unique.

#: The simple solution to this challenge is to use uniq -d [infile] 


import string
import hashlib
import sys
import random
from sets import Set

flag = "__REPLACEME__"

def generate(flag, lines, dest):
	#: the index at which the flag will be placed, randomly generated.
	s = Set();
	while (len(s)!=2):
		s.add(random.randint(0,lines-1));
	for x in range(lines):
		if (x in s):
			dest.write(flag+'\n')
		else:
			dest.write(hashlib.sha256(flag+str(x)).hexdigest()+'\n')

if __name__ == "__main__":
	random.seed(flag);
	dest = open("everyone_unique.txt",'w')
	generate(flag,1000,dest)
	dest.close();
