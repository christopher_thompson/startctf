#!/usr/bin/python

#:Challenge Name: sed_challenge
#:The objective of this challenge is to concatenate all hexadecimal characters in a
#:given file (sed_challenge.txt) to create a single 64 character flag.

#:A solution to this challenge is using sed

import random
import string
from sets import Set

flag = "__REPLACEME__"
solution = ""

#: Returns a psuedorandom non-hexadecimal yet printable character (seeded).
def randomChar():
	return random.choice(list(set(string.printable)-set(string.hexdigits)))

#: Returns a Set of psuedorandom (seeded) integers from between 0 and size, inclusive, of
#: length n
def indexSet(n,size):
	s = Set()
	while (len(s) != n):
		s.add(random.randint(0,size))
	return s

#: Generates the challenge file by either writing a character from the flag (hexadecimal)
#: or writing a random character given by randomChar()
def generate(name,filesize):
	f = open(name+".txt",'w')
	indicies = indexSet(len(solution),filesize-1)
	for i in range(filesize):
		if (i in indicies):
			f.write(solution[len(solution)-len(indicies)])
			indicies.remove(i)
		else:
			f.write(randomChar())
	f.close()

#: creates a random (seeded) hex string of length n
def getRandomHexString(n):
   s = ""
   for i in range(n):
       s += random.choice(string.hexdigits)
   return s.lower()

if __name__ == "__main__":
	random.seed(flag)
	solution = getRandomHexString(32) 
	solution_f = open("SOLUTION",'w')
	solution_f.write(solution)
	solution_f.close()
	generate("thats_what_she_sed",1000)
