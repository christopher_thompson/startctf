# Project Description
* * *
## StartCTF Competition
* * *

See the temporary location [here](http://startctf.apsim.dyndns.org).

We will move to a new server ASAP.

## StartCTF Website
* * *

This website was designed to allow interfacing of competitors of the StartCTF competition and the back end developed to introduce those new to cyber forensics topics and vulnerabilities used by cyber security professionals.

# Project Dependencies
---

You will need:

+ Apache Server Software
+ PHP
+ MySQL

# Installation
---

The easiest way to get all these dependencies installed and setup is using some Linux distro such as Ubuntu.

The setup system has been made much easier in the past few updates. Now, you can type

    ./autogen.sh && ./configure && make

The Makefile will call the respective setup scripts in the web and src folders (the website and backend, respectively).

If this does not work, run setup.sh in web/web_setup/ and install.sh in src/.

You can now open a browser and go to "127.0.1.1" to see the source code in action!

# Troubleshooting
---

1. Make sure you are on a Linux system. The scripts will not run on anything that does not have bash installed.
2. Make sure all the dependencies are met. Look in dependencies.txt in the base folder for a list of dependencies.
3. Make sure MySQL has a root user which has administrator privileges on the database.
4. Make sure there is enough disk space to hold the necessary files. Because StartCTF stores challenge data on the server, a lot of space may be used. The Linux OSes will run into problems when there is low disk space.

If you have any questions or comments feel free to talk to us in the freenode IRC at #startCTF.