Coding Style for the C Programming Language

- Whitespace
  * Use 2 spaces for indentation
  * 1 space occurs between operators (i.e. 3 + 2)
  * Blank lines occur where grouping is necessary
  * 1 blank line at end of file
- Comments
  * Multiline for descriptors
  * Program header includes author, title, and description
  * Function headers include description
- Names
  * Functions, variables occur in camel case
  * Constants occur in capital case with underscores
  * Short and descriptive variable names
  * Macros defined with #define occur in capital case
- Organization
  * Programs are split into .h (header) and .c (source) files
  * 80 lines maximum length per line; use \ at end of long line to split
  * The #include, #define preprocessor directives usually occur at top of file
  * No Makefile included (only for this project)
  * Brackets occur after control flow or function names such as:
    
    ...

    if(a == 3)
    {

    }

    ...

  * Try to keep scope minimized
- Memory/Pointers
  * NULL pointers after memory deallocated
  * Use calloc where arrays are located if necessary
- Casts
  * Avoid excessive casting
- Control Flow
  * Do not use braces if only 1 line
  * Avoid while(1) unless necessary

