# Check if dependencies are met; see dependencies.txt

FAIL_MSG = "DEPENDENCY MISSING: "
PASS_MSG = "Found: "

modules = ["math", "random", "string", "os", "sys", "shutil", "subprocess", "qrcode"]

def check_module(module):
  try:
    __import__(module)
  except ImportError:
    print FAIL_MSG + module
    return 1
  print PASS_MSG + module
  return 0

print "Python Dependency Checker..."

rtcode = 0
for i in modules:
  rtcode += check_module(i)

if rtcode > 0: exit(1)
exit(0)
