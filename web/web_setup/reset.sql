-- ---------------------------------------------------------------------------------------
-- This is the beginning of the Start CTF Reset SQL
-- ---------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------
-- Removing users and databases
-- ---------------------------------------------------------------------------------------

DROP USER 'sec_user'@'localhost';
DROP USER 'admin_user'@'localhost';

DROP DATABASE `secure_login`;
DROP DATABASE `startctf`;
