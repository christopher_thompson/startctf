-- ---------------------------------------------------------------------------------------
-- This is the beginning of the Start CTF SQL
-- ---------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------
-- Creating users for databse
-- ---------------------------------------------------------------------------------------

CREATE USER 'sec_user'@'localhost' IDENTIFIED BY 'PixieblimpNinjayacht';

CREATE USER 'admin_user'@'localhost' IDENTIFIED BY 'RougequeenWhalehorse';

-- ---------------------------------------------------------------------------------------
-- Creating secure login database for users and logging in
-- ---------------------------------------------------------------------------------------

CREATE DATABASE secure_login;

CREATE TABLE `secure_login`.`members` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `first_name` VARCHAR(30) NOT NULL,
    `last_name` VARCHAR(30) NOT NULL,
    `team_id` int(11) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `password` CHAR(128) NOT NULL,
    `salt` CHAR(128) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE `secure_login`.`login_attempts` (
    `user_id` int(11) NOT NULL,
    `time` VARCHAR(30) NOT NULL,
    `unlock_time` VARCHAR(30) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE `secure_login`.`cookies` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id` INT NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `ip` VARCHAR(15) NOT NULL,
    `user_agent` TEXT NOT NULL,
    `epoch` int(11) NOT NULL,
    `login_string` VARCHAR(128) NOT NULL
) ENGINE = InnoDB;

GRANT SELECT, INSERT, UPDATE ON `secure_login`.`members` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `secure_login`.`cookies` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE ON `secure_login`.`login_attempts` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `secure_login`.`members` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `secure_login`.`cookies` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `secure_login`.`login_attempts` TO 'admin_user'@'localhost';

-- ----------------------------------------------------------------------------------------
-- Creating the beef of the database, where almost all the magic happens
-- ----------------------------------------------------------------------------------------

CREATE DATABASE startctf;

CREATE TABLE `startctf`.`teams` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `join_code` varchar(255) NOT NULL,
    `member_1` int(11) NOT NULL,
    `member_2` int(11) NOT NULL,
    `member_3` int(11) NOT NULL,
    `score` int(11) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `startctf`.`shell_credentials` (
    `team_id` int(11) NOT NULL,
    `shell_user` varchar(255) NOT NULL,
    `shell_pass` varchar(255) NOT NULL,
    PRIMARY KEY  (`team_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `startctf`.`waiting_registration` (
    `team_id)` int(11) NOT NULL,
    PRIMARY KEY  (`team_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `startctf`.`global_variables` (
    `variable` varchar(255) NOT NULL,
    `value` int(11) NOT NULL,
    PRIMARY KEY  (`variable`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `startctf`.`global_variables` VALUES ('allow_registration', 1);

INSERT INTO `startctf`.`global_variables` VALUES ('allow_submissions', 1);

INSERT INTO `startctf`.`global_variables` VALUES ('allow_account_change', 1);

INSERT INTO `startctf`.`global_variables` VALUES ('enable_scoreboard', 1);

CREATE TABLE `startctf`.`challenge_list` (
    `prog_id` CHAR(255) NOT NULL,
    `title` varchar(255) NOT NULL,
    `description` text NOT NULL,
    `hint` text NOT NULL,
    `difficulty` int,
    `points` int,
    `topic` varchar(60),
    PRIMARY KEY  (`prog_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `startctf`.`challenge_teams` (
    `team_id` int(11) NOT NULL,
    PRIMARY KEY  (`team_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `startctf`.`challenge_solutions` (
    `team_id` int(11) NOT NULL,
    PRIMARY KEY  (`team_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `startctf`.`challenge_updates` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `body` text NOT NULL,
    `date` int(11) NOT NULL,
    `picture` text NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

GRANT SELECT, INSERT, UPDATE, DELETE ON `startctf`.`teams` TO 'sec_user'@'localhost';

GRANT INSERT ON `startctf`.`waiting_registration` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, DELETE ON `startctf`.`shell_credentials` TO 'sec_user'@'localhost';

GRANT SELECT ON `startctf`.`global_variables` TO 'sec_user'@'localhost';

GRANT SELECT ON `startctf`.`challenge_list` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE ON `startctf`.`challenge_teams` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT ON `startctf`.`challenge_solutions` TO 'sec_user'@'localhost';

GRANT SELECT ON `startctf`.`challenge_updates` TO 'sec_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`teams` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`waiting_registration` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`shell_credentials` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`global_variables` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`challenge_list` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`challenge_teams` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`challenge_solutions` TO 'admin_user'@'localhost';

GRANT SELECT, INSERT, UPDATE, ALTER, DELETE ON `startctf`.`challenge_updates` TO 'admin_user'@'localhost';
