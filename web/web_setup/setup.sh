# This file will automatically setup the server for the nessesary dependencies
# and configurations that are needed (ie. Go through all subdirectories of public_html
# and configure the .htaccess files

sudo apt-get install apache2 php5 php5-mysql libapache2-mod-php5 php5-json;
sudo apt-get -f install;
sudo apt-get install mysql-server;
sudo a2enmod rewrite;
sudo /etc/init.d/apache2 restart;

echo "Enter MySQL Password: "
sudo mysql -u root -p < reset.sql;
sudo mysql -u root -p < setup.sql;

cp startctf.conf.copy startctf.conf
cp .admin_panel.htaccess.copy .admin_panel.htaccess
cp .insecure_security.htaccess.copy .insecure_security.htaccess

cd ../
replacePath=`pwd 2>&1`;
echo $replacePath;
sed -i -e "s|__PATH__|$replacePath|g" ./web_setup/.admin_panel.htaccess;
sed -i -e "s|__PATH__|$replacePath|g" ./web_setup/.insecure_security.htaccess;
sed -i -e "s|__PATH__|$replacePath|g" ./web_setup/startctf.conf;
cp ./web_setup/.admin_panel.htaccess ./public_html/admin_panel/.htaccess;
cp ./web_setup/.insecure_security.htaccess ./public_html/online_challenges/Insecure_Security_INC/.htaccess;
cp ./web_setup/startctf.conf /etc/apache2/sites-available/;
rm /etc/apache2/sites-enabled/*;
sudo a2ensite startctf.conf;
sudo service apache2 reload;
