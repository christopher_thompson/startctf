<?php
require_once '../php/DatabaseConn.class.php';
$team_name = $_GET['team'];
$dbConn = new DatabaseConn(false);
$dbConn->set_table('teams');
$team = $dbConn->get_item($team_name, "name");

$success = array();
if ($team) {
    $success['teamExists'] = true;
} else {
    $success['teamExists'] = false;
}

echo json_encode($success);
?>
