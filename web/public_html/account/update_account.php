<?php
require_once '../php/Require.php';
require_once '../php/ChallengeHandlers.func.php';

$allow_account_change = true;
if (!check_page_access("allow_account_change")) {
    $allow_account_change = false;
}
$return_data = array('updated' => false, 'error' => "");

$user = array();
$user_id = -1;
$team = array("name" => "");
$login_conn = new DatabaseConn(false, 'secure_login');
$db_conn = new DatabaseConn();
$team_admin = false;

if (!$allow_account_change) {
    exit;
}

if (!login_check()) {
    returnError("Not logged in.");
}

if ($info = get_stored_credentials()) {
    $login_conn->set_table("members");
    $user_id = $info['user_id'];
    $result = $login_conn->get_item($user_id, "id");
    if (!$user = $result[0]) {
        returnError("Unable to make changes to account.");
    }
    if ($user['team_id'] != 0) {
        $db_conn->set_table("teams");
        $result = $db_conn->get_item($user['team_id'], "id");
        if (!$team = $result[0]) {
            returnError("Unable to make changes to account.");
        }
    }
    $team_admin = false;
    if ($team['member_1'] == $user_id) {
        $team_admin = true;
    }
} else {
    returnError("Unable to make changes to account.");
}

if (isset($_POST['firstName']) && $_POST['firstName']) {
    $first_name = substr($_POST['firstName'], 0, 255);
    if (preg_match('/^[a-zA-Z0-9_\'\.-ᶜ]*$/', $first_name)) {
        if (!$login_conn->edit_item($first_name, 'first_name', $user_id)) {
            returnError("Unable to update your first.");
        }
    } else {
        returnError("Sorry but your name has unsupported characters. Please complain to the developers that your name is not supported.");
    }
}

if (isset($_POST['lastName']) && $_POST['lastName']) {
    $last_name = substr($_POST['lastName'], 0, 255);
    if (preg_match('/^[a-zA-Z0-9_\'\.-ᶜ]*$/', $last_name)) {
        if (!$login_conn->edit_item($last_name, 'last_name', $user_id)) {
            returnError("Unable to update your last name.");
        }
    } else {
        returnError("Sorry but your name has unsupported characters. Please complain to the developers that your name is not supported.");
    }
}

if (isset($_POST['email']) && $_POST['email']) {
    $email_regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
    $email = $_POST['email'];
    if (preg_match($email_regex, $email)) {
        if (!$login_conn->edit_item($email, 'email', $user_id)) {
            returnError("Unable to update your email.");
        }
        edit_stored_email_cred($email);
    } else {
        returnError("Email given is not an email.");
    }
}

if (isset($_POST['oldPassword'], $_POST['newPassword']) && strlen($_POST['oldPassword']) > 0 && strlen($_POST['newPassword']) > 0) {
    $oldPassword = $_POST['oldPassword'];
    $newPassword = $_POST['newPassword'];
    if (strcasecmp($oldPassword, $newPassword) != 0) {
        $old_password_hashed = hash('sha512', $oldPassword.$user['salt']);
        if (strcasecmp($old_password_hashed, $user['password'])) {
            $login_conn->set_table("members");
            if ($login_conn->edit_item($newPassword, "password", $user['id'])) {
                // Change user's password 
            } else {
                returnError("Unable to make changes to account.");
            }
        } else {
            returnError("Unable to make changes to account.");
        }
    } else {
        returnError("If you are going to reset your password, please choose a new password instead of using your old password.");
    }
}

if (isset($_POST['team'], $_POST['teamPassword']) && $_POST['team'] && preg_match('/^[a-zA-Z0-9]*$/', $_POST['teamPassword'])) {
    $team = substr($_POST['team'], 0, 255);
    $team_pass = $_POST['teamPassword'];

}

returnSuccess("All changes to your account were made successfully."); 

function returnError($error) {
    $return_data["error"] = $error; 
    echo json_encode($return_data);
    exit;
}

function returnSuccess() {
    $return_data["updated"] = true; 
    echo json_encode($return_data);
    exit;
}
?>
