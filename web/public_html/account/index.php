<?php
require_once '../php/Require.php';
$allow_account_change = true;
if (!check_page_access("allow_account_change")) {
    $allow_account_change = false;
}
$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="../js/sha512.js"></script>
<script type="text/javascript" src="../js/account.js"></script>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Account</h1>
    </div>
    <?php
    if (!$allow_account_change) {
        echo html_error("Account changes are not allowed at this time");
    } else {
        if ($loggedIn) {
            if ($info = get_stored_credentials()) {
                $login_conn = new DatabaseConn(false, 'secure_login');
                $login_conn->set_table("members");
                $user_id = $info['user_id'];
                $result = $login_conn->get_item($user_id, "id");
                $user = $result[0];
                $user['first_name'] = strip_tags(htmlentities($user['first_name']));
                $user['last_name'] = strip_tags(htmlentities($user['last_name']));
                $user['email'] = strip_tags(htmlentities($user['email']));
                $team = array(
                    "name" => ""
                );
                if ($user['team_id'] != 0) {
                    $db_conn = new DatabaseConn();
                    $db_conn->set_table("teams");
                    $result = $db_conn->get_item($user['team_id'], "id");
                    $team = $result[0];
                }
                $team_admin = false;
                $join_code = "";
                $admin_info = "";
                $team['name'] = strip_tags(htmlentities($team['name']));
                if ($team['member_1'] == $user_id) {
                    $team_admin = true;
                    $join_code = $team['join_code'];
                    $admin_info = <<<HTML
                    <div class="form-group">
                        <label for="joinCode" class="col-lg-4 control-label">Team Join Code</label>
                        <div class="inputField col-lg-8">
                            <p>You are team administrator of: <p style="word-wrap:break-word"><b>{$team['name']}</b></p> and in order for others to join your team, please share with them this join code: <b>$join_code</b></p>
                        </div>
                    </div>
HTML;
                }
                echo <<<HTML
    <div class="col-lg-6">
        <form class="form-horizontal">
            <fieldset>
                <legend>View/Edit your Account</legend>
                <div class="form-group">
                    <label for="inputFirstName" class="col-lg-4 control-label">First Name</label>
                    <div class="inputField col-lg-8 has-success">
                        <input type="text" onfocus="verifyInput('firstName');" class="form-control" id="inputFirstName" value="{$user["first_name"]}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLastName" class="col-lg-4 control-label">Last Name</label>
                    <div class="inputField col-lg-8 has-success">
                        <input type="text" onfocus="verifyInput('lastName');" class="form-control" id="inputLastName" value="{$user["last_name"]}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-4 control-label">Email</label>
                    <div class="inputField col-lg-8 has-success">
                        <input type="email" onfocus="verifyInput('email');" class="form-control" id="inputEmail" value="{$user["email"]}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-4">
                        <button id="changePasswordButton" onclick="return false;" class="btn btn-default">Change password</button>
                    </div>
                </div>
                $admin_info
                <div class="form-group">
                    <label for="inputTeam" class="col-lg-4 control-label">Team</label>
                    <div class="inputField col-lg-8">
                        <input type="text" onfocus="verifyInput('team');" class="form-control" id="inputTeam" value="{$team['name']}">
                    </div>
                </div>
                <div class="form-group" style="display:none">
                    <label for="inputTeamPassword" class="col-lg-4 control-label">Team Password</label>
                    <div class="inputField col-lg-8">
                        <input type="text" onfocus="verifyInput('teamPassword');" class="form-control" id="inputTeamPassword" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-4">
                        <button id="submitButton" class="btn btn-default">Submit Changes</button>
                        <button class="btn btn-default" onclick="location.href='../'; return false;">Cancel</button>
                    </div>
                </div>
            </fieldset>
        </form>
        <div id="updateError">
        </div>
    </div>
    <div id="formHelp" class="col-lg-6">
        <h1 id="formHelpTitle"></h1>
        <div id="formHelpContent">
            <div id="changePasswordHelp" style="display: none;">
                <div class="col-lg-12">
                    <form class="form-horizontal">
                        <fieldset>
                            <legend>Change your password</legend>
                                <div id="oldPasswordContainer">
                                    <div class="form-group">
                                        <label for="inputOldPassword" class="col-lg-4 control-label">Old Password</label>
                                        <div class="inputField col-lg-8">
                                            <input type="password" class="form-control" id="inputOldPassword">
                                        </div>
                                    </div>
                                </div>
                                <div id="newPasswordContainer">
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-lg-4 control-label">New Password</label>
                                        <div class="inputField col-lg-8">
                                            <input type="password" onfocus="verifyInput('password');" class="form-control" id="inputPassword">
                                        </div>
                                    </div>
                                </div>
                                <div id="retypedPasswordContainer">
                                    <div class="form-group">
                                        <label for="inputRetypePassword" class="col-lg-4 control-label">Retype New Password</label>
                                        <div class="inputField col-lg-8">
                                            <input type="password" onfocus="verifyInput('retypePassword');" class="form-control" id="inputRetypePassword" placeholder="">
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                    </form>
                </div>
                <hr />
            </div>
            <div id="firstNameHelp" style="display:none;"><p></p><hr /></div>
            <div id="lastNameHelp" style="display:none;"><p></p><hr /></div>
            <div id="emailHelp" style="display: none;"><p>For our primary form of communcation with you, we are going to be using email to keep you up to date with the latest and greatest information about this competition. <strong>Remeber this email because you will be using it to login to your account!</strong> In case you do not have an account, Google offers a great free service that you can access <a href="http://mail.google.com">here.</a> An email typically looks something like this: username@email.com (Just a FYI for you).</p><hr /></div>
            <div id="teamHelp" style="display: none;"><p>We are going to next ask you to enter in a name of a team you wish to join or wish to create. As you type your team name, we will check our list of teams to check the availability of your team name. If you know the name of the team you want to join, but do not remember exactly what it is, type what you remember and check the teams that match closest to what you are typing. If you create a new team, you will have to go to your account settings to manage the people who are to also be on your team.</p><hr />
            <button id="teamCheckButton" type="button" class="btn btn-primary" style="margin-bottom:15px">Team Name Check</button>
            <div id="teamCheckNotification">
            </div>
            </div>
            <div id="teamPasswordHelp" style="display: none;"><p>In order to prevent users who are not a part of a team joining the team, we implemented this password system. <strong>If you are creating a new team, you do not need to create a password for the team, one will be generated for you</strong>. If you are attempting to join a team however, you must enter the password exactly as your team's administrator told you (if he / she has not given you a password, ask them to go to their <strong>Account</strong> page and go to <strong>Manage Team</strong> and have them send you the password that is given).</p><hr /></div>
        </div>
    </div>
</div>
HTML;
            } else {
                echo html_error("It appears the cookie monster got a hold of your cookies. Try to login again.");
            }
        } else {
            echo html_error("Make sure you are logged in first before you try to access your account.");
        }
    }
    ?>
</div>
<?php
    echo html_end_setup();
?>
