<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>

<div class="container well">
    <div class="jumbotron page-title">
        <h1>Chat</h1>
        <center><p>Get help from certified Start CTF help officials and other teams (no sharing answers please)</p></center>
    </div>
    <div class="row-fluid">
        <div class="col-lg-9">
            <iframe src="http://webchat.freenode.net/?channels=startCTF" style="width:100%;height:600px;" seamless></iframe>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <center><h3 class="panel-title">IRC Command Help</h3></center>
                </div>
                <div id="cmdInfo">
                    <center>
                    <div style="display:none" id="joinInfo"><h4>/join</h4>Join an IRC channel. Ex. /join #startCTF</div>
                    <div style="display:none" id="meInfo"><h4>/me</h4>Displays an "action message". Ex. /me waves hello</div>
                    <div style="display:none" id="msgInfo"><h4>/msg</h4>Starts a private chat with another user. Ex. /msg tnekent Thank you</div>
                    <div style="display:none" id="nickInfo"><h4>/nick</h4>Changes your nickname. Ex. /nick Catbug</div>
                    <div style="display:none" id="noticeInfo"><h4>/notice</h4>Sends a message to another person without opening a new window. Ex. /notice eleetH4x0r I like your nickname</div>
                    <div style="display:none" id="partInfo"><h4>/part</h4>Leave the current channel (disconnects you from the current IRC chat).</div>
                    <div style="display:none" id="pingInfo"><h4>/ping</h4>Tests the speed of the connection you have with another user. Ex. /ping chrislegolord</div>
                    <div style="display:none" id="queryInfo"><h4>/query</h4>Similar to /msg, but forces a new window open. Ex. /query StartCTFHelp I am having trouble creating an account</div>
                    <div style="display:none" id="quitInfo"><h4>/quit</h4>Disconnects you from the IRC client completely.</div>
                    <div style="display:none" id="ignoreInfo"><h4>/ignore</h4>Prevents you from seeing messages from a certain person. Adding -r undoes this command. Ex. /ignore troll123 3, /ignore -r troll123 3</div>
                    <div style="display:none" id="whoisInfo"><h4>/whois</h4>Displays technical infromation about the user. Ex. /whois yourMom</div>
                    <div style="display:none" id="chatInfo"><h4>/chat</h4>Opens up a private chat window with another user. Ex. /chat uMadBro</div>
                    <div style="display:none" id="helpInfo"><h4>/help</h4>Displays helpful information about the IRC channel or about a topic.</div>
                    </center>
                </div>
                <div class="panel-body">
                    <ul id="commandHelp" style="padding-left:20px;cursor:pointer;">
                        <li id="join">/join #channelname</li>
                        <li id="me">/me message</li>
                        <li id="msg">/msg nickname (message)</li>
                        <li id="nick">/nick newnickname</li>
                        <li id="notice">/notice nickname (message)</li>
                        <li id="part">/part</li>
                        <li id="ping">/ping nickname</li>
                        <li id="query">/query nickname (message)</li>
                        <li id="quit">/quit</li>
                        <li id="ignore">/ignore (-r) nickname 3</li>
                        <li id="whois">/whois nickname</li>
                        <li id="chat">/chat nickname</li>
                        <li id="help">/help (topic)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../js/chat.js"></script>
<?php
    echo html_end_setup();
?>
