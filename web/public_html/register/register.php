<?php
require_once '../php/Require.php';
require_once '../php/ChallengeHandlers.func.php';

$allow_registration = true;
if (!check_page_access("allow_registration")) {
    $allow_registration = false;
}
$return_data = array('registered' => false, 'error' => "");

if ($allow_registration && isset($_POST['email'], $_POST['password'], $_POST['firstName'], $_POST['lastName'], $_POST['team']) &&
       !(empty($_POST['email']) || empty($_POST['password']) || empty($_POST['firstName']) || empty($_POST['lastName']) || empty($_POST['team']))) {
    $login_conn = new DatabaseConn(false, "secure_login");
    $db_conn = new DatabaseConn();

    $first_name = $db_conn->sanitize_variable($_POST['firstName']);
    $fist_name = substr($first_name, 0, 255);
    $last_name = $db_conn->sanitize_variable($_POST['lastName']);
    $last_name = substr($last_name, 0, 255);
    $email = $db_conn->sanitize_variable($_POST['email']);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $password = $db_conn->sanitize_variable($_POST['password']);
    $team = $db_conn->sanitize_variable($_POST['team']);
    $team = substr($team, 0, 255);
    $team_password = "";
    if (isset($_POST['teamPassword'])) {
        $team_password = $db_conn->sanitize_variable($_POST['teamPassword']);
    }
    
    $login_conn->set_table("members");
    $used_email = $login_conn->get_item($email, "email");
    
    if (!$used_email) {
        // Creating user
        // Adding user to a team
        $db_conn->set_table("teams");
        $team_exists = $db_conn->get_item($team, "name");
        $team_id = 0;
        $join_team = array(
            'password_check' => false,
            'member_2_open' => false,
            'member_3_open' => false,
            'new_team' => false
        );

        if (!$team_exists) {
            // Create a new team
            $team_join_code = hash("sha512", $team.$email);
            $team_join_code = substr($team_join_code, 0, 9);
            $team_id = createTeam($team, $team_join_code, $db_conn);
            if ($team_id == 0) {
                returnError("A problem was encountered when creating your team. Please contact the Start CTF administrators.");
            }
            $join_team['new_team'] = true;
        } else {
            // Check to see if the team password matches with the entered one
            $team_id = $team_exists[0]['id'];
            if (strcmp(hash("sha512", $team_exists[0]['join_code']), $team_password) == 0) {
                $join_team['password_check'] = true;
                if ($team_exists[0]['member_2'] == 0) {
                    $join_team['member_2_open'] = true;
                } else {
                    if ($team_exists[0]['member_3'] == 0) {
                        $join_team['member_3_open'] = true;
                    } else {
                        returnError("The team you wish to join is full. Either form a new team or talk to the team administrator about team rearragement.");
                    }
                }
            } else {
                returnError("Team password entered is not correct. Please contact your team's administrator for your team's join code.");
            }
        }

        $random_salt = hash("sha512", uniqid(openssl_random_pseudo_bytes(16), TRUE));
        $user_id = $login_conn->add_item(array($first_name, $last_name, $team_id, $email, $password, $random_salt));

        if (!$user_id) {
            returnError("Unable to register user due to a database error, please contact a competition official about this problem.");
        }

        if ($join_team['new_team']) {
            $db_conn->set_table("teams");
            $db_conn->edit_item($user_id, "member_1", $team_id);
            returnSuccess();
        } else {
            if ($join_team['password_check']) {
                if ($join_team['member_2_open']) {
                    $db_conn->edit_item($user_id, "member_2", $team_exists[0]['id']);
                    returnSuccess();
                } else {
                    if ($join_team['member_3_open']) {
                        $db_conn->edit_item($user_id, "member_3", $team_exists[0]['id']);
                        returnSuccess();
                    } else {
                        returnError("The team you wish to join is full. Either form a new team or talk to the team administrator about team rearragement.");
                    }
                }
            } else {
                returnSuccess();
            }
        }

    } else {
        returnError("Email entered is already being used by someone else. Please use a different one.");
    }
} else {
    returnError("Not all fields were set.");
}

function returnError($error) {
    $return_data["error"] = $error; 
    echo json_encode($return_data);
    exit;
}

function returnSuccess() {
    $return_data["registered"] = true; 
    echo json_encode($return_data);
    exit;
}
?>
