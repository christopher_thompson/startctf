<?php
require_once '../php/Require.php';
$allow_registration = true;
if (!check_page_access("allow_registration")) {
    $allow_registration = false;
}
$loggedIn = login_check();
echo html_begin_setup($loggedIn);
if (!$allow_registration) {
    echo html_error("Registration is currently not open");
} else {
    if (!$loggedIn) {
        echo <<<HTML
    <script type="text/javascript" src="../js/register.js"></script>
    <script type="text/javascript" src="../js/sha512.js"></script>
    <div class="container well">
        <div class="container" id="successfulRegistrationPane">
            <div id="successTitle" style="display:none">
                <center>
                    <div class="jumbotron page-title">
                        <h1>You successfully registered for Start CTF!</h1>
                    </div>
                </center>
            </div>
        </div>
        <div class="container" id="registrationForm">
        <div class="jumbotron page-title" style="padding-bottom:0px;padding-top:0px;">
            <h1>Register for Start CTF</h1>
            <p>Make an account in order to compete in the CTF competition</p>
        </div>
        <div class="col-lg-6">
            <form class="form-horizontal">
                <fieldset>
                    <legend>Create an Account</legend>
                    <div class="form-group">
                        <label for="inputFirstName" class="col-lg-2 control-label">First Name</label>
                        <div class="inputField col-lg-10">
                            <input type="text" onfocus="updateHelpPane('firstName');" class="form-control" id="inputFirstName" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputLastName" class="col-lg-2 control-label">Last Name</label>
                        <div class="inputField col-lg-10">
                            <input type="text" onfocus="updateHelpPane('lastName');" class="form-control" id="inputLastName" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                        <div class="inputField col-lg-10">
                            <input type="email" onfocus="updateHelpPane('email');" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                        <div class="inputField col-lg-10">
                            <input type="password" onfocus="updateHelpPane('password');" class="form-control" id="inputPassword" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputRetypePassword" class="col-lg-2 control-label">Retype Password</label>
                        <div class="inputField col-lg-10">
                            <input type="password" onfocus="updateHelpPane('retypePassword');" class="form-control" id="inputRetypePassword" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTeam" class="col-lg-2 control-label">Team</label>
                        <div class="inputField col-lg-10">
                            <input type="text" onfocus="updateHelpPane('team');" class="form-control" id="inputTeam" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2">
                        </div>
                        <div class="inputField col-lg-10">
                            <button id="joiningTeamButton" class="btn btn-primary">Joining a team?</button>
                        </div>
                    </div>
                    <div id="joinCodeContainer" style="display:none" class="form-group">
                        <label for="inputTeamPassword" class="col-lg-2 control-label">Join Code</label>
                        <div class="inputField col-lg-10 has-success">
                            <input type="text" onfocus="updateHelpPane('teamPassword');" class="form-control" id="inputTeamPassword" placeholder="">
                        </div>
                    </div>
                    <img src="../images/lolCaptcha.png" style="margin-left:80px;margin-bottom:20px" />
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button id="submitButton" onclick="processForm(); return false;" class="btn btn-default" disabled>Submit</button>
                            <button class="btn btn-default" onclick="location.href='../'">Cancel</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <div id="registerError">
            </div>
        </div>
        <div id="formHelp" class="col-lg-6">
            <h1 id="formHelpTitle"></h1>
            <div id="formHelpContent">
                <div id="firstNameHelp" style="display: none;"><p>Here we are asking you to enter your first name. This can be typically found in your immediate memories as it is one of the most vital forms of identification that you have. Now if you have a name such as Jackson or Sophia, then you might be in trouble if you only go by your first name as these are the most used first names in 2013. This is where your last name comes into play.</p><hr /></div>
                <div id="lastNameHelp" style="display: none;"><p>If you would be so inclined to help us out and provide your last name as an additional means of identification, we would be eternally grateful.</p><hr /></div>
                <div id="emailHelp" style="display: none;"><p>For our primary form of communcation with you, we are going to be using email to keep you up to date with the latest and greatest information about this competition. <strong>Remeber this email because you will be using it to login to your account!</strong> In case you do not have an account, Google offers a great free service that you can access <a target="_blank" href="http://mail.google.com">here.</a> An email typically looks something like this: username@email.com (Just a FYI for you).</p><hr /></div>
                <div id="passwordHelp" style="display: none;"><p>This is a very important step in your account creation process as we need to make sure your account is only going to accessed by you and not a hacker. A "good" password is one which is both easy to remember and hard to guess. A good technique for creating a password (as stated by <a target="_blank" href="https://xkcd.com/936/">XKCD</a>) is to take four random words and stick them together. While this a good technique, it would still be easy to <a target="_blank" href="http://nakedsecurity.sophos.com/2013/08/16/anatomy-of-a-brute-force-attack-how-important-is-password-complexity/">crack</a>. This is where I would suggest substituting a few letters with uppercase or their <a target="_blank" href="http://simple.wikipedia.org/wiki/Leet">leet</a> equivalent.</p><hr /></div>
                <div id="retypePasswordHelp" style="display: none;"><p>Here we are asking you to type the password that you entered in the previous box again to make sure you know what it is. Just so you know, if you forget your password at anytime, you can always go to the login screen and click "Forgot password" and we will send you an email to reset it.</p><hr /></div>
                <div id="teamHelp" style="display: none;"><p>We are going to next ask you to enter in a name of a team you wish to join or wish to create. As you type your team name, we will check our list of teams to check the availability of your team name. If you know the name of the team you want to join, but do not remember exactly what it is, type what you remember and check the teams that match closest to what you are typing. If you create a new team, you will have to go to your account settings to manage the people who are to also be on your team.</p><hr />
                <button id="teamCheckButton" type="button" class="btn btn-primary" style="margin-bottom:15px">Team Name Check</button>
                <div id="teamCheckNotification">
                </div>
                </div>
                <div id="teamPasswordHelp" style="display: none;"><p>In order to prevent users who are not a part of a team joining the team, we implemented this password system. <strong>If you are creating a new team, you do not need to create a password for the team, one will be generated for you</strong>. If you are attempting to join a team however, you must enter the password exactly as your team's administrator told you (if he / she has not given you a password, ask them to go to their <strong>Account</strong> page and go to <strong>Manage Team</strong> and have them send you the password that is given).</p><hr /></div>
            </div>
        </div>
        </div>
    </div>
HTML;

    } else {
        echo html_error("You are already logged in, you have to log out first if you want to login again :P");
        echo html_info('<a href="../login/logout.php">Log out?</a>');
    }
}

echo html_end_setup();
?>
