<?php
require_once '../php/Require.php';
require_once '../php/ChallengeHandlers.func.php';

$allow_registration = true;
if (!check_page_access("allow_registration")) {
    $allow_registration = false;
}
$return_data = array('registered' => false, 'error' => "");

if ($allow_registration && isset($_POST['team_id'])) {
} else {
    returnError("Fields not set");
}

function returnError($error) {
    $return_data["error"] = $error; 
    echo json_encode($return_data);
    exit;
}

function returnSuccess() {
    $return_data["registered"] = true; 
    echo json_encode($return_data);
    exit;
}
