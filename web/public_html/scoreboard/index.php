<?php
require_once '../php/Require.php';
$enable_scoreboard = true;
if (!check_page_access("enable_scoreboard")) {
    $enable_scoreboard = false;
}
$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>

<div class="container well">
    <div class="jumbotron page-title" style="padding-bottom:0px;padding-top:0px;">
        <h1>Scoreboard</h1>
    </div>
    <div class="row-fluid">
    <?php
        if (!$enable_scoreboard) {
            echo html_error("The scoreboard is currently disabled");
        } else {
            if ($loggedIn) {
            }
            $dbConn = new DatabaseConn();
            $dbConn->set_table("teams");
            $teams = $dbConn->get_all_items("score");
            echo <<<HTML
<div class="row12">
<div class="table-responsive">
<table id="scoreboard" class="table table-striped table-hover">
    <tbody>
        <tr>
            <th>Place</th>
            <th>Name</th>
            <th>Score</th>
        </tr>
HTML;
            $place = 1;
            foreach ($teams as $team) {
                $team['name'] = htmlentities(stripslashes($team['name']));
                echo <<<HTML
        <tr>
            <td>$place</td>
            <td>{$team['name']}</td>
            <td>{$team['score']}</td>
        </tr>
HTML;
                $place++;
            }
            echo <<<HTML
    </tbody>
</table>
</div>
</div>
HTML;
        }
    ?>
    </div>
</div>
<?php
    echo html_end_setup();
?>
