<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="../js/shell.js"></script>
<div class="container well">
    <div class="row-fluid">
            <div class="span4">
                <div class="jumbotron page-title">
                    <h1>Shell</h1>
                </div> 
            </div>
            <?php
                $cookie_error = false;
                $shell_login = "";
                if ($loggedIn) {
                    if ($info = get_stored_credentials()) {
                        $db_conn = new DatabaseConn();
                        $login_conn = new DatabaseConn(false, 'secure_login');
                        $login_conn->set_table("members");
                        $user_id = $info['user_id'];
                        $result = $login_conn->get_item($user_id, "id");
                        $user = $result[0];
                        $db_conn->set_table("shell_credentials");
                        $result = $db_conn->get_item($user['team_id'], "team_id");
                        $creds = $result[0];
                        $shell_login = "ssh ".$creds['shell_user']."@startctf.com";
                        echo <<<HTML
            <div class="span4 offset4">
                <h4>Shell Credentials</h4>
                <h5>Username: {$creds['shell_user']}</h5>
                <h5>Password: {$creds['shell_pass']}</h5>
            </div>
HTML;
                    } else {
                        $cookie_error = true;
                    }
                }
            ?>
        </div>
        <div class="row-fluid">
            <?php
                if ($loggedIn) {
                    if (!$cookie_error) {
                        echo <<<HTML
                    <div class="row-fluid">
                        <div class="col-lg-12">
                            <div>
                                 <iframe style="width:100%;height:700px;" src="https://startctf.com:10443"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <h3>Using the shell</h3>
                            <p style="word-wrap:break-word;text-align:justify;">Begin by clicking on the SSH button and entering in the Username and Password found to the right under "Shell Credentials". If you want a bigger shell screen, you can go <a target="_blank" href="https://startctf.com:10443">fullscreen</a>. If you have never used a shell before, you can go over to our tutorial <a target="_blank" href="http://startctf.com/learn/?page=shell">to learn what a shell is</a>.</p>
                        </div>
                        <div class="span6">
                            <h3>Shell Login</h4>
                            <p style="word-wrap:break-word;text-align:justify;">
                                If you do not want to use our shell (even though it is so awesome) you can use other shell clients like <a target="_blank" href="http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html">PuTTY</a> or Linux's ssh command by using the same credentials at the top of the page. (For using a command like ssh client you would type: $shell_login then enter your password)
                            </p>
                        </div>
                    </div>
HTML;
                } else {
                    echo html_error("It appears the cookie monster got a hold of your cookies. Try to login again.");
                }
            } else {
                echo html_error("You cannot access the shell unless you are logged in. You can login <a href=\"../login\">here</a>");
            }
        ?>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
