<?php
    require_once '../../php/Require.php';
    require_once '../../php/ChallengeHandlers.func.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        if ($info = get_stored_credentials()) {
            $flag = get_challenge_solution($info['user_id'], 'javacrypt_2');
            echo <<<HTML
<HTML>
<HEAD>
<TITLE>Javacrypt 2</TITLE>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript">
function part1(input){
	var num1 = (input&7356);
	var num2 = (input||2342);
	var num3 = (input^452);
	return num1.toString().concat(num2.toString().concat(num3.toString()));
}

function part2(input){
	var num1 = (input*7126%2356);
	var num2 = (input||2342 % 134);
	var num3 = (input^234&124);
	return num1.toString().concat(num2.toString().concat(num3.toString()));
}

function testResults (form) {


    var TestVar = form.inputbox.value;
	if(TestVar == part1(243331).concat(part2(5234))) alert($flag);
	
}
</SCRIPT>
</HEAD>
<BODY>

<p> You know the drill.... </p>

<FORM NAME="myform" ACTION="" METHOD="GET">Enter something in the box: <BR>
<INPUT TYPE="text" NAME="inputbox" VALUE=""><P>
<INPUT TYPE="button" NAME="button" Value="Click" onClick="testResults(this.form)">
</FORM>
</BODY>
</HTML>
HTML;
        } else {
            echo '<h1>Unable to get user credentials for the challenge. Please login again</h1>';
        }
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
