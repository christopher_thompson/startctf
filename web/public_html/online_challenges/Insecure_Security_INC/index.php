
<?php
    require_once '../../php/Require.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        echo <<<HTML
<html>
<head>
    <link type="text/css" rel="stylesheet" href="styles/main-style.css"></link>
    <script type="text/javascript" src="scripts/hello.js"></script>
</head>
<body>

<center><h1>Insecure Security INC.</h1>
<h4>Giving the world dangerously low quality technology security since Y2K</h4>
</center>
<center><p class="main-content">We have a whole lot of examples for our horrible security practices and you can see this for yourself in the images below.</p></center>

<a href="view_image.php?page=images/crack_passwords.html"><img style="width:250px;margin:15px" title="Cracked Passwords" src="images/cracked-passwords.jpg" /></a>
<a href="view_image.php?page=images/xss.html"><img style="width:250px;margin:15px" title="Cross Site Scripting" src="images/xss.gif" /></a>
<a href="view_image.php?page=images/remote_file_inclusion.html"><img style="width:250px;margin:15px;" title="Remote File Inclusion" src="images/remote-file.jpg" /></a>
<a href="view_image.php?page=images/sql_injection.html"><img style="width:250px;margin:15px" title="SQL Injection" src="images/sql_injection.gif" /></a>

</body>
</html>
HTML;
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
