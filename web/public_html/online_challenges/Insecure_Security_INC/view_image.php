<?php
    require_once '../../php/Require.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        $page = $_GET['page'];
        $allowed_pages = array("images/crack_passwords.html", "images/remote_file_inclusion.html", "images/sql_injection.html", "images/xss.html", 
                                "scripts/hello.js", "scripts/ping.php", "styles/main-style.css", "index.php", ".htpasswd");
        $notice = "";
        if (in_array($page, $allowed_pages)) {
            
        } else if (strcmp($page, "s3cr3t_password_file.php") == 0) {
            $notice = "<h1>Uh oh, it looks like you found our secret password file. Good thing we added that login page so no one could access it without the credentials. But... I don't think our boss gave the page a really secure password... I hope you don't know anything about password cracking...</h1>";
        } else {
            $notice = "<h1>The page you requested either does not exist, or is not allowed by our security configuration</h1>";
        }
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
        exit;
    }
?>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="styles/main-style.css"></link>
</head>
<body>
<center><div class="main-content">
<?php
    if (empty($notice)) {
        include($page);
    } else {
        echo $notice;
    }
?>
</div></center>
</body>
</html>
