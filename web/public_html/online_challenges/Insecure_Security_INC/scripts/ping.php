<?php
    require_once '../../../php/Require.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        $output = array();
        $ipAddress = "";
        $display_output = "";
        if (isset($_POST['ipAddress'])) {
            $ipAddress = substr($_POST['ipAddress'], 0, 255);
            $delimiters = array("&&", "||", ";");
            $ready = str_replace($delimiters, $delimiters[0],$ipAddress);
            $parsed = explode($delimiters[0], $ready);
            $invalidIP = false;
            if (preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', trim($parsed[0]))) {
                exec("ping -W 2 -c 1 " . trim($parsed[0]), $output);
            } else {
                $output[] = "Invalid IP Address given";
                $invalidIP = true;
            }

            if (!$invalidIP && count($parsed) > 1) {
                $foundCommand = false;
                $command_to_run = "";
                foreach($parsed as $command) {
                    $parsed_command = explode(" ", trim($command));
                    if (count($parsed_command) > 0 && strcmp(trim($parsed_command[0]), "ls") == 0) {
                        $foundCommand = true;
                        $exe_command = "ls";
                        $params = "";
                        $path = "";
                        if (strpos($command, "../")) {
                            $path = " ../";
                        }
                        $result = preg_match('/\\-l?a?/', trim($command), $matches);
                        if ($result && strcmp($matches[0], "-") != 0) {
                            $params = " " . $matches[0];
                        }
                        $command_to_run = $exe_command . $params . $path;
                        break;
                    }
                }
                if ($foundCommand && count($parsed) > 1) {
                    $lsOutput = array();
                    exec("$command_to_run", $lsOutput);
                    $output[] = "\n" . join($lsOutput, "\n");
                } else if (count($parsed) > 1) {
                    $output[] = "You are on the right track, but due to security reasons, we can only accept a certain command.";
                }
            }
            $display_output = join("\n", $output);
        }
            echo <<<HTML
<html>
<head>
    <link type="text/css" rel="stylesheet" href="../styles/main-style.css"></link>
</head>
<body>
<center><h1>Super secret ping command</h1></center>
<p>Ping is a computer network tool used to test whether a particular host is reachable across an IP network. In case I need to check on the status of one of my computers, I have this nifty little hidden program for my use. I hope no one finds this because I thought that keeping this hidden was enough security...</p>
<form method="POST">
<label for="ipAddress">$ ping </label>
<input type="text" name="ipAddress" value="$ipAddress"></input>
</form>
<p>Command Output:</p>
<textarea style="width:400px; height:300px;" type="text" id="output">$display_output</textarea>
</body>
</html>
HTML;
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>


<?php
?>
