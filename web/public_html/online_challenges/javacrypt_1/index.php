<?php
    require_once '../../php/Require.php';
    require_once '../../php/ChallengeHandlers.func.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        if ($info = get_stored_credentials()) {
            $flag = get_challenge_solution($info['user_id'], 'javacrypt_1');
            $char_array = str_split($flag);
            $crypt_array = array();
            foreach ($char_array as $char) {
                $crypt_array[] = ord($char) - 42;
            }
            $crypt_flag = "[" . join(", ", $crypt_array) . "]";
            echo <<<HTML
<HTML>
<HEAD>
<TITLE>Javacrypt 1</TITLE>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript">

function part1(input){
	return input+1;
}


function testResults (form) {
    var cryptVar = form.inputbox.value;
    var cryptArray = $crypt_flag;
    var decrypt = [];
    for (i = 0; i < cryptArray.length; i++) {
        decrypt.push(cryptArray[i] + parseInt(part1(cryptVar), 10));
    }
    document.getElementById("output").innerHTML = decrypt.join(" ");
}
</SCRIPT>
</HEAD>
<BODY>
<h1 id="output"></h1>
<p> Figure out the password and get the flag! </p>

<FORM NAME="myform" ACTION="" METHOD="GET">Enter something in the box: <BR>
<INPUT TYPE="text" NAME="inputbox" VALUE=""><P>
<INPUT TYPE="button" NAME="button" Value="Click" onClick="testResults(this.form)">
</FORM>
</BODY>
</HTML>
HTML;
        } else {
            echo '<h1>Unable to get user credentials for the challenge. Please login again</h1>';
        }
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
