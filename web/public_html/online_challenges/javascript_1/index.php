<?php
    require_once '../../php/Require.php';
    require_once '../../php/ChallengeHandlers.func.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        if ($info = get_stored_credentials()) {
            $flag = get_challenge_solution($info['user_id'], 'javascript_1');
            echo <<<HTML
<html><head>
	<title>Javascript 1</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script language="javascript">
        var msg = "To understand what recursion is, you first must understand recursion.";
        var a=new Array(),n="";
        var a = msg.split("");
        function one()
        {
            t=document.f.txt.value;
            j=t.length;
            if(j>0)
            {
                for(var i=1;i<=j;i++)
                {
                    n=n+a[i - 1]
                    if(i==a.length)
                    {
                        document.f.txt.value="";
                        n="";
                    }
                }
            }
            document.f.txt.value=n
            n=""
            setTimeout("one()",1)
        }
        function on()
        {
            one()
        }
    </script>
</head>
<body onload="on()">
<center>
<h2>*WARNING* Pointless Text Area</h2>
<form name="f">
<textarea name="txt" cols="20" rows="5"></textarea>
<h1 style="display:none">The answer to this challenge is not: "$flag"</h1>
</form></center>


</body></html>
HTML;
        } else {
            echo '<h1>Unable to get user credentials for the challenge. Please login again</h1>';
        }
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
