<?php
    require_once '../../php/Require.php';
    require_once '../../php/ChallengeHandlers.func.php';
    $loggedIn = login_check();

    // Get flag for the user logged in
    if ($loggedIn) {
        if ($info = get_stored_credentials()) {
            $flag = get_challenge_solution($info['user_id'], 'bbnt_bank');
            if (isset($_GET['first_name'], $_GET['last_name'], $_GET['email'], $_GET['address'], $_GET['state'], $_GET['zip_code'], $_GET['country'], $_GET['cell_phone'], $_GET['credit_card'])) {
                if (!preg_match('/^[a-zA-Z]*$/', $_GET['first_name'])) {
                    echo "<h1>Invalid First Name</h1>";
                }
                if (!preg_match('/^[a-zA-Z]*$/', $_GET['last_name'])) {
                    echo "<h1>Invalid Last Name</h1>";
                }
                if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_GET['email'])) {
                    echo "<h1>Invalid Email</h1>";
                }
                if (!preg_match('/^[a-zA-Z0-9]*$/', $_GET['address'])) {
                    echo "<h1>Invalid Address</h1>";
                }
                if (!preg_match('/^[a-zA-Z]*$/', $_GET['state'])) {
                    echo "<h1>Invalid State</h1>";
                }
                if (!preg_match('/^[0-9]*$/', $_GET['zip_code'])) {
                    echo "<h1>Invalid Zip Code</h1>";
                }
                if (!preg_match('/^[a-zA-Z]*$/', $_GET['country'])) {
                    echo "<h1>Invalid Country</h1>";
                }
                if (!preg_match('/^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$/', $_GET['cell_phone'])) {
                    echo "<h1>Invalid Cell Phone</h1>";
                }
                if (!preg_match('/^[0-9]*$/', $_GET['credit_card'])) {
                    echo "<h1>Invalid Credit Card</h1>";
                }
                echo "all are set";
            }
            echo <<<HTML
<html>
<head>
<link type="text/css" rel="stylesheet" href="styles/main-style.css"></link>
</head>
<body>
<div class="main-content">
<center><h1>Welcome to BBNT Bank</h1></center>
    <center>
    <form class="form-horizontal" method="GET">
        <fieldset>
            <legend>Create an Account</legend>
            <div class="form-group">
                <label for="first_name" class="col-lg-2 control-label">First Name</label>
                <div class="inputField col-lg-10">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-lg-2 control-label">Last Name</label>
                <div class="inputField col-lg-10">
                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-lg-2 control-label">Email</label>
                <div class="inputField col-lg-10">
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-lg-2 control-label">Address</label>
                <div class="inputField col-lg-10">
                    <input type="text" class="form-control" name="address" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label for="state" class="col-lg-2 control-label">State</label>
                <div class="inputField col-lg-10">
                    <input type="text" class="form-control" name="state" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label for="zip_code" class="col-lg-2 control-label">Zip Code</label>
                <div class="inputField col-lg-10">
                    <input type="text" class="form-control" name="zip_code" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label for="country" class="col-lg-2 control-label">Country</label>
                <div class="inputField col-lg-10 has-success">
                    <input type="text" class="form-control" name="country" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label for="cell_phone" class="col-lg-2 control-label">Cell Phone</label>
                <div class="inputField col-lg-10 has-success">
                    <input type="text" class="form-control" name="cell_phone" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label for="credit_card" class="col-lg-2 control-label">Credit Card</label>
                <div class="inputField col-lg-10 has-success">
                    <input type="text" class="form-control" name="credit_card" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button name="submitButton" class="btn btn-default">Submit</button>
                </div>
            </div>
        </fieldset>
    </form>
    </center>
</div>
</body>
</html>
HTML;
        } else {
            echo '<h1>Unable to get user credentials for the challenge. Please login again</h1>';
        }
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
