<?php
    require_once '../../php/Require.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        echo <<<HTML
<!DOCTYPE html>
<html><head>
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script> 
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
		<title>Javascript 2</title>
	</head>
	<body>
		<script type="text/javascript">
            var xmlhttp;
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            } else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); 
            }

            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var password = prompt("Password: ");
                    var json = jQuery.parseJSON(xmlhttp.responseText);
                    if(password == json.password) {
                        document.write("<h1 style='color:green'>" + json.flag + "</h1>");
                    }
                    else {
                        document.write("Access denied");
                    }
                }
            }

            xmlhttp.open("GET", "get_password.php", false);
            xmlhttp.send();

		</script>
	

</body></html>
HTML;
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
