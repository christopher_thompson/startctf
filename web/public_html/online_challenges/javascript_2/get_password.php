<?php
    require_once '../../php/Require.php';
    require_once '../../php/ChallengeHandlers.func.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        if ($info = get_stored_credentials()) {
            $json = array();
            $flag = get_challenge_solution($info['user_id'], 'javascript_2');
            $json['flag'] = $flag;
            $json['password'] = 'S3curePa$$word';
            echo json_encode($json);
        } else {
            echo 'Unable to get flag, please login again.';
        }
    } else {
        echo 'Unable to get flag, please login.';
    }
?>
