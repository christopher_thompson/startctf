<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
		<title>HTML 1</title>
	</head>

	<body>
<?php
    require_once '../../php/Require.php';
    require_once '../../php/ChallengeHandlers.func.php';
    $loggedIn = login_check();
    // Get flag for the user logged in
    if ($loggedIn) {
        if ($info = get_stored_credentials()) {
            $flag = get_challenge_solution($info['user_id'], 'html_1');
            // Get user flag
            if (isset($_POST['user'], $_POST['passw'])) {
                if (strcmp($_POST['user'], "Admin") == 0 && strcmp($_POST['passw'], "startCTF1sthebest") == 0) {
                    echo "<h1 style='color: green'>$flag</h1>";
                } else {
                    echo "<h1 style='color: red'>Given credentials were incorrect.</h1>";
                }
            } else {
                echo "<h1 style='color: red'>No credentials were given.</h1>";
            }
            echo <<<HTML
        <h1>Login</h1>
		<form action="" method="POST">
			User
			<select name="user">
				<option selected="selected" value="Alice">Alice</option>
				<option value="Bob">Bob</option>
				<option value="Eve">Eve</option>
				<option value="Smitterwerbenjegermanjenson">Smitterwerbenjegermanjenson</option>
				<option value="WhatsItToYa">WhatsItToYa</option>
				<option value="SpongeBobSquarePants">SpongeBobSquarePants</option>
			</select>
			<br>
			Password <input name="passw" type="password" disabled>
			<br>
			<input name="Submit" value="Submit" type="submit">
        </form>
		<p style="color:white"><span>Just so I don't forget: "Admin" account has the password: "startCTF1sthebest"</span></p>
</body></html>
HTML;
        } else {
            echo '<h1>Unable to get user credentials for the challenge. Please login again</h1>';
        }
    } else {
        echo '<h1>You are not currently logged in. Please login before accessing this challenge.</h1>';
    }
?>
