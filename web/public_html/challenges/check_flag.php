<?php
require_once '../php/Require.php';

$allow_submissions = true;
if (!check_page_access("allow_submissions")) {
    $allow_submissions = false;
}
    if ($allow_submissions && isset($_POST['program_id'], $_POST['flag']) && preg_match('/^[a-zA-Z0-9_]*$/', $_POST['program_id'].$_POST['flag'])) {
        $program_id = $_POST['program_id'];
        $flag = $_POST['flag'];
        $loggedIn = login_check();
        if ($loggedIn && $user_info = get_stored_credentials()) {
            if (!preg_match('/^[a-zA-Z0-9_]*$/', $program_id) || !preg_match('/^[a-zA-Z0-9_]*$/', $flag)) {
                returnError($program_id, "Given parameters were incorrect, are you trying to hack us? ;)");
            }
            $db_conn = new DatabaseConn(false);
            $login_conn = new DatabaseConn(false, 'secure_login');
            $login_conn->set_table('members');
            if (!($user_id = $user_info['user_id']))
                    returnError($program_id, "Unable to check your flag.");
            $user_array = $login_conn->get_item($user_id, "id");
            $user = $user_array[0];
            if ($user['team_id'] != 0) {
                $db_conn->set_table("teams");
                $result = $db_conn->get_item($user['team_id'], "id");
                if (!($team = $result[0]))
                    returnError($program_id, "Unable to check your flag.");

                $db_conn->set_table("challenge_solutions");
                $result = $db_conn->get_item($team['id'], "team_id");
                if (!($team_solutions = $result[0]))
                    returnError($program_id, "Unable to check your flag.");
                
                $result = $db_conn->query("SELECT $program_id FROM challenge_teams WHERE team_id = {$team['id']}");
                if (!$result[0])
                    returnError($program_id, "You have already solved this challenge. :D");

                if (array_key_exists($program_id, $team_solutions) && strcmp($team_solutions[$program_id], $flag) == 0) {
                    $db_conn->set_table("challenge_list");
                    $result = $db_conn->get_item($program_id, "prog_id");
                    $challenge = $result[0];
                    $db_conn->set_table("teams");
                    $score = intval($team['score']) + (intval($challenge['points']) + intval($challenge['difficulty']) * 1000);
                    $db_conn->edit_item($score, "score", $team['id']);
                    $db_conn->set_table("challenge_teams");
                    $db_conn->edit_item(1, $program_id, $team['id'], "team_id");
                    returnSuccess($program_id, $challenge['title'], $score);
                } else {
                    returnError($program_id, "Sorry, your flag was incorrect.");
                }
            } else {
                returnError($program_id, "User does not have a team.");
            }
        } else {
            returnError($program_id, "Unable to get user account.");
        }
    } else {
        returnError("", "No flag given");
    }

function returnError($program_id, $error) {
    $return_data["error"] = $error; 
    $return_data["id"] = $program_id; 
    echo json_encode($return_data);
    exit;
}

function returnSuccess($program_id, $title, $score) {
    $return_data["solved"] = $program_id;
    $return_data["title"] = $title;
    $return_data["score"] = $score;
    echo json_encode($return_data);
    exit;
}
?>
