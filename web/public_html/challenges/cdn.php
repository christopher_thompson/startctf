<?php
require_once '../php/Require.php';
require_once '../php/ChallengeHandlers.func.php';

$allow_submissions = true;
if (!check_page_access("allow_submissions")) {
    $allow_submissions = false;
}
    if ($allow_submissions && isset($_GET['challenge']) && preg_match('/^[a-zA-Z0-9_]*$/', $_GET['challenge']) && login_check() && $info = get_stored_credentials()) {
        $login_conn = new DatabaseConn(false, 'secure_login');
        $login_conn->set_table("members");
        $user_id = $info['user_id'];
        $team = array();

        $result = $login_conn->get_item($user_id, "id");
        if (!$user = $result[0]) {
            exit;
        }

        if ($user['team_id'] != 0) {
            $db_conn->set_table("teams");
            $result = $db_conn->get_item($user['team_id'], "id");
            if (!$team = $result[0]) {
                exit;
            }
        } else {
            exit;
        }

        $file = '/home/user_' . $team['id'] . '/' . '/$topic/__Get_file_name__';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    } else {
        exit;
    }
?>
