<?php
require_once '../php/Require.php';
$allow_submissions = true;
if (!check_page_access("allow_submissions")) {
    $allow_submissions = false;
}
$loggedIn = login_check();
echo html_begin_setup($loggedIn);

$online_challenge_urls = array(
    "javascript_1" => "../online_challenges/javascript_1/",
    "javascript_2" => "../online_challenges/javascript_2/",
    "html_1" => "../online_challenges/html_1/",
    "flying_helix" => "../online_challenges/flying_helix/",
    "bbnt_bank" => "../online_challenges/BBNT_Bank/",
    "insecure_security" => "../online_challenges/Insecure_Security_INC/",
    "javacrypt_1" => "../online_challenges/javacrypt_1/",
    "javacrypt_2" => "../online_challenges/javacrypt_2/"
);
?>
<link type="text/css" rel="stylesheet" href="../css/challenge.css?v=2">
<script type="text/javascript" src="../js/challenge.js"></script>

<div class="container well">
    <div class="jumbotron row-fluid page-title">
        <h1 class="span8" style="text-align:left">Challenges</h1>
        <?php
            $db_conn = new DatabaseConn(false);
            $user_info = array();
            if ($loggedIn && $user_info = get_stored_credentials()) {
                $login_conn = new DatabaseConn(false, 'secure_login');
                $login_conn->set_table('members');
                $user_id = $user_info['user_id'];
                $user_array = $login_conn->get_item($user_id, "id");
                $user = $user_array[0];
                if ($user['team_id'] != 0) {
                    $db_conn->set_table("teams");
                    $team_array = $db_conn->get_item($user['team_id'], "id");
                    $team = $team_array[0];
                    $score = $team['score'];
                    echo <<<HTML
        <div class="span4"><h2 style="position:relative;top:20px;">Team's Score: <div style="display:inline" id="score">$score</div></h2></div>
HTML;
                }
            }
        ?>
    </div>
    <hr />
    <div class="container-fluid">
        <div class="row-fluid">
        <?php
        if (!$allow_submissions) {
            echo html_error("Challenge submission is currently not allowed");
        } else {
            if ($loggedIn) {
                $db_conn->set_table('challenge_list');
                $challenges = $db_conn->get_all_items("prog_id");
                if(!$challenges) {
                    echo html_info('No challenges yet.');
                    exit;
                } else {
                    $topics = $db_conn->query("SELECT DISTINCT topic FROM challenge_list");
                    $db_conn->set_table('challenge_teams');
                    $result = $db_conn->get_item($user['team_id'],"team_id");
                    $solved_challenges = $result[0];
                    $sorted_challenges = array();
                    foreach ($challenges as $challenge) {
                        $sorted_challenges[$challenge['topic']][] = $challenge;
                    }
                    $num_of_challenges = 0; 
                    foreach ($sorted_challenges as $key => $topic) {
                        if ($num_of_challenges == 0) {
                            echo '<div class="row-fluid">';
                        }
                        $topic_name = ucwords(str_replace('_', ' ', $key));
                        echo <<<HTML
                        <div class="topic-container span6">
                            <div class="panel panel-default">
                                <center><div class="panel-heading" style="font-size:140%;padding-bottom:0px">$topic_name</div></center>
                                <div class="panel-body">
                                    <div class="list-group">
HTML;
                            for ($i = 0; $i < count($topic); $i++) {
                                $smallest = $i;
                                $smallest_points = intval($topic[$i]['points']) + ($topic[$i]['difficulty'] * 1000);
                                for ($j = $i; $j < count($topic); $j++) {
                                    $temp_points = intval($topic[$j]['points']) + ($topic[$j]['difficulty'] * 1000);
                                    if ($temp_points < $smallest_points) {
                                        $smallest = $j;
                                        $smallest_points = $temp_points;
                                    }
                                }
                                $temp = $topic[$i];
                                $topic[$i] = $topic[$smallest];
                                $topic[$smallest] = $temp;
                            }

                            foreach ($topic as $challenge) {
                                $program_id = $challenge['prog_id'];
                                $title = $challenge['title'];
                                $points = intval($challenge['points']) + ($challenge['difficulty'] * 1000);
                                $description = nl2br(strip_tags(html_entity_decode(stripcslashes($challenge['description']))));
                                $hint = $challenge['hint'];
                                $solved_challenge = false;
                                if ($solved_challenges[$program_id])
                                    $solved_challenge = true;

                                $solved_list_item = "";
                                $flag_input = '<div class="input-group"><input class="form-control" type="text"></input><span class="input-group-btn"><button id="'.$program_id.'" class="btn btn-default flag-submit" type="button">Submit</button></span></div>';
                                $solved_html = "";
                                $unsolved_offset = "offset2";
                                if ($solved_challenge) {
                                    $solved_html = "<span class=\"span2\">Solved!</span>";
                                    $unsolved_offset = "";
                                    $solved_list_item = " solved-challenge";
                                    $flag_input = "";
                                }

                               echo <<<HTML
                                <div class="list-group-item$solved_list_item">
                                    <div class="list-group-item-heading" style="font-size:120%">
                                        <div class="row-fluid" style="padding-right:10px">
                                            <h5 class="span12 challenge-title">
                                                <span class="span8">
                                                    <span class="glyphicon glyphicon-chevron-right" style="float:left;margin-right:10px;"></span>
                                                    $title
                                                </span>
                                                $solved_html
                                                <span class="challenge-points span2 $unsolved_offset">$points</span>
                                            </h5>
                                        </div>
                                    </div>
                                    <p class="list-group-item-text" style="font-size:120%">
                                        $description
HTML;
                                if (strcmp($key, 'web_applications') == 0) {
                                $url = $online_challenge_urls[$program_id];
                                echo <<<HTML
                                        <br /><br />
                                        Go to Challenge:&nbsp&nbsp<a target="_blank" href="$url">here</a>
                                    </p>
                                    $flag_input
                                </div>
HTML;
                                } else {
                                echo <<<HTML
                                        <br /><br />
                                        Download Challenge:&nbsp&nbsp<a href="cdn.php?$program_id">here</a>
                                        <br />
                                        Accessible on the shell at: <code>/challenges/$program_id</code>
                                    </p>
                                    $flag_input
                                </div>
HTML;
                                }
                            }

                            $num_of_challenges++;
                            echo <<<HTML
                                        </div>
                                    </div>
                                </div>
                            </div>
HTML;
                            if ($num_of_challenges == 2) {
                                echo "</div>";
                                $num_of_challenges = 0;
                            }
                    }
                }
            } else {
                echo html_error("You cannot access the challenges unless you are logged in. You can login <a href=\"../login\">here</a>");
            }
        }
        ?>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
