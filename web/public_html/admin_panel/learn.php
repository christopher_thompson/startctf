<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin_panel.css"></link>
<link type="text/css" rel="stylesheet" href="/css/monokai.css"></link>
<link type="text/css" rel="stylesheet" href="/css/codemirror.css"></link>
<script type="text/javascript" src="/js/codemirror.js"></script>
<script type="text/javascript" src="/js/xml.js"></script>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <center>
               <div class="list-group" style="cursor:pointer">
                  <a href="/admin_panel/access" id="access" class="list-group-item">
                    <h4 class="list-group-item-heading">Access</h4>
                  </a>
                  <a href="/admin_panel/challenges" id="challenges" class="list-group-item">
                    <h4 class="list-group-item-heading">Challenges</h4>
                  </a>
                  <a href="/admin_panel/teams" id="teams" class="list-group-item">
                    <h4 class="list-group-item-heading">Teams</h4>
                  </a>
                  <a href="/admin_panel/users" id="users" class="list-group-item">
                    <h4 class="list-group-item-heading">Users</h4>
                  </a>
                  <a href="/admin_panel/updates" id="updates" class="list-group-item">
                    <h4 class="list-group-item-heading">Updates</h4>
                  </a>
                  <a href="/admin_panel/learn" id="learn" class="list-group-item active">
                    <h4 class="list-group-item-heading">Learn</h4>
                  </a>
                </div>
                </center>
            </div>
            <div class="span10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 id="learnHeader" class="panel-header">Learn</h2>
                    </div>
                    <div class="admin-panel-body">
                        <div id="learnPanel">
                            <div id="category-container" class="learn-item-container">
                                <center><h1>Categories</h1></center>
                                <div class="row-fluid">
                                    <div id="cryptography" class="span4">
                                        <h2>Cryptography</h2>
                                    </div>
                                    <div id="exploitation" class="span4">
                                        <h2>Exploitation</h2>
                                    </div>
                                    <div id="forensics" class="span4">
                                        <h2>Forensics</h2>
                                    </div>
                                </div>
                                <div class="row-fluid" style="margin-top:20px">
                                    <div id="operating_systems" class="span4">
                                        <h2>Operating Systems</h2>
                                    </div>
                                    <div id="programming" class="span4">
                                        <h2>Programming</h2>
                                    </div>
                                    <div id="web_applications" class="span4">
                                        <h2>Web Applications</h2>
                                    </div>
                                </div>
                                <hr />
                            </div>
                            <div id="topic-container" class="learn-item-container">
                                <?php
                                    $category_info = array();
                                    $categories = array("cryptography", "exploitation", "forensics", "operating_systems", "programming", "web_applications");
                                    foreach ($categories as $category) {
                                        $category_info[$category] = json_decode(file_get_contents("../learn/categories/$category/category.json"), true);
                                        $category_title = ucwords(str_replace("_", " ", $category));
                                        echo "<div id=\"$category-topics\" class=\"category-topics\">".
                                                "<center><h1>Topics for $category_title</h1></center>".
                                                     "<div class=\"row-fluid\">";
                                        $i = 0;
                                        foreach ($category_info[$category]['topics'] as $topic) {
                                            $topic_title = ucwords(str_replace("_", " ", $topic['title']));
                                            if ($i != 0 && $i % 3 == 0) {
                                                echo "</div>".
                                                     "<div class=\"row-fluid\">";
                                            }
                                            echo <<<HTML
                                            <div id="{$topic['title']}" class="span4">
                                                <h3>$topic_title</h3>
                                                <p>{$topic['description']}</p>
                                            </div>
HTML;
                                            $i++;
                                        }
                                        if ($i % 3 == 0) {
                                                echo "</div>".
                                                     "<div class=\"row-fluid\">";
                                        }
                                        echo "<div class=\"span4\">".
                                            "<h1 class=\"add-item\">+</h1>".
                                        "</div>".
                                        "</div>".
                                        "<hr />".
                                        "</div>";
                                    }
                                ?>
                            </div>
                            <div id="material-container" class="learn-item-container">
                                <?php
                                    foreach ($categories as $category) {
                                        $category_title = ucwords(str_replace("_", " ", $category));
                                        foreach ($category_info[$category]['topics'] as $topic) {
                                            $topic_title = ucwords(str_replace("_", " ", $topic['title']));
                                            $topic_info = json_decode(file_get_contents("../learn/categories/$category/{$topic['title']}/topic.json"), true);
                                            echo "<div id=\"{$topic['title']}-material\" class=\"topic-material\">".
                                                    "<center><h1>Material for $topic_title</h1></center>".
                                                         "<div class=\"row-fluid\">";
                                            $i = 0;
                                            foreach ($topic_info['material'] as $material) {
                                                $material_title = ucwords(str_replace("_", " ", $material['title']));
                                                if ($i != 0 && $i % 3 == 0) {
                                                    echo "</div>".
                                                         "<div class=\"row-fluid\">";
                                                }
                                                echo <<<HTML
                                                <div id="{$material['title']}" class="span4">
                                                    <h4>$material_title</h4>
                                                    <p>{$material['description']}</p>
                                                </div>
HTML;
                                                $i++;
                                            }
                                            if ($i % 3 == 0) {
                                                    echo "</div>".
                                                         "<div class=\"row-fluid\">";
                                            }
                                            echo "<div class=\"span4\">".
                                                "<h1 class=\"add-item\">+</h1>".
                                            "</div>".
                                            "</div>".
                                            "<hr />".
                                            "</div>";
                                        }
                                    }
                                ?>
                            </div>
                            <div id="material-editing-pane">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <h1 id="material-editing-heading"></h1>
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="New file">
                                                        <i class="fa fa-file-o"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Save">
                                                        <i class="fa fa-save"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Print">
                                                        <i class="fa fa-print"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Source code">
                                                        <i class="fa fa-code"></i> Source
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Bold">
                                                        <i class="fa fa-bold"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Italic">
                                                        <i class="fa fa-italic"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Underline">
                                                        <i class="fa fa-underline"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Strikethrough">
                                                        <i class="fa fa-strikethrough"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Cut">
                                                        <i class="fa fa-scissors"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Copy">
                                                        <i class="fa fa-copy"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Past">
                                                        <i class="fa fa-paste"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Undo">
                                                        <i class="fa fa-reply"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Redo">
                                                        <i class="fa fa-share"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Align left">
                                                        <i class="fa fa-align-left"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Align center">
                                                        <i class="fa fa-align-center"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Align right">
                                                        <i class="fa fa-align-right"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Align justify">
                                                        <i class="fa fa-align-justify"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Numbered list">
                                                        <i class="fa fa-list-ol"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Bulleted list">
                                                        <i class="fa fa-list-ul"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Link">
                                                        <i class="fa fa-link"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Unlink">
                                                        <i class="fa fa-unlink"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default"  data-toggle="tooltip" title="Picture">
                                                        <i class="fa fa-picture-o"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="HTML table">
                                                        <i class="fa fa-table"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-toggle="tooltip" title="Font">
                                                        <i class="fa fa-font"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="height:500px">
                                                <textarea class="form-control" rows="12" id="material-area"></textarea>
                                            </div>
                                            <div class="panel-footer">
                                                <button type="button" class="btn btn-success">
                                                    Publish</button>
                                                <button type="button" class="btn btn-primary">
                                                    Save Draft</button>
                                                <div class="pull-right">
                                                    <button type="button" class="btn btn-danger">
                                                        <i class="fa fa-trash-o"></i> Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

  var editor = CodeMirror.fromTextArea(document.getElementById("material-area"), {
    lineNumbers: true,
    matchBrackets: true,
    mode: "xml",
    htmlMode: true,
    viewportMargin: Infinity,
    theme: "monokai"
  });
  editor.setSize("100%", "100%");

  function setVimKeymap() {
    editor.setOption("keyMap", "vim");
  }

  function setEmacsKeymap() {
    editor.setOption("keyMap", "emacs");
  }
</script>
<?php
    echo html_end_setup();
?>
