<?php
require_once '../php/Require.php';
require_once '../php/ChallengeHandlers.func.php';

$return_data = array('registered' => false, 'error' => "");

$action = "";
if (isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    return_error("The action parameter was not set");
}

if ($action) {
    $db_conn = new DatabaseConn(true);
    $login_conn = new DatabaseConn(true, "secure_login");
    switch ($action) {
        case "access_settings":
            if (isset($_POST['registration'], $_POST['submission'], $_POST['accountChange'], $_POST['scoreboard'])) {
                $ALLOW_REGISTRATION = $_POST['registration'] == 'true' ? 1 : 0;
                $ALLOW_SUBMISSIONS = $_POST['submission'] == 'true' ? 1 : 0;
                $ALLOW_ACCOUNT_CHANGE = $_POST['accountChange'] == 'true' ? 1 : 0;
                $ENABLE_SCOREBOARD = $_POST['scoreboard'] == 'true' ? 1 : 0;
                $db_conn->query("UPDATE global_variables SET value=$ALLOW_REGISTRATION WHERE variable='allow_registration'");
                $db_conn->query("UPDATE global_variables SET value=$ALLOW_SUBMISSIONS WHERE variable='allow_submissions'");
                $db_conn->query("UPDATE global_variables SET value=$ALLOW_ACCOUNT_CHANGE WHERE variable='allow_account_change'");
                $db_conn->query("UPDATE global_variables SET value=$ENABLE_SCOREBOARD WHERE variable='enable_scoreboard'");
                return_success("Access settings successfuly saved.");
            } else {
                return_error("Unable to save website access settings.");
            }
        break;
        case "add_challenge":
        break;
        case "edit_challenge":
        break;
        case "remove_challenge":
        break;

        case "add_team":
        break;
        case "edit_team":
        break;
        case "remove_team":
        break;

        case "add_user":
        break;
        case "edit_user":
        break;
        case "remove_user":
        break;

        case "add_update":
            if (isset($_POST['title'], $_POST['body'], $_POST['picture'])) {
                $db_conn->set_table("challenge_updates");
                if ($db_conn->add_item(array($_POST['title'], $_POST['body'], time(), $_POST['picture']))) {
                    return_success("Added update to database.");
                } else {
                    return_error("Unable to add update to database.");
                }
            } else {
                return_error("Add update parameters not properly set");
            }
        break;
        case "edit_update":
            if (isset($_POST['id'], $_POST['title'], $_POST['body'], $_POST['picture'])) {
                $db_conn->set_table("challenge_updates");
                if ($db_conn->edit_row(array($_POST['title'], $_POST['body'], time(), $_POST['picture']), $_POST['id'])) {
                    return_success("Successfully edited update.");
                } else {
                    return_error("Update was unable to be changed.");
                }
            } else {
                return_error("Edit update parameters not properly set");
            }
        break;
        case "remove_update":
            if (isset($_POST['id'])) {
                $db_conn->set_table("challenge_updates");
                if ($db_conn->delete_item($_POST['id'], "id")) {
                    return_success("Successfully deleted update.");
                } else {
                    return_error("Unable to delete update.");
                }
            } else {
                return_error("Remove update parameters not properly set");
            }
        break;
    }
}

function return_error($error) {
    $return_data["error"] = $error; 
    echo json_encode($return_data);
    exit;
}

function return_success($msg) {
    $return_data["registered"] = true; 
    $return_data["successMessage"] = $msg;
    echo json_encode($return_data);
    exit;
}
?>
