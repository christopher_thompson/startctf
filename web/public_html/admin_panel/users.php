<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin_panel.css"></link>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <center>
               <div class="list-group" style="cursor:pointer">
                  <a href="/admin_panel/access" id="access" class="list-group-item">
                    <h4 class="list-group-item-heading">Access</h4>
                  </a>
                  <a href="/admin_panel/challenges" id="challenges" class="list-group-item">
                    <h4 class="list-group-item-heading">Challenges</h4>
                  </a>
                  <a href="/admin_panel/teams" id="teams" class="list-group-item">
                    <h4 class="list-group-item-heading">Teams</h4>
                  </a>
                  <a href="/admin_panel/users" id="users" class="list-group-item active">
                    <h4 class="list-group-item-heading">Users</h4>
                  </a>
                  <a href="/admin_panel/updates" id="updates" class="list-group-item">
                    <h4 class="list-group-item-heading">Updates</h4>
                  </a>
                  <a href="/admin_panel/learn" id="learn" class="list-group-item">
                    <h4 class="list-group-item-heading">Learn</h4>
                  </a>
                </div>
                </center>
            </div>
            <div class="span10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 id="usersHeader" class="panel-header">Users</h2>
                    </div>
                    <div class="admin-panel-body">
                        <div id="usersPanel">
                        <?php
                        $dbConn = new DatabaseConn(false, "secure_login");
                        $dbConn->set_table("members");
                        $users = $dbConn->get_all_items("id");
                        if ($users) {
                            $teamConn = new DatabaseConn(); 
                            $teamConn->set_table("teams");
                            foreach ($users as $user) {
                                $team = array();
                                if ($user['team_id'] != 0) {
                                    $team = $teamConn->get_item($user['team_id'], "id");
                                    $team = $team[0];
                                } else {
                                    $team['name'] = "Has not joined team";
                                }
                                echo <<<HTML
                                <div class="row-fluid admin-panel-row">
                                    <div class="span10">
                                        <strong>{$user['last_name']}, {$user['first_name']}</strong>
                                    </div>
                                    <div class="span2">
                                        <button class="btn btn-primary btn-xs admin-panel-dropdown" data-for="#user_{$user['id']}">Show</button>
                                    </div>
                                </div>
                                <div id="user_{$user['id']}" class="row-fluid admin-panel-infos" style="display:none">
                                    <div class="span10 offset1">
                                        <div class="admin-panel admin-panel-primary">
                                            <div class="admin-panel-heading">
                                                <h3 class="admin-panel-title">User Information</h3>
                                            </div>
                                            <div class="admin-panel-body">
                                                <div class="row-fluid">
                                                    <div class="row9">
                                                        <table class="table table-condensed table-responsive admin-panel-table-information">
                                                            <tbody>
                                                                <tr>
                                                                    <td>ID:</td>
                                                                    <td>{$user['id']}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Name:</td>
                                                                    <td>{$user['last_name']}, {$user['first_name']}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email:</td>
                                                                    <td>{$user['email']}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Team:</td>
                                                                    <td>{$team['name']}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="admin-panel-footer">
                                                <span class="pull-right">
                                                    <button style="display:none" class="btn btn-warning btn-xs" type="button" data-toggle="tooltip" data-original-title="Edit this user">Edit</button>
                                                    <button style="display:none" class="btn btn-danger btn-xs" type="button" data-toggle="tooltip" data-original-title="Delete this user">Delete</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
HTML;
                            }
                        } else {
                            echo html_error("There are no users");
                        }
                        echo <<<HTML
                        <div id="add_user" class="add_form">
                            <button style="display:none" class="btn btn-warning btn-sm add-button" type="button" data-toggle="tooltip" data-original-title="Add Users">Add User</button>
                            <div class="col-lg-6 add-form" style="display:none">
                                <form class="form-horizontal">
                                    <fieldset>
                                        <legend>Add a User</legend>
                                        <div class="form-group">
                                            <label for="inputUserFirstName" class="col-lg-2 control-label">First Name</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUserFirstName" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUserLastName" class="col-lg-2 control-label">Last Name</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUserLastName" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUserEmail" class="col-lg-2 control-label">Email</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUserEmail" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUserPassword" class="col-lg-2 control-label">Password</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUserPassword">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUserTeam" class="col-lg-2 control-label">Team</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUserTeam">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <button id="userFormAddButton" class="btn btn-default btn-xs add-submit-button" >Submit</button>
                                    <button id="userFormCancleButton" class="btn btn-default btn-xs add-cancel-button">Cancel</button>
                                </form>
                            </div>
                        </div>
HTML;
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
