<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="../js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="../css/admin_panel.css"></link>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> Quick Shortcuts</h3>
                </div>
                <div class="panel-body">
                    <div class="row" style="margin-left:-15px">
                        <div class="col-xs-6 col-md-6">
                          <a href="/admin_panel/access" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Access</a>
                          <a href="/admin_panel/challenges" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>Challenges</a>
                          <a href="/admin_panel/learn" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-signal"></span> <br/>Learn</a>
                          <a href="/admin_panel/teams" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>Teams</a>
                        </div>
                        <div class="col-xs-6 col-md-6">
                          <a href="/admin_panel/users" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Users</a>
                          <a href="/admin_panel/updates" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Updates</a>
                          <a href="/images" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-picture"></span> <br/>Photos</a>
                          <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-tag"></span> <br/>Shell</a>
                        </div>
                    </div>
                    <a href="/" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span> Website</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
    echo html_end_setup();
?>
