<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin_panel.css"></link>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <center>
               <div class="list-group" style="cursor:pointer">
                  <a href="/admin_panel/access" id="access" class="list-group-item">
                    <h4 class="list-group-item-heading">Access</h4>
                  </a>
                  <a href="/admin_panel/challenges" id="challenges" class="list-group-item">
                    <h4 class="list-group-item-heading">Challenges</h4>
                  </a>
                  <a href="/admin_panel/teams" id="teams" class="list-group-item active">
                    <h4 class="list-group-item-heading">Teams</h4>
                  </a>
                  <a href="/admin_panel/users" id="users" class="list-group-item">
                    <h4 class="list-group-item-heading">Users</h4>
                  </a>
                  <a href="/admin_panel/updates" id="updates" class="list-group-item">
                    <h4 class="list-group-item-heading">Updates</h4>
                  </a>
                  <a href="/admin_panel/learn" id="learn" class="list-group-item">
                    <h4 class="list-group-item-heading">Learn</h4>
                  </a>
                </div>
                </center>
            </div>
            <div class="span10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 id="teamsHeader" class="panel-header">Teams</h2>
                    </div>
                    <div class="admin-panel-body">
                        <div id="teamsPanel">
                        <?php
                        $dbConn = new DatabaseConn(false);
                        $dbConn->set_table("teams");
                        $loginConn = new DatabaseConn(false, "secure_login");
                        $loginConn->set_table("members");
                        $teams = $dbConn->get_all_items("score");
                        if ($teams) {
                            foreach ($teams as $team) {
                                $member_1 = "There is no team admin";
                                $member_2 = "There is no second member";
                                $member_3 = "There is no third memeber";
                                if ($team["member_1"] != 0) {
                                    $member = $loginConn->get_item($team["member_1"], "id");
                                    $member_1 = $member[0]["last_name"].", ".$member[0]["first_name"];
                                }

                                if ($team["member_2"] != 0) {
                                    $member = $loginConn->get_item($team["member_2"], "id");
                                    $member_2 = $member[0]["last_name"].", ".$member[0]["first_name"];
                                }

                                if ($team["member_3"] != 0) {
                                    $member = $loginConn->get_item($team["member_3"], "id");
                                    $member_3 = $member[0]["last_name"].", ".$member[0]["first_name"];
                                }
                                $dbConn->set_table("shell_credentials");
                                $shell_creds = $dbConn->get_item($team["id"], "team_id");
                                $shell_username = $shell_creds[0]["shell_user"];
                                $shell_password = $shell_creds[0]["shell_pass"];

                                echo <<<HTML
                                <div class="row-fluid admin-panel-row">
                                    <div class="span10">
                                        <strong>{$team['name']}</strong>
                                    </div>
                                    <div class="span2">
                                        <button class="btn btn-primary btn-xs admin-panel-dropdown" data-for="#team_{$team['id']}">Show</button>
                                    </div>
                                </div>
                                <div id="team_{$team['id']}" class="row-fluid admin-panel-infos" style="display:none">
                                    <div class="span10 offset1">
                                        <div class="admin-panel admin-panel-primary">
                                            <div class="admin-panel-heading">
                                                <h3 class="admin-panel-title">Team Information</h3>
                                            </div>
                                            <div class="admin-panel-body">
                                                <div class="row-fluid">
                                                    <div class="row9">
                                                        <table class="table table-condensed table-responsive admin-panel-table-information">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Name:</td>
                                                                    <td>{$team['name']}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Join Code:</td>
                                                                    <td>{$team['join_code']}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Admin:</td>
                                                                    <td>$member_1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Member 2:</td>
                                                                    <td>$member_2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Member 3:</td>
                                                                    <td>$member_3</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Shell Username:</td>
                                                                    <td>$shell_username</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Shell Password:</td>
                                                                    <td>$shell_password</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Score:</td>
                                                                    <td>{$team['score']}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="admin-panel-footer">
                                                <span class="pull-right">
                                                    <button style="display:none" class="btn btn-warning btn-xs" type="button" data-toggle="tooltip" data-original-title="Edit this team">Edit</button>
                                                    <button style="display:none" class="btn btn-danger btn-xs" type="button" data-toggle="tooltip" data-original-title="Delete this team">Delete</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
HTML;
                            }
                        } else {
                            echo html_error("There are no teams");
                        }
                        echo <<<HTML
                        <div id="add_team" class="add_form">
                            <button style="display:none" class="btn btn-warning btn-sm add-button" type="button" data-toggle="tooltip" data-original-title="Add Teams">Add Team</button>
                            <div class="col-lg-6 add-form" style="display:none">
                                <form class="form-horizontal">
                                    <fieldset>
                                        <legend>Add a Team</legend>
                                        <div class="form-group">
                                            <label for="inputTeamName" class="col-lg-2 control-label">Name</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputTeamName" placeholder="Team Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputTeamMember1" class="col-lg-2 control-label">User 1</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputTeamMember1" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputTeamMember2" class="col-lg-2 control-label">User 2</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputTeamMember2" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputTeamMember3" class="col-lg-2 control-label">User 3</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputTeamMember3" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputTeamScore" class="col-lg-2 control-label">Score</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputTeamScore" value="0">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <button id="teamFormAddButton" class="btn btn-default btn-xs add-submit-button" >Submit</button>
                                    <button id="teamFormCancleButton" class="btn btn-default btn-xs add-cancel-button">Cancel</button>
                                </form>
                            </div>
                        </div>
HTML;
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
