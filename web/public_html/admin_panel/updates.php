<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin_panel.css"></link>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <center>
               <div class="list-group" style="cursor:pointer">
                  <a href="/admin_panel/access" id="access" class="list-group-item">
                    <h4 class="list-group-item-heading">Access</h4>
                  </a>
                  <a href="/admin_panel/challenges" id="challenges" class="list-group-item">
                    <h4 class="list-group-item-heading">Challenges</h4>
                  </a>
                  <a href="/admin_panel/teams" id="teams" class="list-group-item">
                    <h4 class="list-group-item-heading">Teams</h4>
                  </a>
                  <a href="/admin_panel/users" id="users" class="list-group-item">
                    <h4 class="list-group-item-heading">Users</h4>
                  </a>
                  <a href="/admin_panel/updates" id="updates" class="list-group-item active">
                    <h4 class="list-group-item-heading">Updates</h4>
                  </a>
                  <a href="/admin_panel/learn" id="learn" class="list-group-item">
                    <h4 class="list-group-item-heading">Learn</h4>
                  </a>
                </div>
                </center>
            </div>
            <div class="span10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 id="updatesHeader" class="panel-header">Updates</h2>
                    </div>
                    <div class="admin-panel-body">
                        <div id="updatesPanel">
                        <?php
                            $dbConn = new DatabaseConn();
                            $dbConn->set_table("challenge_updates");
                            $updates = $dbConn->get_all_items("id");
                            if ($updates) {
                                foreach ($updates as $update) {
                                    $id = $update['id'];
                                    $date = date('F j<\s\up>S</\s\up>, Y', $update['date']);
                                    $body = nl2br(stripslashes(htmlspecialchars_decode($update['body'])));
                                    $picture = $update['picture'];
                                    if (strcmp($picture, "") == 0) {
                                        $picture = "default.png";
                                    }
                                    if (strpos($picture, "http") === false) {
                                        $picture = "../images/".$picture;
                                    }
                                    echo <<<HTML
                                <div class="row-fluid admin-panel-row">
                                    <div class="span10">
                                        <strong>{$update['title']}</strong>
                                    </div>
                                    <div class="span2">
                                        <button class="btn btn-primary btn-xs admin-panel-dropdown" data-for="#update_{$update['id']}">Show</button>
                                    </div>
                                </div>
                                <div id="update_$id" class="row-fluid admin-panel-infos" style="display:none">
                                    <div class="span10 offset1">
                                        <div class="admin-panel admin-panel-primary">
                                            <div class="admin-panel-heading">
                                                <h3 class="admin-panel-title">Update Information</h3>
                                            </div>
                                            <div class="admin-panel-body">
                                                <div class="row-fluid">
                                                    <div class="span3">
                                                        <img style="max-width:100%" src="$picture">
                                                    </div>
                                                    <div class="span9">
                                                        <table class="table table-condensed table-responsive admin-panel-table-information">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Title:</td>
                                                                    <td>{$update['title']}</td>
                                                                    <td id="editUpdateTitle$id"><input style="display:none" class="form-control" type="text" value="{$update['title']}"></input></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Body:</td>
                                                                    <td><p style="word-wrap: break-word;">$body</p></td>
                                                                    <td id="editUpdateBody$id"><textarea class="form-control" rows="10" style="display:none">$body</textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pictures:</td>
                                                                    <td><p style="word-wrap: break-word;">{$update['picture']}</p></td>
                                                                    <td id="editUpdatePicture$id"><input style="display:none" class="form-control" type="text" value="{$update['picture']}"></input></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Date:</td>
                                                                    <td>$date</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><button style="display:none;" class="edit-btn btn btn-warning" type="button" data-original-title="Edit this update">Save Changes</button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="admin-panel-footer">
                                                <span class="pull-right">
                                                    <button class="show-edit-form-btn btn btn-warning btn-xs" type="button" data-toggle="tooltip" data-original-title="Edit this update">Edit</button>
                                                    <button class="delete-btn btn btn-danger btn-xs" type="button" data-toggle="tooltip" data-original-title="Delete this update">Delete</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
HTML;
                        }
                        } else {
                            echo html_error("There are no updates");
                        }
                        echo <<<HTML
                        <div id="add_update" class="add_form">
                            <button class="btn btn-warning btn-sm add-button" type="button" data-toggle="tooltip" data-original-title="Add Updates">Add Update</button>
                            <div class="col-lg-6 add-form" style="display:none">
                                <form class="form-horizontal">
                                    <fieldset>
                                        <legend>Add an Update</legend>
                                        <div class="form-group">
                                            <label for="inputUpdateTitle" class="col-lg-2 control-label">Title</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUpdateTitle" placeholder="Title">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUpdateBody" class="col-lg-2 control-label">Body</label>
                                            <div class="inputField col-lg-10">
                                                <textarea class="form-control" id="inputUpdateBody" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputUpdatePicture" class="col-lg-2 control-label">Picture</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputUpdatePicture" placeholder="Picture">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <button id="updateFormAddButton" class="btn btn-default btn-xs add-submit-button" >Submit</button>
                                    <button id="updateFormCancleButton" class="btn btn-default btn-xs add-cancel-button">Cancel</button>
                                </form>
                            </div>
                        </div>
HTML;
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
