<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin_panel.css"></link>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <center>
               <div class="list-group" style="cursor:pointer">
                  <a href="/admin_panel/access" id="access" class="list-group-item">
                    <h4 class="list-group-item-heading">Access</h4>
                  </a>
                  <a href="/admin_panel/challenges" id="challenges" class="list-group-item active">
                    <h4 class="list-group-item-heading">Challenges</h4>
                  </a>
                  <a href="/admin_panel/teams" id="teams" class="list-group-item">
                    <h4 class="list-group-item-heading">Teams</h4>
                  </a>
                  <a href="/admin_panel/users" id="users" class="list-group-item">
                    <h4 class="list-group-item-heading">Users</h4>
                  </a>
                  <a href="/admin_panel/updates" id="updates" class="list-group-item">
                    <h4 class="list-group-item-heading">Updates</h4>
                  </a>
                  <a href="/admin_panel/learn" id="learn" class="list-group-item">
                    <h4 class="list-group-item-heading">Learn</h4>
                  </a>
                </div>
                </center>
            </div>
            <div class="span10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 id="challengesHeader" class="panel-header">Challenges</h2>
                    </div>
                    <div class="admin-panel-body">
                        <div id="challengesPanel">
                        <?php
                        $dbConn = new DatabaseConn();
                        $dbConn->set_table("challenge_list");
                        $challenges = $dbConn->get_all_items("prog_id");
                        if ($challenges) {
                            foreach ($challenges as $challenge) {
                                echo <<<HTML
                            <div class="row-fluid admin-panel-row">
                                <div class="span10">
                                    <strong>{$challenge['title']}</strong>
                                    <br />
                                    <span class="text-muted">Solved: Implement way to count number solved</span>
                                </div>
                                <div class="span1">
                                    <button class="btn btn-primary btn-xs admin-panel-dropdown" data-for="#challenge_{$challenge['prog_id']}">Show</button>
                                </div>
                            </div>
                            <div id="challenge_{$challenge['prog_id']}" class="row-fluid admin-panel-infos" style="display:none">
                                <div class="span10 offset1">
                                    <div class="admin-panel admin-panel-primary">
                                        <div class="admin-panel-heading">
                                            <h3 class="admin-panel-title">Challenge Information</h3>
                                        </div>
                                        <div class="admin-panel-body">
                                            <div class="row-fluid">
                                                <div class="row9">
                                                    <table class="table table-condensed table-responsive admin-panel-table-information">
                                                        <tbody>
                                                            <tr>
                                                                <td>Title:</td>
                                                                <td class="content-view">{$challenge['title']}</td>
                                                                <td style="display:none" class="edit-input"><input type="text" value="{$challenge['title']}"></input></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Description:</td>
                                                                <td>{$challenge['description']}</td>
                                                                <td style="display:none" class="edit-input"><input type="text" value="{$challenge['description']}"></input></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="admin-panel-footer">
                                            <span class="pull-right">
                                                <button style="display:none" class="btn btn-warning btn-xs" type="button" data-toggle="tooltip" data-original-title="Edit this challenge">Edit</button>
                                                <button style="display:none" class="btn btn-danger btn-xs" type="button" data-toggle="tooltip" data-original-title="Delete this challenge">Delete</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
HTML;
                            }
                        } else {
                            echo html_error("There are no challenges");
                        }
                        echo <<<HTML
                        <div id="add_challenge" class="add_form">
                            <button style="display:none" class="btn btn-warning btn-sm add-button" type="button" data-toggle="tooltip" data-original-title="Add Challenges">Add Challenge</button>
                            <div class="col-lg-6 add-form" style="display:none">
                                <form class="form-horizontal">
                                    <fieldset>
                                        <legend>Add a Challenge</legend>
                                        <div class="form-group">
                                            <label for="inputChallengeTitle" class="col-lg-2 control-label">Title</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputChallengeTitle" placeholder="Challenge Title">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputDescription" class="col-lg-2 control-label">Description</label>
                                            <div class="inputField col-lg-10">
                                                <input type="text" onfocus="" class="form-control" id="inputChallengeDescription" placeholder="Challenge Description">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <button id="challengeFormAddButton" class="btn btn-default btn-xs add-submit-button" >Submit</button>
                                    <button id="challengeFormCancleButton" class="btn btn-default btn-xs add-cancel-button">Cancel</button>
                                </form>
                            </div>
                        </div>
HTML;
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
