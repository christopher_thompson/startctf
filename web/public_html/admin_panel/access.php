<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/admin_panel.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin_panel.css"></link>
<div class="container well">
    <div class="jumbotron page-title">
        <h1>Admin Panel</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <center>
               <div class="list-group" style="cursor:pointer">
                  <a href="/admin_panel/access" id="access" class="list-group-item active">
                    <h4 class="list-group-item-heading">Access</h4>
                  </a>
                  <a href="/admin_panel/challenges" id="challenges" class="list-group-item">
                    <h4 class="list-group-item-heading">Challenges</h4>
                  </a>
                  <a href="/admin_panel/teams" id="teams" class="list-group-item">
                    <h4 class="list-group-item-heading">Teams</h4>
                  </a>
                  <a href="/admin_panel/users" id="users" class="list-group-item">
                    <h4 class="list-group-item-heading">Users</h4>
                  </a>
                  <a href="/admin_panel/updates" id="updates" class="list-group-item">
                    <h4 class="list-group-item-heading">Updates</h4>
                  </a>
                  <a href="/admin_panel/learn" id="learn" class="list-group-item">
                    <h4 class="list-group-item-heading">Learn</h4>
                  </a>
                </div>
                </center>
            </div>
            <div class="span10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 id="accessHeader" class="panel-header">Access</h2>
                    </div>
                    <div class="admin-panel-body">
                        <div id="accessPanel">
                            <?php
                                $dbConn = new DatabaseConn();
                                $dbConn->set_table('global_variables');
                                $results = $dbConn->get_all_items('variable');
                                $ALLOW_REGISTRATION = false;
                                $ALLOW_SUBMISSIONS = false;
                                $ALLOW_ACCOUNT_CHANGE = false;
                                $ENABLE_SCOREBOARD = false;
                                foreach ($results as $result) {
                                    switch ($result['variable']) {
                                        case 'allow_registration':
                                            $ALLOW_REGISTRATION = intval($result['value']);
                                            break;
                                        case 'allow_submissions':
                                            $ALLOW_SUBMISSIONS = intval($result['value']);
                                            break;
                                        case 'allow_account_change':
                                            $ALLOW_ACCOUNT_CHANGE = intval($result['value']);
                                            break;
                                        case 'enable_scoreboard':
                                            $ENABLE_SCOREBOARD = intval($result['value']);
                                            break;
                                    }
                                }
                                $reg_en = "none"; $reg_dis = "block"; $reg_c = "btn-danger";
                                if ($ALLOW_REGISTRATION) {
                                    $reg_en = "block"; $reg_dis = "none"; $reg_c = "btn-info";
                                }
                                $sub_en = "none"; $sub_dis = "block"; $sub_c = "btn-danger";
                                if ($ALLOW_SUBMISSIONS) {
                                    $sub_en = "block"; $sub_dis = "none"; $sub_c = "btn-info";
                                }
                                $acct_en = "none"; $acct_dis = "block"; $acct_c = "btn-danger";
                                if ($ALLOW_ACCOUNT_CHANGE) {
                                    $acct_en = "block"; $acct_dis = "none"; $acct_c = "btn-info";
                                }
                                $scr_en = "none"; $scr_dis = "block"; $scr_c = "btn-danger";
                                if ($ENABLE_SCOREBOARD) {
                                    $scr_en = "block"; $scr_dis = "none"; $scr_c = "btn-info";
                                }
                            ?>
                            <div class="row-fluid">
                                <form class="form-horizontal">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="allowRegistration" class="col-lg-4 control-label"><div style="display:<?php echo $reg_en; ?>" class="access-allowed">Registration is allowed</div><div style="display:<?php echo $reg_dis; ?>" class="access-not-allowed">Registration is not allowed</div></label>
                                            <div class="inputField col-lg-8">
                                                <button id="allowRegistration" class="btn <?php echo $reg_c ?> span6 accessButton" type="button">Allow Registration</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="allowSubmission" class="col-lg-4 control-label"><div style="display:<?php echo $sub_en; ?>" class="access-allowed">Submissions are allowed</div><div style="display:<?php echo $sub_dis; ?>" class="access-not-allowed">Submissions are not allowed</div></label>
                                            <div class="inputField col-lg-8">
                                                <button id="allowSubmission" class="btn <?php echo $sub_c ?> span6 accessButton" type="button">Allow Challenge Submission</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="allowAccountChange" class="col-lg-4 control-label"><div style="display:<?php echo $acct_en; ?>" class="access-allowed">Accounts are able to be changed</div><div style="display:<?php echo $acct_dis; ?>" class="access-not-allowed">Accounts are not allowed to be changed</div></label>
                                            <div class="inputField col-lg-8">
                                                <button id="allowAccountChange" class="btn <?php echo $acct_c ?> span6 accessButton" type="button">Allow Account Changes</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="enableScoreboard" class="col-lg-4 control-label"><div style="display:<?php echo $scr_en; ?>" class="access-allowed">The Scoreboard is enabled</div><div style="display:<?php echo $scr_dis; ?>" class="access-not-allowed">The Scoreboard is not enabled</div></label>
                                            <div class="inputField col-lg-8">
                                                <button id="enableScoreboard" class="btn <?php echo $scr_c ?> span6 accessButton" type="button">Enable Scoreboard</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <hr />
                            <div class="row-fluid">
                                <button style="position:relative;left:5px;" id="saveAccessSettings" class="btn btn-info btn-warning span4 offset4" type="button">Save Settings</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
