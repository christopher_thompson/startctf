<?php
require_once '../php/Require.php';

$return_data = array('updated' => false, 'error' => "");

$login_conn = new DatabaseConn(false, 'secure_login');

if (login_check()) {
    returnError("Already logged in, if you need to change your password, just go to the account page");
}

if (isset($_POST['email']) && $_POST['email']) {
    $email_regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
    $email = $_POST['email'];
    if (preg_match($email_regex, $email)) {
        $login_conn->set_table("members");
        $result = $login_conn->get_item($email, 'email');
        if (count($result) > 1)
            returnError("Uh oh. There appears to be two users in our database with the same email. Please notify us about this. Thanks ;D");

        if (count($result) == 1 && $user = $result[0]) {
            $newPassword = hash("sha512", substr(hash("sha512", time().$user['id']), 0, 8));
            $login_conn->edit_item($newPassword, "password", $user['id']);
            $message = "The temporary password for your account is: $newPassword. Make sure that you change this password once you log back into your account";
            $message = wordwrap($message, 70, "\r\n");
            mail($email, "StartCTF Temporary Password", $message);
        } else {
            returnError("Email given does not match any accounts that we have. Did you enter in the right email address?");
        }
    } else {
        returnError("Email given is not an email.");
    }
} else {
    returnError("No email was given.");
}

returnSuccess("You have been emailed your temporary password. Make sure you go into your account settings page and change your password to something you will remember."); 

function returnError($error) {
    $return_data["error"] = $error; 
    echo json_encode($return_data);
    exit;
}

function returnSuccess() {
    $return_data["updated"] = true; 
    echo json_encode($return_data);
    exit;
}
?>
