<?php
require_once '../php/Require.php';

echo html_begin_setup(false);

if (login_check() == true) {
    $_SESSION = array();

    $params = session_get_cookie_params();

    setcookie(session_name(), '', time() - 42000, $params["path"], 
        $params["domain"], $params["secure"], $params["httponly"]);

    session_destroy();
    if (verify_cookies()) {
        $db_conn = new DatabaseConn(false, 'secure_login');
        $cookie_id = $db_conn->sanitize_variable($_COOKIE['cookie_id']);
        $db_conn->set_table("cookies");
        $db_conn->delete_item($cookie_id, "id");
        setcookie("cookie_id", "", time() - (60 * 60 * 24 * 14), "/");
        setcookie("cookie_ver", "", time() - (60 * 60 * 24 * 14), "/");
    }
    header('Location: ' . $_SERVER['HTTP_REFERER']);
} else {
    echo html_error("You are not already logged in, so you might have a hard time logging out :D");
    echo html_info('<a href="./login.php">Login?</a>');
}
?>
</div>
</body>
<?php
echo html_end_setup();
?>
