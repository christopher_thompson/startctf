<?php
require_once '../php/Require.php';

$loggedIn = array('loggedIn' => false);
if (login_check() != true) {
    if (isset($_POST['email'], $_POST['password'], $_POST['rememberMe'])) {
        $email = $_POST['email'];
        if ($email == "carolinamz1@gmail.com") {
            $loggedIn['carolina'] = true;
            echo json_encode($loggedIn);
            exit;
        }
        $password = $_POST['password'];
        $remember_me = $_POST['rememberMe'];
        if (login($email, $password, $remember_me) == true) {
            $loggedIn['loggedIn'] = true;
            echo json_encode($loggedIn);
        } else {
            echo json_encode($loggedIn);
        }
    } else {
        echo json_encode($loggedIn);
    }
} else {
    echo json_encode($loggedIn);
}
?>
