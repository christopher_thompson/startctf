<?php
require_once '../php/Require.php';

$loggedIn = login_check();

echo html_begin_setup($loggedIn);

if ($loggedIn != true) {
    if (isset($_GET['error'])) {
        echo html_error('Sorry, you could not be logged in :C');
    }

    echo <<<HTML
<script type="text/javascript" src="../js/sha512.js"></script>
<script type="text/javascript" src="../js/login.js"></script>
<style>
body {
  background: #ecf0f1;
}
</style>

<div class="container" id="successfulLoginPane">
    <div id="successTitle" style="display:none">
        <center>
            <div class="jumbotron page-title">
                <h1>Way to go! You logged in!</h1>
            </div>
        </center>
    </div>
    <center>
        <h1 style="display:none">Start solving <a href="../challenges">challenges</a>,</h1>
        <h1 style="display:none">Visit your team's <a href="../shell">shell</a>,</h1>
        <h1 style="display:none">Or manage your <a href="../account">account</a></h1>
    </center>
</div>
<div class="container" id="loginPane">
    <center>
        <div class="jumbotron page-title">
            <h1>Sign In</h1>
        </div>
    </center>
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form name="login_form" role="form">
                <fieldset>
                    <hr>
                    <div class="form-group">
                                 <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address">
                    </div>
                    <div class="form-group">
                                  <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password">
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <span class="button-checkbox">
                                <button id="rememberMeButton" type="button" class="btn" data-color="info">Remember Me</button>
                                <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                            </span>
                        </div>
                        <div class="span6">
                            <a href="forgot_password.php" class="btn btn-link pull-right">Forgot Password?</a>
                        </div>
                    </div>
                    <hr />
                    <div class="row-fluid">
                        <div class="span6">
                            <input style="margin-bottom:15px;" type="button" id="loginButton" class="btn btn-lg btn-success btn-block" onClick="processForm()" value="Sign In">
                        </div>
                        <div class="span6">
                            <a style="margin-bottom:15px;" href="../register/" class="btn btn-lg btn-primary btn-block">Register</a>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

HTML;
} else {
    echo html_error("You are already logged in, you have to log out first if you want to login again :P");
    echo html_info('<a href="./logout.php">Log out?</a>');
}

echo html_end_setup();
?>
