<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>
<script type="text/javascript" src="/js/forgot_password.js"></script>
<div class="well">
    <div class="jumbotron page-title">
        <h1>Forgot Password</h1>
    </div>
    <div class="">
        <div class="row-fluid">
            <div class="span12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>If you have forgotten your password to your account, please enter in the email address that you used to register your account in the email box. Once you click reset, an email will be sent to your account notifying you that a temporary password has been set for your account. Once you receive this email, login to your account and change the password through the <a href="/account" target="_blank">account</a> to something that you will remember. (Please try to avoid writing down your passwords on paper, that is like the #1 thing not to do with passwords, thanks :D)</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="offset3 span6">
               <form class="form-horizontal">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="form-group">
                                <label for="inputEmail" class="span1 control-label">Email</label>
                                <div class="span10">
                                  <input type="text" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="form-group">
                                <div class="offset3 span4">
                                    <button id="resetButton" class="offset2 btn btn-primary btn-lg" onclick="return false;" disabled>Reset Password</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
               </form>
            </div>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
