<?php
require_once '../php/Require.php';

echo html_begin_setup(login_check());
?>

<link type="text/css" rel="stylesheet" href="../css/learn.css?v=2">

<div class="container well">
    <div class="jumbotron page-title">
        <h1>About this Competition</h1>
    </div>
    <hr />
    <p>The inception of this project took place during the 2013 CSAW Finals trip just after we had received news of the winners of the competition. Being in New York City competing alongside the best high school students in the United States is a prize unto itself, and actually winning the competition seemed almost to be an afterthought to us. To us (and I believe I am able to speak for every other finalist) and every other finalist in New York, this competition, and the challenges placed before us are addicting. Now I do not mean just defeating the challenge by solving it, but the failure that one experiences before success. For us, failure is what makes CSAW, CSAW. There were times when weeks would go by and no progress would be made in finding additional evidence. Our passion for solving the challenge was fueled by the single moment of triumph when you realized that you found the next clue to the puzzle. There is no greater feeling than surpassing the mental block which holds you down for weeks on end. </p>

    <p>After the end of this competition, we began to brainstorm ways for us to provide others with the same experience of triumph that we felt when competing. Wanting to appeal to a wide variety of students, we had to greatly consider the learning side to this competition. We wanted to allow those who have a background in cyber security to continue to excel and learn new topics, while at the same time those who have never seen a MD5 hash would also be able to compete and develop both their interest and knowledge.</p>

    <p>Most importantly, we aim to get rid of the "hacker sterotype" which Hollywood has most graciously given to those who are knowledgable about computers. This sterotype is epitomised by the introverted male furiously typing away at a screen which contains green text cascading down so fast that any "normal person" would not be able to decypher it (all of this is taking place in a pitch black room for a dramatic effect). Well, in reality "hackers" are just people who have fun solving puzzles and finding flaws in other people's programs. In addition, contrast to popular belief, not everything can be "hacked" with a single press of a big red button. One of the most famous (or infamous depending on your ideology) viruses ever made was the United States' very own <a target="_blank" href="http://vimeo.com/25118844">Stuxnet virus</a>. The preperation for deploying this piece of malware took years of planning, spying and programming to ensure that its intended purpose (to destroy Iran's centrifuges) was carried out. We hope that by participating in StartCTF, you will not just be able to understand what it really means to be a hacker but also to comprehend the importance of cyber warfare and how we are going to deal with the ever growing threat of security</p>
</div>
<!--Biographies still need to be put here-->
<?php
    echo html_end_setup();
?>
