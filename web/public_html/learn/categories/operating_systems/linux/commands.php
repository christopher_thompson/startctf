<div class="row-fluid">
     <div class="span12">
     	  <h1 class="page-header">Overview</h1>
	  <hr />
	  <br />
     </div>
</div>
<div class="row-fluid">
     <div class="span12">
     	  <h1 class="page-header">Description</h1>
	  <hr />
      <br />
     </div>
</div>
<div class="row-fluid">
     <h1 class="page-header">Commands</h1>
     <hr />
     <br />

     <h3>pwd</h3>
     <font color="#4C4C4C"><i>Displays location in filesystem.</i></font>
     <p style="margin-left:20px">The pwd Linux command allows the user to see where they current are in the filesystem. It can be useful to decide where one needs to go access a certain file.</p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">If our username is "user", running pwd in our home folder would display the path /home/user</p>
     <pre class="brush: linux" style="margin-left:20px">
$ pwd
/home/user
     </pre>
     <br />

     <h3>ls</h3>
     <font color="#4C4C4C"><i>Lists the name of files.</i></font>
     <p style="margin-left:20px">The ls command allows the user to be able to see what files are in the current directory, which can be useful when working with or manipulating files. </p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">In the home folder (/home/user), the user can see that they have several folders; Desktop, Downloads, Documents, Pictures, etc.</p>
     <pre class="brush: linux" style="margin-left:20px">
$ ls
Desktop Downloads Music Public Templates Documents
Pictures Videos
     </pre>
     <br />

     <h3>cd</h3>
     <font color="#4C4C4C"><i>Moves the user to the indicated directory/path.</i></font>
     <p style="margin-left:20px">It is oftentimes easier to work with certain files or scripts in their own directory. This requires moving around in the filesystem, which the cd command simplifies.</p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">If the user wanted to easily access a file in their documents folder, they can move from the home directory (/home/user) to the their Documents folder(/home/user/Documents).
     <pre class="brush:linux" style="margin-left:20px">
$ cd Documents
     </pre>
     <p style="margin-left:20px">Moving into another directory changes the file path.</p>
     <pre class="brush:linux" style="margin-left:20px">
$ pwd
/home/user
$ cd Documents
$ pwd
/home/user/Documents
     </pre>
     <p style="margin-left:20px">To move back into the folder we were just in, we would use ../</p>
     <pre class="brush:linux" style="margin-left:20px">
$ pwd
/home/user/Documents
$ cd ../
$ pwd
/home/user
     </pre>
     <br />

     <h3>echo</h3>
     <font color="#4C4C4C"><i>Echo allows text to be displayed on-screen. When working with scripts and programs, it may be useful to display the results on-screen.</i></font>
     <p style="margin-left:20px">Echo is useful in displaying information on-screen.</p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">Echoing "hi" simply displays "hi" on-screen. This can also be used to display the results of programs or the values of variables.</p>
     <pre class="brush: linux" style="margin-left:20px">
$ echo hi
hi
     </pre>
     <br />

     <h3>cat</h3>
     <font color="#4C4C4C"><i>Displays a text file.</i></font>
     <p style="margin-left:20px">It is useful to be able to display the contents of a file in the terminal, oftentimes to determine whether the file is the one we are interested in opening up or quickly read information in ther terminal. The cat command is able to display a file for the user.</p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">Assume we have a file called file.txt that contains the names of several colors in it. Running cat on file.txt allows us to see the words that were written in the file.</p>
     <pre class="brush: linux" style="margin-left:20px">
$ cat file.txt
red orange yellow green blue purple
     </pre>
     <p style="margin-left:20px">We could also look in the current directory to see if there are any files we would like to view.</p>
     <pre class="brush: linux" style="margin-left:20px">
$ pwd
/home/user/Documents
$ ls
dog.txt cat.txt 
$ cat cat.txt
meow meow meow meow
     </pre>
     <br />

     <h3>cp</h3>
     <font color="#4C4C4C"><i>Copies a file.</i></font>
     <p style="margin-left:20px">Cp allows for terminal users to have a way of copying (and easily naming) files, whether in the current directory or to another directory.</p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">Let's say we have a file called file1.txt that we would like to make a copy of. We can copy file1.txt as file2.txt in the same directory using the cp command.</p>
     <pre class="brush: linux" style="margin-left:20px">
$ cp file1.txt file2.txt
     </pre>
     <p style="margin-left:20px">We can employ the cat command to first see what is in each file and then copy the file we want. In this case, we see file2.txt contains the text "i like dogs", which we would like to make a copy of.</p>
     <pre class="brush: linux" style="margin-left:20px">
$ cat file1.txt
i like cats
$ cat file2.txt
i like dogs
$ cp file2.txt me.txt
     </pre>
     <br />
     
     <h3>rm</h3>
     <font color="#4C4C4C"><i>Deletes a file.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <h5><i>EXAMPLES</i></h5>
     <p style="margin-left:20px">Hullo</p>
     <pre class="brush: linux" style="margin-left:20px">
$ cp file1.txt file2.txt
     </pre>
     <p style="margin-left:20px">Example summary.</p>
     <br />

     <h3>mv</h3>
     <font color="#4C4C4C"><i>Changes the file name or moves the file.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>hostname</h3>
     <font color="#4C4C4C"><i>Displays the system name.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />
     
     <h3>grep</h3>
     <font color="#4C4C4C"><i>Searches for a string.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>head</h3>
     <font color="#4C4C4C"><i>Displays beginning of a file.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>tail</h3>
     <font color="#4C4C4C"><i>Displays the end of the file.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>sort</h3>
     <font color="#4C4C4C"><i>Displays a file in order.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>uniq</h3>
     <font color="#4C4C4C"><i>Removes duplicate lines from a file.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>diff</h3>
     <font color="#4C4C4C"><i>Compares two files.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />

     <h3>date</h3>
     <font color="#4C4C4C"><i>Displays the current time.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />
     
     <h3>script</h3>
     <font color="#4C4C4C"><i>Records commands.</i></font>
     <p style="margin-left:20px">Summary.</p>
     <br />
</div>
<script type="text/javascript" src="/js/syntaxHighlighter_scripts/shCore.js"></script>
<script type="text/javascript" src="/js/syntaxHighlighter_scripts/shBrushLinux.js"></script>
<link type="text/css" rel="stylesheet" href="/css/syntaxHighlighter_styles/shCoreMidnight.css">
<link type="text/css" rel="stylesheet" href="/css/syntaxHighlighter_styles/shThemeMidnight.css">
<script type="text/javascript">SyntaxHighlighter.all();</script>
