<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);

$page_showing = 0;
$page_info = array(
    "category" => "",
    "topic" => "",
    "material" => "",
    "info" => array()
);

if (isset($_GET['category']) && !empty($_GET['category']) && preg_match('/^[a-zA-Z0-9_]*$/', $_GET['category'])) {
    $page_info['category'] = $_GET['category'];
}

if (isset($_GET['topic']) && !empty($_GET['topic']) && preg_match('/^[a-zA-Z0-9_]*$/', $_GET['topic'])) {
    $page_info['topic'] = $_GET['topic'];
}

if (isset($_GET['material']) && !empty($_GET['material']) && preg_match('/^[a-zA-Z0-9_]*$/', $_GET['material'])) {
    $page_info['material'] = $_GET['material'];
}

$title = "Learn";
$page_root = "categories/".$page_info['category'];
$material_file = "";

if (!empty($_GET['category']) && file_exists($page_root)) {
    if (!empty($_GET['topic']) && file_exists($page_root."/".$page_info['topic'])) {
        $page_root = $page_root."/".$page_info['topic'];
        if (!empty($_GET['material']) && file_exists($page_root."/".$page_info['material'].".php")) {
            $material_file = $page_root."/".$page_info['material'].".php";
            $page_showing = 3;
            $title = ucwords(str_replace("_", " ", $page_info['material']));
        } else {
            $page_showing = 2;
            $title = ucwords(str_replace("_", " ", $page_info['topic']));
        }
        $page_info["info"] = json_decode(file_get_contents($page_root."/topic.json"), true);
    } else {
        $page_showing = 1;
        $title = ucwords(str_replace("_", " ", $page_info['category']));
        $page_info["info"] = json_decode(file_get_contents($page_root."/category.json"), true);
    }
}


    echo <<<HTML
<link type="text/css" rel="stylesheet" href="/css/learn.css">
<div class="container well">
    <div class="jumbotron page-title">
        <h1>$title</h1>
    </div>
    <br />
    <div class="row-fluid">
HTML;
            switch ($page_showing) {
                case 3:
                $topic_info = json_decode(file_get_contents("categories/{$page_info['category']}/{$page_info['topic']}/topic.json"), true);
                $category_title = ucwords(str_replace("_", " ", $page_info['category']));
                $topic_title = ucwords(str_replace("_", " ", $page_info['topic']));
                echo <<<HTML
<div class="span3">
    <div class="list-group">
        <a class="list-group-item material-side-bar-title side-bar-title">
            <div class="row-fluid">
                <div class="span12">
                    <h4>Material</h4>
                </div>
            </div>
        </a>
HTML;
                foreach ($topic_info['material'] as $material) {
                    $viewed_style = "";
                    if ($material['title'] == $page_info['material']) {
                        $viewed_style = "current-page";
                    }
                        $material_title = ucwords(str_replace("_", " ", $material['title']));
                        echo <<<HTML
    <div class="move-box">
        <a href="/learn/{$page_info['category']}/{$page_info['topic']}/{$material['title']}" class="list-group-item $viewed_style">
            <div class="row-fluid">
                <div class="span12">
                    <h5>$material_title</h5>
                </div>
            </div>
        </a>
    </div>
HTML;
                }

                echo <<<HTML
    </div>
    <ul class="breadcrumb">
        <li><a href="/learn/{$page_info['category']}">$category_title</a></li>
        <li><a href="/learn/{$page_info['category']}/{$page_info['topic']}">$topic_title</a></li>
        <li class="active">$title</li>
    </ul>
</div>
<div class="span9">
HTML;
                include $material_file;
                echo <<<HTML
</div>
HTML;
                break;
                case 2:
                $category_info = json_decode(file_get_contents("categories/{$page_info['category']}/category.json"), true);
                $category_title = ucwords(str_replace("_", " ", $page_info['category']));
                echo <<<HTML
<div class="span3">
    <div class="list-group">
        <a class="list-group-item topic-side-bar-title side-bar-title">
            <div class="row-fluid">
                <div class="span12">
                    <h4>Topics</h4>
                </div>
            </div>
        </a>
HTML;
                foreach ($category_info['topics'] as $topic) {
                    $viewed_style = "";
                    if ($topic['title'] == $page_info['topic']) {
                        $viewed_style = "current-page";
                    }
                        $topic_title = ucwords(str_replace("_", " ", $topic['title']));
                        echo <<<HTML
    <div class="move-box">
        <a href="/learn/{$page_info['category']}/{$topic['title']}" class="list-group-item $viewed_style">
            <div class="row-fluid">
                <div class="span12">
                    <h5>$topic_title</h5>
                </div>
            </div>
        </a>
    </div>
HTML;
                }
                echo <<<HTML
    </div>
    <ul class="breadcrumb">
        <li><a href="/learn/{$page_info['category']}">$category_title</a></li>
        <li class="active">$title</li>
    </ul>
</div>
<div class="span9">
    <div class="list-group">
HTML;
                foreach ($page_info['info']['material'] as $material) {
                    $material_title = ucwords(str_replace("_", " ", $material['title']));
                    echo <<<HTML
        <a href="/learn/{$page_info['category']}/{$page_info['topic']}/{$material['title']}" class="border-fade list-group-item">
            <div class="row-fluid">
                <div class="span3">
                    <img src="http://placehold.it/165x80" />
                </div>
                <div class="span9">
                    <h4 class="list-group-item-heading">$material_title</h4>
                    <p class="list-group-item-text">{$material['description']}</p>
                </div>
            </div>
        </a>
HTML;
                }
                echo <<<HTML
    </div>
</div>
HTML;
                break;
                case 1:
                $categories = array("cryptography", "exploitation", "forensics", "operating_systems", "programming", "web_applications");
                echo <<<HTML
<div class="span3">
    <div class="list-group">
        <a class="list-group-item category-side-bar-title side-bar-title">
            <div class="row-fluid">
                <div class="span12">
                    <h4>Categories</h4>
                </div>
            </div>
        </a>
HTML;
                foreach ($categories as $category) {
                    $viewed_style = "";
                    if ($category == $page_info['category']) {
                        $viewed_style = "current-page";
                    }
                        $category_title = ucwords(str_replace("_", " ", $category));
                        echo <<<HTML
    <div class="move-box">
        <a href="/learn/$category" class="list-group-item $viewed_style">
            <div class="row-fluid">
                <div class="span12">
                    <h5>$category_title</h5>
                </div>
            </div>
        </a>
    </div>
HTML;
                }
                echo <<<HTML
    </div>
    <ul class="breadcrumb">
        <li class="active">$title</li>
    </ul>
</div>
<div class="span9">
    <div class="list-group">
HTML;
                foreach ($page_info['info']['topics'] as $topic) {
                    $topic_title = ucwords(str_replace("_", " ", $topic['title']));
                    echo <<<HTML
        <a href="/learn/{$page_info['category']}/{$topic['title']}" class="border-fade list-group-item">
            <div class="row-fluid">
                <div class="span3">
                    <img src="http://placehold.it/165x80" />
                </div>
                <div class="span9">
                    <h4 class="list-group-item-heading">$topic_title</h4>
                    <p class="list-group-item-text">{$topic['description']}</p>
                </div>
            </div>
        </a>
HTML;
                }
                echo <<<HTML
    </div>
</div>
HTML;
                break;
                case 0:
                    echo <<<HTML
                    <div class="col-xs-18 col-sm-6 col-md-4">
            <a href="/learn/operating_systems/">
              <div class="thumbnail">
                <img src="/images/operating_systems.png" alt="">
                  <div class="caption">
                    <h3>Operating Systems</h3>
                    <p>Ever wanted to know what goes on in that little box that sits on your desk actually does? Operating Systems are one of the most important parts of your computer and knowing how they work is very important for cyber security.</p>
                </div>
              </div>
            </a>
            </div>

            <div class="col-xs-18 col-sm-6 col-md-4">
            <a href="/learn/forensics/">
              <div class="thumbnail">
                <img src="/images/forensics.png" alt="">
                  <div class="caption">
                    <h3>Forensics</h3>
                    <p>Whether it be finding hidden information in a picture (steganography) or locating the password for someone's computer, knowing how to perform cyber forensics will show you how to be a real cyber detective.</p>
                </div>
              </div>
            </a>
            </div>

            <div class="col-xs-18 col-sm-6 col-md-4">
            <a href="/learn/cryptography/">
              <div class="thumbnail">
                <img src="/images/cryptography.png" alt="">
                  <div class="caption">
                    <h3>Cryptography</h3>
                    <p>You see it all the time on detective shows when their "cyber specialist" rambles on about some encrypted message mumbo jumbo. Here you will learn if those people are actually saying a load of nonsense or intelligently describing encryption protocols (hardly the case).</p>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xs-18 col-sm-6 col-md-4">
            <a href="/learn/programming/">
              <div class="thumbnail">
                <img src="/images/programming.png" alt="">
                  <div class="caption">
                    <h3>Programming</h3>
                    <p>Knowing how to program is an invaluable tool for cyber security. Not only does it teach you how to automate tedious tasks, but it helps you understand how your computer works, and how it can be hacked.</p>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xs-18 col-sm-6 col-md-4">
            <a href="/learn/web_applications/">
              <div class="thumbnail">
                <img src="/images/login_form.png" alt="">
                  <div class="caption">
                    <h3>Web Applications</h3>
                    <p>Pretty much everyone uses some sort of browser everyday to check their online information. Did you know that 99% of all websites have some sort of vulnerability that if found could allow a hacker to do bad stuff?</p>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xs-18 col-sm-6 col-md-4">
            <a href="/learn/exploitation/">
              <div class="thumbnail">
                <img src="/images/exploitation.png" alt="">
                  <div class="caption">
                    <h3>Exploitation</h3>
                    <p>If you really want to be a hard core cyber hacker, exploitation will teach you the basics on how to find vulnerabilities and exploit them.</p>
                </div>
              </div>
              </a>
            </div>
HTML;
                break;
            }
        ?>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
