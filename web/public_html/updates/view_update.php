
<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup('updates', $loggedIn);
?>

<div class="container well">
    <?php
        if (!is_numeric($_GET['id'])) {
            echo html_error('Update does not exist');
            exit;
        }

        $dbConn = new DatabaseConn(false);
        $dbConn->set_table('challenge_updates');

        $result = $dbConn->get_item(intval($_GET['id']), 'id');
        $update = $result[0];
        if(!$update) {
            echo html_info('Update does not exist');
            exit;
        } else {
            $date = date("l, F jS, Y \a\\t h:i:s A", $update['date']);
            $title = $update['title'];
            $id = $update['id'];
            $body = nl2br(html_entity_decode(stripcslashes($update['body'])));
            $picture = $update['picture'];
            if (strcmp($picture, "") == 0) {
                $picture = "default.png";
            }
            if (strpos($picture, "http") === false) {
                $picture = "../images/".$picture;
            }
                echo <<<HTML
    <div class="jumbotron">
        <h1>
            $title
        </h1>
    <div class="container" style="margin-top:30px">
        <div class="update row-fluid">
            <div class="span3">
                <img class="update-picture" src="$picture" />
                <h6><em>$date</em></h6>
            </div>
            <div class="span9">
                <p style="margin-left:30px;margin-top:15px;">
                    $body
                </p>
            </div>
        </div>
    </div>
</div>
        <hr/>
HTML;
            }
            ?>
        </div>
    </div>
<?php
    echo html_end_setup();
?>
