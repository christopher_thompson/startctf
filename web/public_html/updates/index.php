<?php
require_once '../php/Require.php';

$loggedIn = login_check();
echo html_begin_setup($loggedIn);
?>

<div class="container well">
    <div class="jumbotron page-title">
        <h1>Updates</h1>
        <div class="container" style="margin-top:30px">
        <?php
            $dbConn = new DatabaseConn(false);
            $dbConn->set_table('challenge_updates');

            $updates = $dbConn->get_all_items();
            if(!$updates) {
                echo html_info('No posts yet.');
            } else {
                foreach ($updates as $row) {
                    $title = $row['title'];
                    $id = $row['id'];
                    $body = nl2br(substr(strip_tags(html_entity_decode(stripcslashes($row['body']))), 0, 300));
                    $picture = $row['picture'];
                    if (strcmp($picture, "") == 0) {
                        $picture = "default.jpg";
                    }
                    if (strpos($picture, "http") === false) {
                        $picture = "../images/".$picture;
                    }
                    echo <<<HTML
<div class="update row-fluid">
	<div class="span3">
		<img class="update-picture" src="$picture" />
	</div>
	<div class="span9">
	    <h2>
        	$title
   		</h2>
    	<p class="update-body" style="margin-left:30px;margin-top:15px;">
        	$body...
    	</p>
    	<a href='view_update.php?id=$id'>
        	Read More
    	</a>
	</div>

</div>
<hr/>
HTML;
                }
            }
            ?>
        </div>
    </div>
</div>
<?php
    echo html_end_setup();
?>
