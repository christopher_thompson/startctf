<?php
require_once "php/Login.func.php";
sec_session_start();

$loggedIn = login_check();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"></meta>
        <title>Start CTF</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"></meta>
        <meta content="Where the fun begins." name="description"></meta>
        <meta content="Christopher Thompson" name="author"></meta>
        <meta name="google-site-verification" content="An3Go0ujU_8-btT0e0BpmZ2bBV7fxUgNFPkXO-kRTtU" />
        <link rel="shortcut icon" href="images/favicon.ico?v=4"></link>
        <link rel="stylesheet" href="css/bootstrap.min.css"></link>
        <link rel="stylesheet" href="css/main-page.css"></link>
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css"></link>
        <link rel="stylesheet" href="css/jquery-ui-1.10.4.custom.min.css"></link>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="js/jquery.onepage-scroll.min.js"></script>
        <link href='css/onepage-scroll.css' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    </head>
    <body>
            <!--====Navigation Bar====-->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;margin-bottom:0px">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Start CTF</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="#">Wiki</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="learn/">Learn</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="about/">About</a></li>
                            <li><a href="chat/">Chat</a></li>
                            <li><a href="challenges/">Challenges</a></li>
                            <li><a href="scoreboard/">Scoreboard</a></li>
                            <li><a href="updates/">Updates</a></li>
                            <?php if ($loggedIn) { echo "<li><a href=\"shell/\">Shell</a></li>"; } ?>
                            <?php
                                if (!$loggedIn) {
                                    echo '<li><a href="register/">Register</a></li>';
                                    echo '<li><a href="login/">Login</a></li>';
                                } else {
                                    echo '<li><a href="account/">Account</a></li>';
                                    echo '<li><a href="login/logout.php">Logout</a></li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!--====End Navigation Bar====-->
            <!--====Website Header====-->
            <!--====End Website Header====-->
            <div class="wrapper">
            <div class="main">
                <section id="page1">
                    <div class="page_container">
                        <center>
                            <div class="jumbotron page-title">
                                <h1>Start CTF</h1>
                                <h2>A beginning for everyone in cyber security</h2>
                            </div>
                        </center>
                        <div class="header-images row-fluid">
                            <div class="span4">
                                <img src="images/lightbulb.png?v=2" />
                                <h1>Learn.</h1>
                            </div> 
                            <div class="span4">
                                <img id="controller-image" src="images/controller.png" />
                                <h1>Play.</h1>
                            </div>
                            <div class="span4">
                                <img src="images/smiley.png?v=2" />
                                <h1>Have fun.</h1>
                            </div>
                        </div>
                    </div>
                    <img src="images/power_button.png" class="large-image" id="power_button" style="display:none"/>
                </section>
                <section id="page2">
                    <div class="page_container">
                        <center>
                            <div class="jumbotron slide-title" style="padding-bottom:0px;padding-top:0px;">
                                <h1>What on earth is a "CTF"?</h1>
                            </div>
                        </center>
                        <div class="header-images row-fluid">
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">"Capture the Flag"</h3>
                                    </div>
                                    <div class="panel-body">
                                        CTF stands for "Capture the Flag" where you are given a challenge that you have to solve using your awesome computer science skills. You know you will have solved it when you see a <a target="_blank">flag</a>.
                                    </div>
                                </div>
                            </div> 
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Solve Challenges, Earn Points</h3>
                                    </div>
                                    <div class="panel-body">
                                        Once you find a flag, you go back to the challenge page and enter it into the answer box. You will earn a certain number of points depending on the difficulty of the challenge (more difficult, more points).
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Learn about Cyber Security</h3>
                                    </div>
                                    <div class="panel-body">
                                        As you try to solve these challenges, you will actually be learning a whole lot about cyber security. We want to show you just how cool cyber security can be and the first step to doing this is for you to <a target="_blank" href="learn/">learn</a> about the different topics which make cyber security up.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="images/flag.png" class="large-image" id="flag" />
                </section>
                <section id="page3">
                    <div class="page_container">
                        <center>
                            <div class="jumbotron slide-title" style="padding-bottom:0px;padding-top:0px;">
                                <h1>Why would I do this?</h1>
                            </div>
                        </center>
                        <div class="header-images row-fluid">
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Learn how to hack</h3>
                                    </div>
                                    <div class="panel-body">
                                        Ever seen those people in the movies who press a few buttons and hack into the most secure building in the world? In case you did not know, one does not simply "hack" into something. You will learn about cyber hacking techniques and the next time that you watch an crime show, you will be able to sit back and laugh at them magically <a target="_blank" href="https://www.youtube.com/watch?v=LhF_56SxrGk">"enhancing" their images</a> (if you want to learn why you cannot "enhance" an image as in enlarging it, take a look <a target="_blank" href="https://answers.yahoo.com/question/index?qid=20090205134857AAA6Dxt">here</a>.
                                    </div>
                                </div>
                            </div> 
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Solve puzzles</h3>
                                    </div>
                                    <div class="panel-body">
                                        When you really think about it, cyber hacking is just a different kind of puzzle. Start CTF will introduce you to the different types of puzzles that exist in cyber security and it will be up to you to figure out how they work and solve them.
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Compete against your friends</h3>
                                    </div>
                                    <div class="panel-body">
                                        Everyone wants to be better than each other. But do you really want to best someone in a mindless activity like "Flappy Bird" or "2048"? Hacking is the true test of intellegence as you have to be creative in how you go about the challenges. Start CTF will allow you to show up your friends in a friendly form of competition.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="page4">
                    <div class="page_container">
                        <center>
                            <div class="jumbotron slide-title" style="padding-bottom:0px;padding-top:0px;">
                                <h1>How do I play?</h1>
                            </div>
                        </center>
                        <div class="header-images row-fluid">
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Challenge Viewer</h3>
                                    </div>
                                    <div class="panel-body">
                                        TODO animate a moc up of challenge viewer 
                                    </div>
                                </div>
                            </div> 
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Download or do it online</h3>
                                    </div>
                                    <div class="panel-body">
                                        TODO animate downloading challenge or doing it online
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Find flag, earn points</h3>
                                    </div>
                                    <div class="panel-body">
                                        TODO animate solving challenge and entering it in on site
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="page5">
                    <div class="page_container">
                        <center>
                            <div class="jumbotron slide-title" style="padding-bottom:0px;padding-top:0px;">
                                <h1>I have no experience in hacking, what should I do?</h1>
                            </div>
                        </center>
                        <div class="header-images row-fluid">
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Vist our "Learn" page</h3>
                                    </div>
                                    <div class="panel-body">
                                        One of the most important things that we wanted to focus on was getting people comfortable with cyber security so that they would feel confident compeleting our challenges. Our <a target="_blank" href="learn/">"Learn"</a> page has a whole bunch of stuff directly related to the challenges we have. So if you are not really sure what <a target="_blank" href="learn/?page=linux_commands">linux commands</a>, <a target="_blank" href="learn/?page=sql_injection">SQL injection</a> or anything else cyber related is, make sure you at least take a look at our page.
                                    </div>
                                </div>
                            </div> 
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">We are here to help you learn</h3>
                                    </div>
                                    <div class="panel-body">
                                        Our primary drive to make this competition was to get people of all kinds of educational backgrounds interested in cyber security (and hacking). In order to fulfill our goal, we want to make sure that you are able to actually understand the cyber security topics that we present to you. If you really just are not understanding something, feel free to contact us with your questions and we will point you in the right direction.
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Chat with us</h3>
                                    </div>
                                    <div class="panel-body">
                                        We will be available to chat with you on our <a target="_blank" href="chat/">chat page</a> at any time during the competition.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="page6">
                    <div class="page_container">
                        <center>
                            <div class="jumbotron slide-title" style="padding-bottom:0px;padding-top:0px;">
                                <h1>Well...</h1>
                                <h1>What are you waiting for? Press Start!</h1>
                            </div>
                        </center>
                    </div>
                </section>
                <section id="page6">
                    <div class="page_container">
                        <div class="page-header" style="border-bottom: 1px solid #444444">
                            <h3>
                            A little about this site...
                            </h3>
                        </div>
                        <div class="well" >
                            <p>
                            The Start CTF competition was developed in order to allow students to either learn or build upon computer science topics that are frequently used in the cyber security and forensics realm of learning. We hope to provide those who compete with both experience and knowledge in working with challenges that many in this industry face everyday. Through the implementation of a fun and competative challenge which introduces topics at a basic level of comprehension, we are able to attract a variety of students which are not strictly focused on learning computer science, but find puzzle solving and abstract thinking enjoyable. To all those competing good luck, and hack on! 
                            </p>
                        </div>
                    </div>
                </section>
        </div>
        </div>
    </body>
    <!--====Script Loading (At the end to speed up loading time)====-->
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="../js/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <script type="text/javascript" src="../js/main_page.js"></script>
    <script type="text/javascript" src="../js/suchjavascript.js"></script>
	<script>
	  $(document).ready(function(){
      $(".main").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: 770,
        loop: false
      });
		});
		
	</script>
</html>
