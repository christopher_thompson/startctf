<?php
/* *
 *  Generates an error popup for the given error message
 * */
function html_error ($error) {
    return "<div class='alert alert-danger'><strong>Oh noes!: </strong>$error</div>";
}

/* *
 *  Generates a success popup for the given success message
 * */
function html_success ($msg) {
    return "<div class='alert alert-success'><strong>Success baby says: </strong>$msg</div>";
}

/* *
 *  Generates an info popup for the given info message
 * */
function html_info ($info) {
    return "<div class='alert alert-info'><strong>Just so you know: </strong>$info</div>";
}

function check_page_access ($page) {
    $db_conn = new DatabaseConn(false);
    $page = $db_conn->sanitize_variable($page);
    $result = $db_conn->query("SELECT value FROM global_variables WHERE variable='$page'");
    return intval($result[0]['value']);
}

/* *
 * Generates the html meta data for all of the webpages
 * The title of the given page can be changed if need be
 * */
function html_meta_info ($title = null) {
    if ($title == null) {
        $title = "Start CTF";
    }
    return <<<HTML
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <title>$title</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Home of the StartCTF competition." name="description">
        <meta content="Christopher Thompson" name="author">
        <link rel="shortcut icon" href="/images/favicon.ico?v=2">
        <link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/bootstrap-responsive.min.css">
        <link type="text/css" rel="stylesheet" href="/css/main-style.css?v=2">
        <link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.10.4.custom.min.css">
        <link type="text/css" rel="stylesheet" href="/css/jquery.pnotify.default.css">
        <link type="text/css" rel="stylesheet" href="/css/jquery.pnotify.default.icons.css">
        <script type="text/javascript" src="/js/jquery-1.10.2.js"></script>
    </head>
HTML;
}

function html_nav_bar ($loggedIn) {
    $login_str = "Login";
    $navbar_str = ""; 
    $login_nav = "/login";
    if ($loggedIn) {
        $login_str = "Log Out";
        $login_nav = "/login/logout.php";
        $navbar_str = <<<HTML
        <li><a id="shell-link" href="/shell">Shell</a></li>
        <li><a id="account-link" href="/account">Account</a></li>
HTML;
    } else {
        $navbar_str = '<li><a id="register-link" href="/register">Register</a></li>';
    }

    return <<<HTML
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="position: relative;">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Start CTF</a>
        </div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a id="wiki-link" href="#">Wiki</a></li>
            <li><a id="faq-link" href="#">FAQ</a></li>
            <li><a id="learn-link" href="/learn">Learn</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a id="about-link" href="/about">About</a></li>
            <li><a id="chat-link" href="/chat">Chat</a></li>
            <li><a id="challenges-link" href="/challenges">Challenges</a></li>
            <li><a id="scoreboard-link" href="/scoreboard">Scoreboard</a></li>
            <li><a id="updates-link" href="/updates">Updates</a></li>
            $navbar_str
            <li><a id="login-link" href="$login_nav">$login_str</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
HTML;

}

/* *
 * Generates the html for the top of every webpage
 * */
function html_begin_setup ($loggedIn, $title = null) {
    return html_meta_info($title) . "\n".
           '<body><div class="mainsection">' .
           html_nav_bar($loggedIn) . "\n" .
           '<div class="container">';
}

/* *
 * Generates the html for the end of every webpage
 * */
function html_end_setup () {
    return <<<HTML
    </div>
</div>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery.pnotify.min.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/suchjavascript.js"></script>
</body>
</html>
HTML;
}

function decodeAndStripHTML($string){
    return strip_tags(htmlspecialchars_decode($string));
}

