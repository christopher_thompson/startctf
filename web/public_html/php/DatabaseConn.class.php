<?php
require_once 'Global.vars.php';

class DatabaseConn
{
    /* *
     * Variable: $_conn
     * Description: The connection to the database given the 
     * DatabaseConn.vars variables. It is also a mysqli object
     * */
    private $_conn;

    /* *
     * Variable: $_table
     * Description: The table name that is currently being
     * queried by the client
     * */
    private $_table;

    /* *
     * Function: __construct
     * @param $admin (boolean): Determines if database is being
     * queried as an admin user
     * @param $database (string): Determines the database being
     * used by the client
     * Description: The constructor which creates a new DatabaseConn object
     * */
    public function __construct ($admin = false, $database = null) {
        global $HOST;
        global $USER;
        global $PASSWORD;
        global $DATABASE;
        if ($database == null) {
            $database = $DATABASE;
        }

        if ($admin) {
            global $ADMIN_USER;
            global $ADMIN_PASSWORD;
            $this->_conn = new mysqli($HOST, $ADMIN_USER, $ADMIN_PASSWORD, $database);
            if ($this->_conn->connect_errno) {
                error_log("Failed to connect to MySQL: " . $this->_conn->connect_error);
            }
        } else {
            $this->_conn = new mysqli($HOST, $USER, $PASSWORD, $database);
            if ($this->_conn->connect_errno) {
                error_log("Failed to connect to MySQL: " . $this->_conn->connect_error);
            }
        }
    }

    function get_global ($global) {
        return $this->query("SELECT value FROM global_variables WHERE name = $global");
    }

    function set_global ($global, $value) {
        return $this->query("UPDATE global_variables SET $global = $value WHERE name = $global");
    }


    /* *
     * Function: add_item
     * @param $values (array): Contains a row of values (in the corresponding order
     * for the table) to insert into the table
     * @param $col_names (array): Specifies the names of the columns for the 
     * particular table
     * @param $col_types (array): Specifies the types of variables which going to 
     * be put in the row (ex. ('s', 'i') for a row with two columns that are of 
     * string and integer datatypes respectively
     * Description: Adds an item to the set table given the values and optional 
     * column names and column types
     * */
    public function add_item ($values, $col_names = null, $col_types = null) {
        if (!$this->_table) {
            error_log("Table is not set");
            return false;
        }

        $table = $this->sanitize_variable($this->_table);
        if ($col_names == null) {
            $col_names = $this->get_table_attributes($table, 'columns');
        }

        if ($col_types == null) {
            $col_types = $this->get_table_attributes($table, 'types');
        }

        if (strcmp($col_names[0], "id") == 0) {
            $col_names = array_slice($col_names, 1);
            $col_types = array_slice($col_types, 1);
        }

        if (count($values) != count($col_names)) {
            $ser_values = serialize($values);
            $ser_col_names = serialize($col_names);
            error_log("Number of values given ".count($values)." do not match the expected amount ".count($col_names)." $ser_col_names: for $ser_values");
            return false;
        }

        $escaped_values = array();
        foreach ($values as $value) {
            array_push($escaped_values, $this->sanitize_variable($value));
        }

        $question_marks = array();
        foreach ($col_types as $col_type) {
            array_push($question_marks, '?');
        }

        $prep_stmt = "INSERT INTO $table (".join(',', $col_names) .
            ") VALUES (".join(", ", $question_marks).")";

        if ($stmt = $this->_conn->prepare($prep_stmt)) {
            $stmt = $this->dynamicBindVariables($stmt, $escaped_values);
            $stmt->execute();
            return $stmt->insert_id;
        } else {
            error_log("Add item query: $prep_stmt, unable to perpare mysqli: " . mysqli_error($this->_conn));
        }

        return false;
    }

    /* *
     * Function: get_item
     * @param $value (string): The value to search for in the column
     * @param $column (string): The column to search through for value
     * Description: Return a row of values where the value variable
     * appears in the column
     * */
    public function get_item ($value, $column) {
        if (!$this->_table) {
            return false;
        }

        $column = $this->sanitize_variable($column);
        $value = $this->sanitize_variable($value);
        $table = $this->sanitize_variable($this->_table);

        $prep_stmt = "SELECT * FROM $table WHERE $column = ? LIMIT 1";

        if ($stmt = $this->_conn->prepare($prep_stmt)) {
            $stmt->bind_param("s", $value);
            $stmt->execute();

            $meta = $stmt->result_metadata();
            while ($field = $meta->fetch_field())
            {
                $params[] = &$row[$field->name];
            }

            call_user_func_array(array($stmt, 'bind_result'), $params);

            $result = array();
            while ($stmt->fetch()) {
                foreach($row as $key => $val)
                {
                    $c[$key] = $val;
                }
                $result[] = $c;
            } 

            return $result;
        } else {
            error_log("get item query unable to perpare mysqli: " . mysqli_error($this->_conn));
        }

        return false;
    }

    /* *
     * Function: get_like_item
     * @param $text (string): The text to search for in the column
     * @param $column (string): The column to search through for text
     * Description: Uses the sql LIKE search function to do a wild card
     * search for the text variable in the given column
     * */
    public function get_like_item ($text, $column) {
        if (!$this->_table) {
            return false;
        }

        $text = "%" . $this->sanitize_variable($text) . "%";
        $column = $this->sanitize_variable($column);
        $table = $this->sanitize_variable($this->_table);

        $prep_stmt = "SELECT * FROM $table WHERE $column LIKE ?";

        if ($stmt = $this->_conn->prepare($prep_stmt)) {
            $stmt->bind_param("s", $text);
            $stmt->execute();

            $meta = $stmt->result_metadata();
            while ($field = $meta->fetch_field())
            {
                $params[] = &$row[$field->name];
            }

            call_user_func_array(array($stmt, 'bind_result'), $params);
            $result = array();
            while ($stmt->fetch()) {
                foreach($row as $key => $val)
                {
                    $c[$key] = $val;
                }
                $result[] = $c;
            } 

            return $result;
        } else {
            error_log("get like item query unable to perpare mysqli: " . mysqli_error($this->_conn));
        }

        return false;
    }

    /* *
     * Function: get_all_items
     * @param $sort_column (string): Optional to sort the rows by a value 
     * Description: Returns all rows of the given database given an optional
     * sorting variable (default is "id")
     * */
    public function get_all_items ($sort_column = "id") {
        if (!$this->_table) {
            return false;
        }
        $table = $this->sanitize_variable($this->_table);

        $col_names = $this->get_table_attributes($table, 'columns');
        if (!in_array($sort_column, $col_names)) {
            $sort_column = "id";
        }

        $table = $this->sanitize_variable($this->_table);
        $sql = "SELECT * FROM $table ORDER BY $sort_column DESC";
        if (!$result = $this->_conn->query($sql)) {
            error_log("get all items query error: $sql : " . mysqli_error($this->_conn));
            return array();
        }
        $items = array();
        while ($row = $result->fetch_assoc()) {
            array_push($items, $row);
        }
        $result->free();
        return $items;
    }

    public function edit_item ($value, $column, $id, $id_column = null) {
        if (!$this->_table) {
            error_log("Table is not set");
            return false;
        }

        $table = $this->sanitize_variable($this->_table);
        $value = $this->sanitize_variable($value);
        $id = $this->sanitize_variable($id);
        $column = $this->sanitize_variable($column);
        $col_names = $this->get_table_attributes($table, 'columns');

        /* TODO get columns from database and not statically get them in program
        if (!in_array($column, $col_names)) {
            error_log("Column given: $column is not a column of the table: {$this->_table}");
            return false;
        }
        */

        $prep_stmt = "";

        if ($id_column == null)
            $prep_stmt = "UPDATE $table SET $column=? WHERE id=?";
        else 
            $prep_stmt = "UPDATE $table SET $column=? WHERE $id_column=?";

        if ($stmt = $this->_conn->prepare($prep_stmt)) {
            $stmt->bind_param("si", $value, $id);
            $stmt->execute();
            return true;

        } else {
            error_log("Edit item query unable to perpare mysqli: " . mysqli_error($this->_conn));
        }

        return false;
    }

    public function edit_row ($values, $id) {
        if (!$this->_table) {
            error_log("Table is not set");
            return false;
        }

        $table = $this->sanitize_variable($this->_table);
        $escaped_id = $this->sanitize_variable($id);
        $col_names = $this->get_table_attributes($table, 'columns');
        $col_types = $this->get_table_attributes($table, 'types');

        if (strcmp($col_names[0], "id") == 0) {
            $col_names = array_slice($col_names, 1);
            $col_types = array_slice($col_types, 1);
        }

        if (count($values) != count($col_names)) {
            error_log("Number of values given ".count($values)." do not match the expected amount ".count($col_names));
            return false;
        }

        $escaped_values = array();
        foreach ($values as $value) {
            array_push($escaped_values, $this->sanitize_variable($value));
        }

        $question_marks = array();
        foreach ($col_names as $col_name) {
            array_push($question_marks, $col_name.' = ?');
        }

        $prep_stmt = "UPDATE $table SET ".join(', ', $question_marks)." WHERE id=$escaped_id";
        
        if ($stmt = $this->_conn->prepare($prep_stmt)) {
            $stmt = $this->dynamicBindVariables($stmt, $escaped_values);
            $stmt->execute();
            return true;
        } else {
            error_log("Edit row query unable to perpare mysqli: " . mysqli_error($this->_conn));
        }

        return false;
    }

    public function delete_item ($value, $column) {
        if (!$this->_table) {
            return false;
        }

        $value = $this->sanitize_variable($value);
        $column = $this->sanitize_variable($column);
        $table = $this->sanitize_variable($this->_table);
        $sql = "DELETE FROM $table WHERE $column = ?";

        if ($stmt = $this->_conn->prepare($sql)) { 
            $stmt->bind_param('i', $value);
            $stmt->execute();
            return true;
        } else {
            if ($stmt == false) {
                error_log($this->_conn->error);
            } else {
                error_log($stmt->error);
            }
            return false;
        }
    }

    public function query ($sql) {
        $result = $this->_conn->query($sql);

        if (!$result) {
            error_log("Unable to query: $sql. ".$this->_conn->error);
            return false;
        }

        if (is_bool($result) && $result == 1) 
            return true;

        $results = array();
        while ($row = $result->fetch_assoc()) {
           $results[] = $row;
        }

        return $results;
    }

    public function get_table_attributes($table, $data) {
        $table_array = array(
            'teams' => array(
                'columns' => array('id', 'name', 'join_code', 'member_1', 'member_2', 'member_3', 'score'),
                'types' => array('i', 's', 's', 'i', 'i', 'i', 'i')
            ),
            'shell_credentials' => array(
                'columns' => array('team_id', 'shell_user', 'shell_pass'),
                'types' => array('i', 's', 's')
            ),
            'challenge_list' => array(
                'columns' => array('prog_id', 'title', 'description', 'hint', 'difficulty', 'points', 'topic'),
                'types' => array('s', 's', 's', 's', 'i', 'i', 's')
            ),
            'challenge_updates' => array(
                'columns' => array('id', 'title', 'body', 'date', 'picture'),
                'types' => array('i', 's', 's', 'i', 's')
            ),
            'members' => array(
                'columns' => array('id', 'first_name', 'last_name', 'team_id', 'email', 'password', 'salt'),
                'types' => array('i', 's', 's', 'i', 's', 's', 's')
            ),
            'cookies' => array(
                'columns' => array('id', 'user_id', 'email', 'ip', 'user_agent', 'epoch', 'login_string'),
                'types' => array('i', 'i', 's', 's', 's', 'i', 's')
            ),
            'global_variables' => array(
                'columns' => array('variable', 'value'),
                'types' => array('s', 'i')
            ),
            'admins' => array(
                'columns' => array('user_id'),
                'types' => array('i')
            ),
            'waiting_registration' => array(
                'columns' => array('team_id'),
                'types' => array('i')
            )
        );
        
        if (array_key_exists($table, $table_array)) {
            if ($data == "columns") {
                return $table_array[$table]['columns'];
            } else {
                if ($data == "types") {
                    return $table_array[$table]['types'];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    //**********************************************************************************************
    //*****************Other seamingly meaningless functions****************************************
    //**********************************************************************************************

    public function sanitize_variable ($arg) {
        return htmlspecialchars($this->_conn->real_escape_string($arg));
    }

    public function get_column_names () {
        $result = mysql_query("SHOW COLUMNS FROM {$this->_table}");
        if (!$result) {
            error_log("Unable to get columns for table: $table. ".mysql_error());
            return false;
        }

        $column_names = array();
        while ($row = mysql_fetch_assoc($result)) {
           $column_names[] = $row;
        }

        return $column_names;
    }

    public function redirect ($uri) {
        header('location:'.$uri);
        exit;
    }

    public function get_conn () {
        return $this->_conn;
    }

    public function set_table ($table) {
        $this->_table = $table;
    }

    public function dynamicBindVariables($stmt, $params)
    {
        if ($params != null)
        {
            // Generate the Type String (eg: 'issisd')
            $types = '';
            foreach($params as $param)
            {
                if(is_int($param)) {
                    // Integer
                    $types .= 'i';
                } elseif (is_float($param)) {
                    // Double
                    $types .= 'd';
                } elseif (is_string($param)) {
                    // String
                    $types .= 's';
                } else {
                    // Blob and Unknown
                    $types .= 'b';
                }
            }
      
            // Add the Type String as the first Parameter
            $bind_names[] = $types;
      
            // Loop thru the given Parameters
            for ($i=0; $i<count($params);$i++)
            {
                // Create a variable Name
                $bind_name = 'bind' . $i;
                // Add the Parameter to the variable Variable
                $$bind_name = $params[$i];
                // Associate the Variable as an Element in the Array
                $bind_names[] = &$$bind_name;
            }
             
            // Call the Function bind_param with dynamic Parameters
            call_user_func_array(array($stmt, 'bind_param'), $bind_names);
        }
        return $stmt;
    }
}

