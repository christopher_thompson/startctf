<?php
    require_once "DatabaseConn.class.php";

    function add_challenge($prog_id, $title, $description, $hint, $difficulty, $points, $solution, $custom_solution = false, $topic = "web_applications") {
        $db_conn = new DatabaseConn(true);
        $db_conn->set_table("challenge_list");
        $db_conn->add_item(array($prog_id, $title, $description, $hint, $difficulty, $points, $topic));
        $db_conn->set_table("challenge_teams");
        $db_conn->query("ALTER TABLE challenge_teams ADD $prog_id VARCHAR(255)");
        $teams = $db_conn->query("SELECT team_id FROM challenge_teams");
        foreach ($teams as $team) {
            $team = $team['team_id'];
            $db_conn->edit_item(0, $prog_id, $team, "team_id");
        }
        $db_conn->set_table("challenge_solutions");
        $db_conn->query("ALTER TABLE challenge_solutions ADD $prog_id VARCHAR(255)");
        if ($topic == "web_applications") {
            $teams = $db_conn->query("SELECT team_id FROM challenge_solutions");
            foreach ($teams as $team) {
                $team = $team['team_id'];
                $team_solution = "";
                if ($custom_solution)
                    $team_solution = $solution;
                else
                    $team_solution = $solution . "_" . substr(hash('md5', $prog_id.$team), 0, 9);
                $db_conn->edit_item($team_solution, $prog_id, $team, "team_id");
            }
        }
    }

    add_challenge("bbnt_bank", "Registered for Failure", "As the investigator of a case, you have to gather information about your suspect's bank records. Being the vigilante your are, you don't wait for the search warrant to be approved by the judge and decide to see if you can bypass the bank's security to get the information you seek. Perusing the site, you find a registration page that seems like a good place to gain access to the bank...", "", 3, 250, "the_suspect_is_innocent");

    add_challenge("flying_helix", "Unorthodox Undertaking", "You have a friend who has recently been watching Twitch Plays Pokemon and playing Flappy Bird obsessively. Your friend decided to mix his odd addictions by creating Flying Helix. He told you that the game had an easter egg (a hidden part to the game) and challenged you to find it.", "You will have to look at the <em>source</em> of your friend's problem.", 1, 750, "easter_eggs_are_fun_314159265", true);

    add_challenge("html_1", "School Project", "Your friend (with the strange obsession with Twitch Plays Pokemon and Flappy Bird) has an older sister who has begun to learn how to code in HTML and PHP in school. She made a login page for her website, but had not finished the entire site yet and removed the Administrator's username from the drop down box and disabled the password box. You notice that her \"security\" measures were not very secure and you decide to show her how someone could easily log into the website.", "Anything and everything on a webpage can be changed to suit your needs.", 2, 100, "dont_leave_things_hidden");

    add_challenge("insecure_security", "Insecure Security", "One of the most infamous \"security\" agencies in the business, Insecure Security INC., has been tricking businesses into thinking their systems are \"secure\" for years. You decide that you have had enough of this company taking money from businesses and leaving gaping security holes. Their site is just as insecure as their practices (go figure) and from your experiences as a website developer, you know people like to have hidden files on the website that contain information for gaining access to the entire site.", "This is a hard one. You are on your own.", 5, 500, "this_site_has_met_a_terrible_fate");

    add_challenge("javascript_1", "Javascript 1", "Being a part of a website programming team, you and your co-workers tend to play tricks on each other. Recently, someone told you that one of your webpages has fallen victim to a prank.", "The answer is there somewhere.", 1, 500, "javascript_is_fun");

    add_challenge("javascript_2", "Alert!", "Since you are a respected website programmer at your company, you were asked to mentor an intern who did not have much experience with security in website development. At first glance of the intern's webpage, he needs to learn a lot about security. Find the flag that the intern left on the webpage.", "Do you even need to know the password? Ajax is your friend.", 2, 750, "this_is_not_really_secure_at_all");

    add_challenge("javacrypt_1", "Javacrypt 1", "After spending hours watching cat videos on Youtube, you decide to go somewhere else on the Internet that will waste even more of your time. You take a glance at the message board that you operate called \"Windows is Insecure, Don't Know What For\" to find that an anonymous user has created a new post under your \"Answer to Life, the Universe and Everything\" section. Being the curious person you are, you decide to take a crack at it.", "To get the flag, you have to first understand what is happening to it. It would also help to hitch hike a ride.", 2, 500, "childs_play");

    add_challenge("javacrypt_2", "Javacrypt 2", "After the anonymous user posted the challenge on your message board, you recieved an email you assumed was from the same user as the subject line was \"Did you have fun?\". Opening the email up you see another link to a challenge. You start to wonder why this user has been sending you these challenges but your curiosity quickly consumes your thoughts and persuades you to try the challenge give to you.", "Just like before.", 4, 750, "that_was_pretty_impressive_not_bad");

    // Non-online challenges

/*

import csv
import sys

f = open("asdf", 'rt')
try:
    reader = csv.reader(f)
    for row in reader:
      print "add_challenge(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"\", false, \"%s\");" % (row[0], row[1], row[2], row[3], row[4], row[5], row[6])
finally:
    f.close()

*/

?>
