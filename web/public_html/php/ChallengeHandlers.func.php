<?php
    require_once dirname(__FILE__) . "/DatabaseConn.class.php";

    function get_challenge_solution($user_id, $challenge) {
        $login_conn = new DatabaseConn(false, 'secure_login');
        $db_conn = new DatabaseConn(false);
        $user_id = $db_conn->sanitize_variable($user_id);
        $challenge = $db_conn->sanitize_variable($challenge);
        if (is_numeric($user_id)) {
            $result = $login_conn->query("SELECT team_id FROM members WHERE id = $user_id");
            $team_id = $result[0]['team_id'];
            $result = $db_conn->query("SELECT $challenge FROM challenge_solutions WHERE team_id = $team_id");
            $solution = $result[0][$challenge];
            return $solution;
        } else {
            return false;
        }
    }

    function add_team_to_challenges($team_id, $dbConn) {
        $dbConn->set_table('challenge_teams');
        if ($dbConn->get_item($team_id, "team_id")) {
            error_log("Attempting to add team when team id already exists in table");
            return false;
        } 
        $column_details = $dbConn->query("SHOW COLUMNS FROM challenge_teams");
        $values = array();
        $column_names = array();
        $column_types = array();
        $values[] = $team_id;
        $column_types[] = "i";
        $column_names[] = "team_id";
        for ($i = 1; $i < count($column_details) - 1; $i++) {
            $values[] = 0;
            $column_names[] = $column_details[$i]["Field"];
            $column_types[] = "i";
        }

        $dbConn->add_item($values, $column_names, $column_types);
    }

    function add_team_to_solutions($team_id, $dbConn) {
        $challenges = json_decode(file_get_contents("challenge_flags.json", true), true);
        $dbConn->set_table('challenge_solutions');
        if ($dbConn->get_item($team_id, "team_id")) {
            error_log("Attempting to add team when team id already exists in table");
            return false;
        } 
        $column_details = $dbConn->query("SHOW COLUMNS FROM challenge_solutions");
        $values = array();
        $column_names = array();
        $column_types = array();
        $values[] = $team_id;
        $column_types[] = "i";
        $column_names[] = "team_id";
        for ($i = 1; $i < count($column_details) - 1; $i++) {
            $prog_id = $column_details[$i]["Field"];
            if ($challenges[$prog_id]["custom"] == "true") {
                $values[] = $challenges[$prog_id]["base"];
            } else {
                $values[] = $challenges[$prog_id]["base"] . "_" . substr(hash('md5', $prog_id.$team_id), 0, 9);
            }
            $column_names[] = $prog_id;
            $column_types[] = "s";
        }

        $dbConn->add_item($values, $column_names, $column_types);
    }

    function add_challenge_to_challenges($title, $description, $dbConn) {
        $dbConn->set_table('challenge_list');
        // Insert row into challenge list and retrieve the challenge id
        $dbConn->set_table('challenge_teams');
        // Take the id from the challenge and stick it in the challenge_teams table
        // with the command "alter table $this->_table add column $challenge_id varchar (20);"
    }

    function remove_team_from_challenges($team_id, $dbConn) {
        $dbConn->set_table('challenge_teams');
        // Remove row from challenge_teams where team_id is $team_id
        return $dbConn->delete_item($team_id, "team_id");
    }

    function remove_challenge_from_challenges($id, $dbConn) {
        $dbConn->set_table('challenge_list');
        // Remove row from challenge_list with id of $int_escaped_id
        $dbConn->set_table('challenge_teams');
        // Remove coulmn from challenge_teams with "alter table $dbConn->_table remove column $int_escaped_id;"
    }

    function createTeam($name, $join_code, $db_conn) {
       // Insert team into teams, challenge teams as well as shell_creds 
       // Into teams
       $db_conn->set_table("teams");
       $team_id = $db_conn->add_item(array($name, $join_code, 0, 0, 0, 0));
       // Into challenge_teams
       add_team_to_challenges($team_id, $db_conn);
       add_team_to_solutions($team_id, $db_conn);

       $db_conn->set_table("waiting_registration");
       $db_conn->add_item(array($team_id));
       return $team_id;
    }
?>
