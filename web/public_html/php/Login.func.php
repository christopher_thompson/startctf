<?php
require_once 'DatabaseConn.class.php';

function sec_session_start() {
    $session_name = 'sec_session_id'; // Set a custom session name
    $secure = false; // Set to true if using https.
    $httponly = true; // This stops javascript being able to access the session id. 

    ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
    $cookieParams = session_get_cookie_params(); // Gets current cookies params.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name); // Sets the session name to the one set above.
    session_start(); // Start the php session
    session_regenerate_id(true); // regenerated the session, delete the old one.
}

function login($email, $password, $remember_me) {
    $db_conn = new DatabaseConn(true, 'secure_login');
    $mysqli = $db_conn->get_conn();
    $email = $mysqli->real_escape_string($email);

    if ($stmt = $mysqli->prepare("SELECT id, first_name, last_name, team_id, email, password, salt FROM members WHERE email = ? LIMIT 1")) { 
        $stmt->bind_param('s', $email); // Bind "$email" to parameter.
        $stmt->execute(); // Execute the prepared query.
        $stmt->store_result();
        $stmt->bind_result($id, $first_name, $last_name, $team_id, $db_email, $db_password, $salt); // get variables from result.
        $stmt->fetch();
        $password = hash('sha512', $password.$salt); // hash the password with the unique salt.
        $db_password = hash('sha512', $db_password.$salt);
        if($stmt->num_rows == 1) { // If the user exists
            // We check if the account is locked from too many login attempts
            if(checkbrute($id, $mysqli) == true) { 
                // Account is locked
                // Send an email to user saying their account is locked
                return false;
            } else {
                if(strcmp($db_password, $password) == 0) { // Check if the password in the database matches the password the user submitted. 
                    // Password is correct!
                    $user_agent = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
                    $id = preg_replace("/[^0-9]+/", "", $id); // XSS protection as we might print this value
                    $first_name = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $first_name);
                    $last_name = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $last_name);
                    $_SESSION['id'] = $id; 
                    $_SESSION['email'] = $email;
                    $_SESSION['team_id'] = $team_id;
                    $_SESSION['first_name'] = $first_name;
                    $_SESSION['last_name'] = $last_name;
                    $login_string = hash('sha512', $password.$user_agent);
                    $_SESSION['login_string'] = $login_string;
                    if (strcmp($remember_me, "true") == 0) {
                        $db_conn->set_table("cookies");
                        $current_time = time();
                        $ip = get_client_ip();
                        $user_agent_hash = hash('md5', $user_agent);
                        $db_conn->query("DELETE FROM cookies WHERE user_agent = '$user_agent_hash' AND ip = '$ip'");
                        $cookie_ver = hash('sha512', $email.$login_string.$current_time.$ip);
                        $cookie_id = $db_conn->add_item(array($id, $db_email, $ip, $user_agent_hash, $current_time, $login_string));
                        if (isset($_COOKIE["cookie_id"])) {
                            $set_cookie_id = $db_conn->sanitize_variable($_COOKIE['cookie_id']);
                            $db_conn->delete_item($set_cookie_id, "id");
                            setcookie("cookie_id", "", time() - (60 * 60 * 24 * 14), "/");
                        }
                        if (isset($_COOKIE["cookie_ver"])) {
                            setcookie("cookie_ver", "", time() - (60 * 60 * 24 * 14), "/");
                        }
                        setcookie("cookie_id", $cookie_id, time() + (60 * 60 * 24 * 14), "/");
                        setcookie("cookie_ver", $cookie_ver, time() + (60 * 60 * 24 * 14), "/");
                    }
                    // Login successful.
                    return true;    
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    // $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$id', '$now')");
                    return false;
                }
            }
        } else {
        // No user exists. 
        return false;
        }
    }
}

function checkbrute($user_id, $mysqli) {
    $now = time();
    $valid_attempts = $now - (2 * 60 * 60);

    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);

        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows > 20) {
            return true;
        } else {
            return false;
        }
    }
}

function login_check() {
    $dbConn = new DatabaseConn(false, 'secure_login');
    $mysqli = $dbConn->get_conn();
    if (!$info = get_stored_credentials()) {
        return false;
    }

    if (isset($info['login_string']) && isset($info['user_id'])) {
        if ($stmt = $mysqli->prepare("SELECT password, salt FROM members WHERE id = ? LIMIT 1")) {
            $stmt->bind_param('i', $info['user_id']);
            $stmt->execute();
            $stmt->store_result();
                
            if ($stmt->num_rows == 1) {
                $stmt->bind_result($password, $salt);
                $stmt->fetch();

                $user_browser = $_SERVER['HTTP_USER_AGENT'];
                $login_check = hash('sha512', $password.$salt);                
                $login_check = hash('sha512', $login_check.$user_browser);

                if ($login_check == $info['login_string']) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function edit_stored_email_cred($email) {
   if ($email) {
        if (isset($_SESSION['email'])) {
            $_SESSION['email'] = $email;
        }

        if (isset($_COOKIE['cookie_id']) && $_COOKIE['cookie_id'] && is_numeric($_COOKIE['cookie_id'])) {
            $dbConn = new DatabaseConn(false, 'secure_login');
            $dbConn->set_table("cookies");
            $dbConn->edit_item($email, "email", intval($_COOKIE['cookie_id']));
        }
   }
}

function get_stored_credentials() {
    $info = array();
    $dbConn = new DatabaseConn(false, 'secure_login');
    if (isset($_SESSION['id'], $_SESSION['email'], $_SESSION['login_string'])) {
        $info['user_id'] = $_SESSION['id'];
        $info['email'] = $_SESSION['email'];
        $info['login_string'] = $_SESSION['login_string'];
        return $info;
    } elseif (isset($_COOKIE['cookie_id']) && isset($_COOKIE['cookie_ver'])) {
        $cookie_id = $dbConn->sanitize_variable($_COOKIE['cookie_id']);
        $cookie_ver = $dbConn->sanitize_variable($_COOKIE['cookie_ver']);
        $dbConn->set_table("cookies");
        if (!$cookie_creds = $dbConn->get_item($cookie_id, "id")) {
            return false;
        }
        $cookie_creds = $cookie_creds[0];
        $info['user_id'] = $cookie_creds['user_id'];
        $info['email'] = $cookie_creds['email'];
        $info['epoch'] = $cookie_creds['epoch'];
        $info['login_string'] = $cookie_creds['login_string'];
        if (strcmp(hash('sha512', $info['email'].$info['login_string'].$info['epoch'].get_client_ip()), $cookie_ver) != 0) {
            return false;
        }
        return $info;
    } else {
        return false;
    }
}

function verify_cookies() {
    $info = array();
    $dbConn = new DatabaseConn(false, 'secure_login');
    if (isset($_COOKIE['cookie_id']) && isset($_COOKIE['cookie_ver'])) {
        $cookie_id = $dbConn->sanitize_variable($_COOKIE['cookie_id']);
        $cookie_ver = $dbConn->sanitize_variable($_COOKIE['cookie_ver']);
        $dbConn->set_table("cookies");
        if (!$creds = $dbConn->get_item($cookie_id, "id")) {
            return false;
        }
        $creds = $creds[0];
        if (strcmp(hash('sha512', $creds['email'].$creds['login_string'].$creds['epoch'].get_client_ip()), $cookie_ver) != 0) {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

function get_client_ip() {
     $ipaddress = '';
     if (getenv('HTTP_CLIENT_IP'))
         $ipaddress = getenv('HTTP_CLIENT_IP');
     else if(getenv('HTTP_X_FORWARDED_FOR'))
         $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
     else if(getenv('HTTP_X_FORWARDED'))
         $ipaddress = getenv('HTTP_X_FORWARDED');
     else if(getenv('HTTP_FORWARDED_FOR'))
         $ipaddress = getenv('HTTP_FORWARDED_FOR');
     else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
     else if(getenv('REMOTE_ADDR'))
         $ipaddress = getenv('REMOTE_ADDR');
     else
         $ipaddress = 'UNKNOWN';

     return $ipaddress; 
}
?>
