$(document).ready( function() {
    $(".list-group-item-text").hide();

    var clickEvents = $('.navbar-toggle').click(function() {
        $('#bs-example-navbar-collapse-1').slideToggle();
    });

    $(".challenge-title").click(function() {
        $(this).closest(".list-group-item").find(".list-group-item-text").slideToggle();
        $(this).closest(".list-group-item").find(".glyphicon").toggleClass("glyphicon-rotate-down").toggleClass("glyphicon-rotate-right");
    });
    $(".input-group").click(function (e) {
        e.preventDefault();
        return false;
    });
    $(".flag-submit").click(function () {
        programId = $(this).attr('id');
        flag = $(this).parent().siblings(".form-control").val();
        $.ajax({
            type: "POST",
            url: "check_flag.php",
            data: {flag: flag,
                   program_id: programId},
            dataType: "json"
        }).done( function(data) {
            if (data.hasOwnProperty('solved')) {
                var listGroup = $("#" + data.solved).closest(".list-group-item");
                listGroup.addClass("solved-challenge");
                listGroup.children(".list-group-item-heading").find(".challenge-points").removeClass("offset2").before("<span class=\"span2\">Solved!</span>");
                listGroup.find(".input-group").fadeOut('slow');
                listGroup.children("br").remove();
                $("#score").fadeOut(function() {
                    $(this).text(data.score);
                    $(this).fadeIn();
                });
                $.pnotify({
                    title: "Solved " + data.title + "!",
                    text: 'Nice job! Now keep solving challenges :D',
                    type: 'success',
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            } else {
                $("#" + data.id).closest(".list-group-item").animate({backgroundColor: "rgba(240, 65, 36, 0.9)"}, 1000).animate({backgroundColor: "white"}, 1000, function() {
                    $("#" + data.id).closest(".list-group-item").removeAttr("style");
                });
                $.pnotify({
                    title: data.error,
                    text: 'Make sure you entered the correct flag',
                    type: 'error',
                    addclass: 'alert-danger',
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            }
        });
    });
});
