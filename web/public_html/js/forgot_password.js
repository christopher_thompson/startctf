$(document).ready(function() {
    $('#inputEmail').bind('change keyup', function(e) {
        if (validateEmail($(this).val()) == false) {
            $(this).parent().removeClass("has-success").addClass("has-error");
            $("#resetButton").prop('disabled', true);
        } else {
            $(this).parent().removeClass("has-error").addClass("has-success");
            $("#resetButton").removeAttr("disabled");
        }
    });
    
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $("#resetButton").click( function() {
        $.ajax({
            type: "POST",
            url: "process_forgot_password.php",
            data: {
                email: $("#inputEmail").val() 
            },
            dataType: "json"
        }).done(function(data) {
            if (data.error === undefined || data.error == null) {
                $.pnotify({
                    title: 'Your password has been reset!',
                    text: "An email was sent to your account letting you know of your temporary password. Please check your email, login and change your password.",
                    type: 'success',
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            } else {
                $.pnotify({
                    title: 'Unsuccessful password reset',
                    text: data.error,
                    type: 'error',
                    addclass: "alert-danger",
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            }
        });
    });
});
