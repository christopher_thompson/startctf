$(document).ready(function() {
    /* JS for learning page */
    var currentCategory, currentTopic;
    $('#category-container .span4').click(function(event) {
        var category = $(this).attr('id');
        currentCategory = category;
        var timeDelay = 0;
        $('.category-topics').each( function() {
            if ($(this).is(":visible")) {
                timeDelay = 600;
            }
        });
        $('.topic-material').each( function() {
            if ($(this).is(":visible")) {
                timeDelay = 600;
            }
        });
        $('.category-topics').fadeOut(600);
        $('.topic-material').fadeOut(600);
        $("#material-editing-pane").fadeOut(600);
        $('#' + category + '-topics').delay(timeDelay).fadeIn(600);
        $("html, body").delay(timeDelay + 600).animate({ scrollTop: $(document).height() }, 1000);
    });

    $('#topic-container .span4').click(function(event) {
        var topic = $(this).attr('id');
        currentTopic = topic;
        var timeDelay = 0;
        $('.topic-material').each( function() {
            if ($(this).is(":visible")) {
                timeDelay = 600;
            }
        });
        $('.topic-material').fadeOut(600);
        $("#material-editing-pane").fadeOut(600);
        $('#' + topic + '-material').delay(timeDelay).fadeIn(600);
        $("html, body").delay(timeDelay + 600).animate({ scrollTop: $(document).height() }, 1000);
    });

    $('#material-container .span4').click(function(event) {
        var material = $(this).attr('id');
        // Ajax request to get material file and put html in box
        $.get("/learn/categories/"+currentCategory+"/"+currentTopic+"/"+material+".php", function(data) {
            editor.setValue(data);
        });
        $("#material-editing-heading").text(toCamelCase(material));
        $("#material-editing-pane").fadeIn();
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    });

    function toCamelCase(str) {
        var arr = str.split(/\s|_/);
        for(var i=0,l=arr.length; i<l; i++) {
            arr[i] = arr[i].substr(0,1).toUpperCase() + 
                     (arr[i].length > 1 ? arr[i].substr(1).toLowerCase() : "");
        }
        return arr.join(" ");
    }

    $('button').tooltip({container: 'body'});

    $(".add-button").click(function(event) {
        event.preventDefault();
        $(this).hide();
        var form = $(this).siblings(".add-form");
        if (!form.is(':visible')) {
            form.slideDown();
        }
    });

    $(".add-cancel-button").click(function(event) {
        event.preventDefault();
        var form = $(this).closest(".add-form");
        form.slideUp(function() {
            $(this).siblings(".add-button").fadeIn();
        });
    });

    var panels = $('.admin-panel-infos');
    var panelsButton = $('.admin-panel-dropdown');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
        //get data-for attribute
        var dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function() {
            //Completed slidetoggle
            if (idFor.is(':visible')) {
                currentButton.html('Hide');
            } else {
                currentButton.html('Show');
            }
        })
    });

    $(".accessButton").click(function(event) {
        $(this).toggleClass("btn-info");
        $(this).toggleClass("btn-danger");
        var label = $(this).parent().siblings(".control-label");
        label.children(".access-allowed").toggle();
        label.children(".access-not-allowed").toggle();
    });

    $("#saveAccessSettings").click(function(event) {
        var data = new Object();
        data.action = "access_settings";
        if ($("#allowRegistration").hasClass("btn-info")) {
            data.registration = true;
        } else {
            data.registration = false;
        }
        if ($("#allowSubmission").hasClass("btn-info")) {
            data.submission = true;
        } else {
            data.submission = false;
        }
        if ($("#allowAccountChange").hasClass("btn-info")) {
            data.accountChange = true;
        } else {
            data.accountChange = false;
        }
        if ($("#enableScoreboard").hasClass("btn-info")) {
            data.scoreboard = true;
        } else {
            data.scoreboard = false;
        }
        sendData(data);
    });

    $(".show-edit-form-btn").click(function(event) {
        $(this).closest(".admin-panel").find(".edit-btn").toggle();
        var inputs = $(this).closest(".admin-panel").find("input");
        inputs.toggle();
        var text_area = $(this).closest(".admin-panel").find("textarea");
        text_area.toggle();
        text_area.parent().siblings().toggle();
        inputs.parent().siblings().toggle();
        $(this).closest(".admin-panel").find("edit-btn").toggle();
    });

    $(".edit-btn").click(function(event) {
        event.preventDefault();
        var parentId = $(this).closest(".admin-panel-infos").attr("id");
        var temp = parentId.split("_");
        var numId = parseInt(temp[1], 10);
        console.log(numId);
        var data = new Object();
        data.action = "edit_" + temp[0];
        switch (data.action) {
            case "edit_challenge":
            break;
            case "edit_team":
            break;
            case "edit_user":
            break;
            case "edit_update":
                data.id = numId;
                data.title = $("#editUpdateTitle" + numId).children("input").val();
                data.body = $("#editUpdateBody" + numId).children("textarea").val();
                data.picture = $("#editUpdatePicture" + numId).children("input").val();
                if (data.picture == "" || data.picture === undefined) {
                    data.picture = "default.jpg";
                }
            break;
        }

        sendData(data);
    });

    $(".add-submit-button").click(function(event) {
        event.preventDefault();
        var addForm = $(this).closest(".add_form");
        var data = new Object();
        data.action = addForm.attr("id");
        switch (data.action) {
            case "add_challenge":
                data.title = $("#inputChallengeTitle").val();
                data.description = $("#inputChallengeDescription").val();
            break;
            case "add_team":
                data.name = $("#inputTeamName").val();
                data.member_1 = $("#inputTeamMember1").val();
                data.member_2 = $("#inputTeamMember2").val();
                data.member_3 = $("#inputTeamMember3").val();
                data.score = $("#inputTeamScore").val();
            break;
            case "add_user":
                data.first_name = $("#inputUserFirstName").val();
                data.last_name = $("#inputUserLastName").val();
                data.email = $("#inputUserEmail").val();
                data.password = $("#inputUserPassword");
                data.team = $("#inputUserTeam").val();
            break;
            case "add_update":
                data.title = $("#inputUpdateTitle").val();
                data.body = $("#inputUpdateBody").val();
                data.picture = $("#inputUpdatePicture").val();
                if (data.picture == "" || data.picture === undefined) {
                    data.picture = "default.jpg";
                }
            break;
        }

        sendData(data);
    });


    $(".delete-btn").click(function(event) {
        event.preventDefault();
        var parentId = $(this).closest(".admin-panel-infos").attr("id");
        var temp = parentId.split("_");
        var numId = parseInt(temp[1], 10);
        var data = new Object();
        data.action = "remove_" + temp[0];
        data.id = numId;
        sendData(data);
    });

    function sendData(data) {
        $.ajax({
            type: "POST",
            url: "admin_panel.php",
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic c3RhcnRDVEZBZG0xbjpJc01heW9ubmFpc2VBUDQkJHdvcmQ=");
            },
            dataType: "json"
        }).done(function(data) {
            if (data.registered == true) {
                $.pnotify({
                    title: data.successMessage,
                    text: "Your changes have been successfully saved to the database.",
                    type: 'success',
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            } else {
                if (data.error.length > 0) {
                    $.pnotify({
                        title: "Database Error; check Apache Error log",
                        text: data.error,
                        type: 'error',
                        addclass: "alert-danger",
                        before_open: function(pnotify) {
                            pnotify.css({
                                "top": "65px",
                                "right": "5px"
                            });
                        }
                    });
                }
            }
        });
    }

});
