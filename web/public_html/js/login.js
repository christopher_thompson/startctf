$(function() {
    $("form input").keypress(function(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            processForm();
            return false;
        } else {
            return true;
        }
    });
});

function processForm() {
    var email = $("#email").val();
    var password = hex_sha512($("#password").val());
    var rememberMe = false;
    if ($("#rememberMeButton").hasClass("active")) {
        rememberMe = true;
    }
    $.ajax({
        type: "POST",
        url: "process_login.php",
        data: {email: email,
               password: password,
               rememberMe: rememberMe},
        dataType: "json"
    }).done( function(data) {
        if(data.carolina == true) {
            suchKonami();
        }
        if(data.loggedIn == true) {
            $('#login-link').html("Log Out");
            $('#register-link').parent().remove();
            $('.navbar-right li:nth-child(5)').after("<li><a id=\"shell-link\" href=\"../shell\">Shell</a></li> \
                                                        <li><a id=\"account-link\" href=\"../account\">Account</a></li>");
            $("#loginPane").css({"display": "block", "position": "relative"});
            $("#successTitle, #successfulLoginPane > h1").css({"position": "relative"});
            $("#loginPane").animate({opacity: 0, top: "100px"}, 1000, function() {
                $("#loginPane").css({"display": "none"});
                $("#successTitle").fadeIn(function() {
                    $("#successfulLoginPane > center > h1").each(function(i) {
                        $(this).delay(i*400).fadeIn(1000);
                    });
                });
            });
        } else {
            $.pnotify({
                title: 'Unsuccessful Login',
                text: 'Your credentials were not correct. Please make sure you are entering the correct username and password.',
                type: 'error',
                addclass: "alert-danger",
                before_open: function(pnotify) {
                    pnotify.css({
                        "top": "65px",
                        "right": "5px"
                    });
                }
            });
        }
    });
}

$(function(){
    $('.button-checkbox').each(function(){
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
            };

        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });

        $checkbox.on('change', function () {
            updateDisplay();
        });

        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else
            {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
        function init() {
            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});

