var verifyInput;
var setTeamInput;
var processForm;
var userTeamName;
var form
var formStatus = {email: true, oldpassword: true, newpassword: true, retypedpassword: true};

$(document).ready(function() {
    userTeamName = $("#inputTeam").val();
    $("#submitButton").click(function(e) {
        e.preventDefault();
        var email = $("#inputEmail").val();
        var firstName = $("#inputFirstName").val();
        var lastName = $("#inputLastName").val();
        var oldPassword = "";
        if ($("#inputOldPassword").val() !== undefined && $("#inputOldPassword").val().length >= 8) {
            oldPassword = hex_sha512($("#inputOldPassword").val());
        }

        var newPassword = "";
        if ($("#inputNewPassword").val() !== undefined && $("#inputNewPassword").val().length >= 8) {
            newPassword = hex_sha512($("#inputNewPassword").val());
        }

        var team = $("#inputTeam").val();
        var teamPassword = hex_sha512($("#inputTeamPassword").val());
        $.ajax({
            type: "POST",
            url: "update_account.php",
            data: {email: email,
                   oldPassword: oldPassword,
                   newPassword: newPassword,
                   firstName: firstName,
                   lastName: lastName,
                   team: team,
                   teamPassword: teamPassword
                   },
            dataType: "json"
        }).done( function(data) {
            $('#registerError').html("");
            if (data.updated == true) {
                $.pnotify({
                    title: 'Your account was successfuly updated!',
                    text: "All your changes to your account have been saved!",
                    type: 'success',
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            } else {
                $('#updateError').html(printError(data.error));
                if (data.error.length > 0) {
                    $.pnotify({
                        title: 'Unsuccessful Account Update',
                        text: 'Sorry, but there was a problem when we attempted to save your changes. Please fix any errors on the form before you attempt to register again.',
                        type: 'error',
                        addclass: "alert-danger",
                        before_open: function(pnotify) {
                            pnotify.css({
                                "top": "65px",
                                "right": "5px"
                            });
                        }
                    });
                } else {
                    $.pnotify({
                        title: 'Unsuccessful Account Update',
                        text: 'Sorry, but there was a problem when we attempted to save your changes. Try changing your account again and let us know if the problem persists.',
                        type: 'error',
                        addclass: "alert-danger",
                        before_open: function(pnotify) {
                            pnotify.css({
                                "top": "65px",
                                "right": "5px"
                            });
                        }
                    });
                }
            }
        });
        return false;
    });

    verifyInput = function(inputBox) {
        var successMessage = "";
        switch(inputBox) {
            case "firstName":
                if ($("#inputFirstName").val().length > 0) {
                    formStatus.firstname = true;
                    inputHasSuccess("inputFirstName", "firstNameHelp", "You have something here so we will assume it is your name.");
                } else {
                    formStatus.firstname = false;
                    inputHasError("inputFirstName", "firstNameHelp", "Could you please enter your first name? We would appreciate it.");
                }
                changeFormHelp("First Name", "firstNameHelp");
            break;
            case "lastName":
                if ($("#inputLastName").val().length > 0) {
                    formStatus.lastname = true;
                    inputHasSuccess("inputLastName", "lastNameHelp", "You have something here so we will assume it is your last name.");
                } else {
                    formStatus.lastname = false;
                    inputHasError("inputLastName", "lastNameHelp", "Could you please enter your last name? We would appreciate it.");
                }
                changeFormHelp("Last Name", "lastNameHelp");
            break;
            case "email":
                if (validateEmail($("#inputEmail").val()) == true) {
                    inputHasSuccess("inputEmail", "emailHelp",  "You have entered a valid email");
                    formStatus.email = true;
                }
                changeFormHelp("Email", "emailHelp");
            break;
            case "password":
                // TODO have notification  appear under input
            break;
            case "retypePassword":
                // TODO have notification  appear under input
            break;
            case "team":
                changeFormHelp("Team", "teamHelp");
            break;
            case "teamPassword":
                changeFormHelp("Team Password", "teamPasswordHelp");
            break;
        }
    }

    function changeFormHelp(title, id) {
        $("#formHelp").fadeOut("fast", function() {
            $("#formHelpTitle").html(title);
            $("#formHelpContent").children().hide();
            $("#" + id).show();
            $("#formHelp").fadeIn();
        });
    }

    $(function() {
        $('#inputFirstName').bind('change keyup', function (e) {
            if ($(this).val().length > 0) {
                formStatus.firstname = true;
                inputHasSuccess("inputFirstName", "firstNameHelp", "You have something here so we will assume it is your name.");
            } else {
                formStatus.firstname = false;
                inputHasError("inputFirstName", "firstNameHelp", "Could you please enter your first name? We would appreciate it.");
            }
        });

        $('#inputLastName').bind('change keyup', function (e) {
            if ($(this).val().length > 0) {
                formStatus.lastname = true;
                inputHasSuccess("inputLastName", "lastNameHelp", "You have something here so we will assume it is your last name.");
            } else {
                formStatus.lastname = false;
                inputHasError("inputLastName", "lastNameHelp", "Could you please enter your last name? We would appreciate it.");
            }
        });

        $('#inputEmail').bind('change keyup', function (e) {
            if (validateEmail($(this).val()) == false) {
                formStatus.email = false;
                inputHasError("inputEmail", "emailHelp", "This is not the email we were looking for");
            } else {
                formStatus.email = true;
                inputHasSuccess("inputEmail", "emailHelp", "You have entered a valid email");
            }
        });

        $('#inputOldPassword').bind('change keyup', function (e) {
            if ($(this).val().length > 0) {
                formStatus.oldpassword = true;
                $(this).parent().removeClass("has-error").addClass("has-success");
                changeSubmit();
            }
        });

        $('#inputPassword').bind('change keyup', function (e) {
            if (getPasswordComplexity($(this).val())) {
                formStatus.password = true;
                $("#newPasswordContainer").children(".alert").slideUp();
                $(this).parent().removeClass("has-error").addClass("has-success");
                changeSubmit();
            }

            if ($('#inputRetypePassword').val() == $("#inputPassword").val() && formStatus.password) {
                formStatus.retypedpassword = true;
                $("#retypedPasswordContainer").children(".alert").slideUp();
                $('#inputRetypePassword').parent().removeClass("has-error").addClass("has-success");
                changeSubmit();
            } else {
                formStatus.retypedpassword = false;
                $('#inputRetypePassword').parent().removeClass("has-success").addClass("has-error");
                changeSubmit();
            }
        });

        $('#inputRetypePassword').bind('change keyup', function (e) {
            if ($(this).val() == $("#inputPassword").val() && formStatus.password) {
                formStatus.retypedpassword = true;
                $("#retypedPasswordContainer").children(".alert").slideUp();
                $(this).parent().removeClass("has-error").addClass("has-success");
                changeSubmit();
            } else {
                formStatus.retypedpassword = false;
                $("#retypedPasswordContainer").children(".alert").remove();
                $("#retypedPasswordContainer").append(printError("Your retyped password does not match your new password."));
                $(this).parent().removeClass("has-success").addClass("has-error");
                changeSubmit();
            }
        });

        $('#inputTeam').bind('change keyup', function (e) {
            if ($(this).val().length > 0) {
                formStatus.team = true;
                if (!$("#teamCheckNotification").is(":visible")) {
                    $("#teamCheckNotification").slideDown();
                }
                inputHasSuccess("inputTeam", "teamHelp", "Great work! Make sure this is the team you want to create or join before you finalize your application");
            } else {
                $("#teamNameTableBody").html("<tr class=\"warning\"><th>Start typing to search for teams</th></tr>");
                $("#teamCheckNotification").slideUp();
                formStatus.team = false;
                inputHasError("inputTeam", "teamHelp", "Make sure you have entered a team name before you move on.");
            }

            if ($(this).val() != userTeamName) {
                $("#inputTeamPassword").closest(".form-group").fadeIn();
            } else {
                $("#inputTeamPassword").closest(".form-group").fadeOut();
            }
        });

        $('#teamCheckButton').bind('click', function(e) {
            searchForTeam($('#inputTeam').val());
        });

        $('#changePasswordButton').bind('click', function(e) {
            changeFormHelp("Change Password", "changePasswordHelp");
        });

        $('#inputTeamPassword').bind('change keyup', function (e) {
            if ($(this).val().length == 9) {
                formStatus.teamPassword = true;
                inputHasSuccess("inputTeamPassword", "teamPasswordHelp", "If you are attempting to join an existing team, we will check to make sure the password you entered is correct.");
            } else {
                if ($(this).val().length > 0) {
                    inputHasError("inputTeamPassword", "teamPasswordHelp", "If you are attempting to join an existing team, you have to enter a team password which is exactly 9 characters long.");
                } else {
                    $("#teamPasswordHelp").children(".alert-success").remove();
                }
            }
        });
    });

    function getPasswordComplexity(password) {
        var length = password.length;
        var lowercase = /[a-z]/;
        var uppercase = /[A-Z]/;
        var numbers = /[0-9]/;
        var sqlInjection = /[\-']/;
        var xssAttempt = /[<>]/;
        var goodPassword = true;

        if (length < 8) {
            passwordHasError("Your password is less than 8 characters, please make it longer :D");
            goodPassword = false;
        }

        if (!goodPassword) {
            formStatus.password = false;
        }

        return goodPassword;
    }

    function passwordHasError(message) {
        $("#newPasswordContainer").children(".alert").remove();
        $("#newPasswordContainer").append(printError(message));
        $("#inputPassword").parent().removeClass("has-success").addClass("has-error");
        changeSubmit();
    }

    function validateEmail(email) { 
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    } 

    function inputHasSuccess(inputID, helpContentID, message) {
        $("#" + helpContentID).children(".alert").remove();
        $("#" + helpContentID).append(printSuccess(message));
        $("#" + inputID).parent().removeClass("has-error").addClass("has-success");
        changeSubmit();
    }

    function inputHasError(inputID, helpContentID, message) {
        $("#" + helpContentID).children(".alert").remove();
        $("#" + helpContentID).append(printError(message));
        $("#" + inputID).parent().removeClass("has-success").addClass("has-error");
        changeSubmit();
    }

    function searchForTeam(teamName) {
        $.ajax({
            type: "GET",
            url: "team_search.php",
            data: {
                team: teamName
            },
            dataType: "json"
        }).done( function(data) {
            if (data.teamExists == false) {
                $("#teamCheckNotification").html(printInfo("If you use this team name, a new team will be created."));
            } else {
                if (teamName == userTeamName) {
                    $("#teamCheckNotification").html(printInfo("You are already a part of this team."));
                } else {
                    $("#teamCheckNotification").html(printSuccess("There is a team which has the name you entered. Make sure you enter in the correct team password."));
                    if ($("#inputTeamPassword").val().length != 9) {
                        $("#inputTeamPassword").parent().removeClass("has-success");
                        $("#inputTeamPassword").parent().addClass("has-error");
                        inputHasError("inputTeamPassword", "teamPasswordHelp", "If you are attempting to join an existing team, you have to enter a team password which is exactly 9 characters long.");
                    }
                }
            }
        });
    }

    setTeamInput = function(teamName) {
        $("#inputTeam").val(teamName);
    }

    function changeSubmit() {
        var allowSubmit = true;
        for (var propt in formStatus) {
            allowSubmit = allowSubmit && formStatus[propt];
        }

        var submitButton = $("#submitButton");
        if (allowSubmit) {
            submitButton.removeAttr("disabled");
            submitButton.removeClass("btn-default");
            submitButton.addClass("btn-primary");
        } else {
            submitButton.attr("disabled");
            submitButton.addClass("btn-default");
            submitButton.removeClass("btn-primary");
        }
    }
});
