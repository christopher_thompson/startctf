//  wow
var suchKonami;
(function($) {
    //  such plugin
    suchKonami = function(tings) {
        //  very jquery
        var doge = $('body').css('font-family', 'Comic Sans MS, Comic Sans, Chalkboard, cursive');

        //  much array
        tings = $.extend(['doge', 'shibe', 'excite', 'impress', 'skill', 'warn', 'ctf', 'start', 'competition', 'challenge', 'christopher', 'jonathan', 'kent', 'umesh', 'ethan'], tings);

        var r = function(arr) {
            if (!arr) arr = tings;
            return arr[Math.floor(Math.random() * arr.length)];
        };

        var dogefix = [
            'wow', 'such ' + r(), 'very ' + r(), 'much ' + r(),
            'wow', 'such ' + r(), 'very ' + r(), 'much ' + r(),
            'so ' + r(), 'many ' + r(), 'want ' + r(),
            'wow', 'wow'
        ];

        var very = doge.append('<div class="such overlay" />').children('.such.overlay').css({
            position: 'fixed',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            pointerEvents: 'none'
        });

        $('img').each(function() {
            $(this).attr('src', 'https://images.encyclopediadramatica.es/3/3e/Doge_full_image.jpg');
        });

        setInterval(function() {

            $('.such.overlay').append(
                '<span style="position: absolute; left: ' + Math.random() * 100 + '%;top: ' + Math.random() * 100 + '%;font-size: ' + Math.max(24, (Math.random() * 50 + 50)) + 'px; color: rgb(' + Math.round(Math.random() * 255) + ', ' + Math.round(Math.random() * 255) + ', ' + Math.round(Math.random() * 255) + ');">' + r(dogefix) +
                '</span>');
        }, 700);
    };
})(jQuery);

var Konami = function() {
    var konami = {
        addEvent: function(obj, type, fn, ref_obj) {
            if (obj.addEventListener)
                obj.addEventListener(type, fn, false);
            else if (obj.attachEvent) {
                // IE
                obj["e" + type + fn] = fn;
                obj[type + fn] = function() {
                    obj["e" + type + fn](window.event, ref_obj);
                }

                obj.attachEvent("on" + type, obj[type + fn]);
            }
        },
        input: "",
        pattern: "3838404037393739666513",
        /*pattern:"38384040373937396665",*/
        load: function(link) {
            this.addEvent(document, "keydown", function(e, ref_obj) {
                if (ref_obj) konami = ref_obj; // IE
                konami.input += e ? e.keyCode : event.keyCode;
                if (konami.input.length > konami.pattern.length) konami.input = konami.input.substr((konami.input.length - konami.pattern.length));
                if (konami.input == konami.pattern) {
                    konami.code(link);
                    konami.input = "";
                    return;
                }
            }, this);
            this.iphone.load(link)

        },
        code: function(link) {
            window.location = link
        },
        iphone: {
            start_x: 0,
            start_y: 0,
            stop_x: 0,
            stop_y: 0,
            tap: false,
            capture: false,
            orig_keys: "",
            keys: ["UP", "UP", "DOWN", "DOWN", "LEFT", "RIGHT", "LEFT", "RIGHT", "TAP", "TAP", "TAP"],
            code: function(link) {
                konami.code(link);
            },
            load: function(link) {
                this.orig_keys = this.keys;
                konami.addEvent(document, "touchmove", function(e) {
                    if (e.touches.length == 1 && konami.iphone.capture == true) {
                        var touch = e.touches[0];
                        konami.iphone.stop_x = touch.pageX;
                        konami.iphone.stop_y = touch.pageY;
                        konami.iphone.tap = false;
                        konami.iphone.capture = false;
                        konami.iphone.check_direction();
                    }
                });
                konami.addEvent(document, "touchend", function(evt) {
                    if (konami.iphone.tap == true) konami.iphone.check_direction(link);
                }, false);
                konami.addEvent(document, "touchstart", function(evt) {
                    konami.iphone.start_x = evt.changedTouches[0].pageX
                    konami.iphone.start_y = evt.changedTouches[0].pageY
                    konami.iphone.tap = true
                    konami.iphone.capture = true
                });
            },
            check_direction: function(link) {
                x_magnitude = Math.abs(this.start_x - this.stop_x)
                y_magnitude = Math.abs(this.start_y - this.stop_y)
                x = ((this.start_x - this.stop_x) < 0) ? "RIGHT" : "LEFT";
                y = ((this.start_y - this.stop_y) < 0) ? "DOWN" : "UP";
                result = (x_magnitude > y_magnitude) ? x : y;
                result = (this.tap == true) ? "TAP" : result;

                if (result == this.keys[0]) this.keys = this.keys.slice(1, this.keys.length)
                if (this.keys.length == 0) {
                    this.keys = this.orig_keys;
                    this.code(link)
                }
            }
        }
    }
    return konami;
}

konami = new Konami();
konami.code = function() {
    suchKonami();
};
konami.load();
