var updateHelpPane;
var setTeamInput;
var processForm;
var joiningTeam;
var formStatus = {
    firstname: false,
    lastname: false,
    email: false,
    password: false,
    retypedpassword: false,
    team: false
};
var hackAttempt;

$(document).ready(function() {
    processForm = function() {
        // TODO make sure once registration is complete that they are logged into the site automatically
        // TODO send authentification email to make sure they are legit
        // TODO implement captcha system so no spamming
        var email = $("#inputEmail").val();
        var password = hex_sha512($("#inputPassword").val());
        var firstName = $("#inputFirstName").val();
        var lastName = $("#inputLastName").val();
        var team = $("#inputTeam").val();
        var teamPassword = hex_sha512($("#inputTeamPassword").val());
        $.ajax({
            type: "POST",
            url: "register.php",
            data: {
                email: email,
                password: password,
                firstName: firstName,
                lastName: lastName,
                team: team,
                teamPassword: teamPassword
            },
            dataType: "json"
        }).done(function(data) {
            $('#registerError').html("");
            if (data.registered == true) {
                $("#registrationForm").css({"display": "block", "position": "relative"});
                $("#successTitle, #successfulRegistrationPane > h1").css({"position": "relative"});
                $("#registrationForm").animate({opacity: 0, top: "100px"}, 1000, function() {
                    $("#registrationForm").css({"display": "none"});
                    $("#successTitle").fadeIn();
                });
                $.pnotify({
                    title: 'You were successfully registered!',
                    text: "You are free to move about the website :D\nLogin to start solving challenges <a href=\"../login/\">here</a>!",
                    type: 'success',
                    before_open: function(pnotify) {
                        pnotify.css({
                            "top": "65px",
                            "right": "5px"
                        });
                    }
                });
            } else {
                $('#registerError').html(printError(data.error));
                if (data.error.length > 0) {
                    $.pnotify({
                        title: 'Unsuccessful Registration',
                        text: 'Sorry, but there was a problem when we attempted to register you. Please fix any errors on the form before you attempt to register again.',
                        type: 'error',
                        addclass: "alert-danger",
                        before_open: function(pnotify) {
                            pnotify.css({
                                "top": "65px",
                                "right": "5px"
                            });
                        }
                    });
                } else {
                    $.pnotify({
                        title: 'Unsuccessful Registration',
                        text: 'Sorry, but there was a problem when we attempted to register you. Try registering again and let us know if the problem persists.',
                        type: 'error',
                        addclass: "alert-danger",
                        before_open: function(pnotify) {
                            pnotify.css({
                                "top": "65px",
                                "right": "5px"
                            });
                        }
                    });
                }
            }
        });
    }
    updateHelpPane = function(inputBox) {
        var successMessage = "";
        switch (inputBox) {
            case "firstName":
                changeFormHelp("First Name", "firstNameHelp");
                break;
            case "lastName":
                changeFormHelp("Last Name", "lastNameHelp");
                break;
            case "email":
                if (validateEmail($("#inputEmail").val()) == true) {
                    inputHasSuccess("inputEmail", "emailHelp", "You have entered a valid email");
                    formStatus.email = true;
                }
                changeFormHelp("Email", "emailHelp");
                break;
            case "password":
                changeFormHelp("Password", "passwordHelp");
                break;
            case "retypePassword":
                changeFormHelp("Retype Password", "retypePasswordHelp");
                break;
            case "team":
                changeFormHelp("Team", "teamHelp");
                break;
            case "teamPassword":
                changeFormHelp("Team Password", "teamPasswordHelp");
                break;
        }
    }

    $("#joiningTeamButton").click(function(e) {
        e.preventDefault();
        $("#joinCodeContainer").slideToggle();
    });

    function changeFormHelp(title, id) {
        $("#formHelp").fadeOut("fast", function() {
            $("#formHelpTitle").html(title);
            $("#formHelpContent").children().hide();
            $("#" + id).show();
            $("#formHelp").fadeIn();
        });
    }

    $(function() {
        $('#inputFirstName').bind('change keyup', function(e) {
            if ($(this).val().length > 0) {
                formStatus.firstname = true;
                inputHasSuccess("inputFirstName", "firstNameHelp", "You have something here so we will assume it is your name.");
            } else {
                formStatus.firstname = false;
                inputHasError("inputFirstName", "firstNameHelp", "Could you please enter your first name? We would appreciate it.");
            }
        });

        $('#inputLastName').bind('change keyup', function(e) {
            if ($(this).val().length > 0) {
                formStatus.lastname = true;
                inputHasSuccess("inputLastName", "lastNameHelp", "You have something here so we will assume it is your last name.");
            } else {
                formStatus.lastname = false;
                inputHasError("inputLastName", "lastNameHelp", "Could you please enter your last name? We would appreciate it.");
            }
        });

        $('#inputEmail').bind('change keyup', function(e) {
            if (validateEmail($(this).val()) == false) {
                formStatus.email = false;
                inputHasError("inputEmail", "emailHelp", "This is not the email we were looking for");
            } else {
                formStatus.email = true;
                inputHasSuccess("inputEmail", "emailHelp", "You have entered a valid email");
            }
        });

        $('#inputPassword').bind('change keyup', function(e) {
            if (getPasswordComplexity($(this).val())) {
                formStatus.password = true;
                inputHasSuccess("inputPassword", "passwordHelp", "Nice job! Your password matches our requirements.");
            }
        });

        $('#inputRetypePassword').bind('change keyup', function(e) {
            if ($(this).val() == $("#inputPassword").val()) {
                formStatus.retypedpassword = true;
                inputHasSuccess("inputRetypePassword", "retypePasswordHelp", "Nice job! You proved to us your abilities to recall short term memory.");
            } else {
                formStatus.retypedpassword = false;
                inputHasError("inputRetypePassword", "retypePasswordHelp", "Sorry, your passwords do not match :C");
            }
        });

        $('#inputTeam').bind('change keyup', function(e) {
            if ($(this).val().length > 0) {
                formStatus.team = true;
                inputHasSuccess("inputTeam", "teamHelp", "Great work! Make sure this is the team you want to create or join before you finalize your application");
            } else {
                $("#teamNameTableBody").html("<tr class=\"warning\"><th>Start typing to search for teams</th></tr>");
                formStatus.team = false;
                inputHasError("inputTeam", "teamHelp", "Make sure you have entered a team name before you move on.");
            }
        });

        $('#teamCheckButton').bind('click', function(e) {
            searchForTeam($('#inputTeam').val());
        });

        $('#inputTeamPassword').bind('change keyup', function(e) {
            if ($(this).val().length == 9) {
                formStatus.teamPassword = true;
                inputHasSuccess("inputTeamPassword", "teamPasswordHelp", "If you are attempting to join an existing team, we will check to make sure the password you entered is correct.");
            } else {
                if ($(this).val().length > 0) {
                    inputHasError("inputTeamPassword", "teamPasswordHelp", "If you are attempting to join an existing team, you have to enter a team password which is exactly 9 characters long.");
                } else {
                    $("#teamPasswordHelp").children(".alert-success").remove();
                }
            }
        });
    });

    hackAttempt = false;

    function getPasswordComplexity(password) {
        var length = password.length;
        var lowercase = /[a-z]/;
        var uppercase = /[A-Z]/;
        var numbers = /[0-9]/;
        var sqlInjection = /[\-']/;
        var xssAttempt = /[<>]/;
        var goodPassword = true;

        if (length < 8) {
            inputHasError("inputPassword", "passwordHelp", "Your password is less than 8 characters, please make it longer :D");
            goodPassword = false;
        }
        /*
        if (!lowercase.test(password) && goodPassword) {
            inputHasError("inputPassword", "passwordHelp", "Make sure you have at least one lowercase character.");
            goodPassword = false;
        }

        if (!uppercase.test(password) && goodPassword) {
            inputHasError("inputPassword", "passwordHelp", "Make sure you have at least one uppercase character.");
            goodPassword = false;
        }

        if (!numbers.test(password) && goodPassword) {
            inputHasError("inputPassword", "passwordHelp", "Your password needs at least one number.");
            goodPassword = false;
        }
        */

        if (sqlInjection.test(password) && goodPassword && !hackAttempt) {
            hackAttempt = true;
            $.pnotify({
                title: "Congratulations!",
                text: "You attempted to hack our site with a SQL injection, 10 points to Griffendor! (Sorry, you don't actually get any points)",
                type: "success",
                before_open: function(pnotify) {
                    pnotify.css({
                        "top": "50px",
                        "right": "5px"
                    });
                }
            });
        }

        if (!goodPassword) {
            formStatus.password = false;
        }

        return goodPassword;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function inputHasSuccess(inputID, helpContentID, message) {
        $("#" + helpContentID).children(".alert").remove();
        $("#" + helpContentID).append(printSuccess(message));
        $("#" + inputID).parent().removeClass("has-error").addClass("has-success");
        changeSubmit();
    }

    function inputHasError(inputID, helpContentID, message) {
        $("#" + helpContentID).children(".alert").remove();
        $("#" + helpContentID).append(printError(message));
        $("#" + inputID).parent().removeClass("has-success").addClass("has-error");

        changeSubmit();
    }

    function searchForTeam(teamName) {
        $.ajax({
            type: "GET",
            url: "team_search.php",
            data: {
                team: teamName
            },
            dataType: "json"
        }).done(function(data) {
            if (data.teamExists == false) {
                $("#teamCheckNotification").html(printInfo("If you use this team name, a new team will be created."));
            } else {
                $("#teamCheckNotification").html(printSuccess("There is a team which has the name you entered. Make sure you enter in the correct team password."));
                if ($("#inputTeamPassword").val().length != 9) {
                    $("#inputTeamPassword").parent().removeClass("has-success");
                    $("#inputTeamPassword").parent().addClass("has-error");
                    inputHasError("inputTeamPassword", "teamPasswordHelp", "If you are attempting to join an existing team, you have to enter a team password which is exactly 9 characters long.");
                }
            }
        });
    }

    setTeamInput = function(teamName) {
        $("#inputTeam").val(teamName);
    }

    function changeSubmit() {
        var allowSubmit = true;
        for (var propt in formStatus) {
            allowSubmit = allowSubmit && formStatus[propt];
        }

        var submitButton = $("#submitButton");
        if (allowSubmit) {
            submitButton.removeAttr("disabled");
            submitButton.removeClass("btn-default");
            submitButton.addClass("btn-primary");
        } else {
            submitButton.attr("disabled", "disabled");
            submitButton.addClass("btn-default");
            submitButton.removeClass("btn-primary");
        }
    }
});
