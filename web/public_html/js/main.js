var printInfoToHelp, printSuccessToHelp, printErrorToHelp;

$(document).ready(function() {
    $(".navbar-toggle").bind("click", function() {
        $(".navbar-collapse").slideToggle();
    });

    printSuccess = function(message) {
        return "<div class='alert alert-success'><strong>Oh yeah!: </strong> " + message + "</div>";
    }

    printError = function(message) {
        return "<div class='alert alert-danger'><strong>Oh noes!: </strong> " + message + "</div>";
    }

    printInfo = function(message) {
        return "<div class='alert alert-info'><strong>Just letting you know: </strong> " + message + "</div>";
    }
});
