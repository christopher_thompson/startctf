$(document).ready(function() {
    $("#commandHelp > li").bind("click", function(e) {
        var infoItem = "#" + $(this).attr("id") + "Info";
        $(infoItem).siblings().each(function() {
            $(this).slideUp();
        });
        $(this).siblings().each(function() {
            $(this).css("color", "#000000");
        });
        if ($(infoItem).is(":visible")) {
            $(this).css("color", "#000000");
            $(infoItem).slideUp();
        } else {
            $(this).css("color", "#008cba");
            $(infoItem).slideDown();
        }
    });
});
